<a href="{{ route('products.show', $product->slug) }}" class="single-product">
    @if (! $product->base_image->exists)
        <div class="image-placeholder">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
        </div>
    @else
        <div class="image-holder">
            <img src="{{ $product->base_image->path }}">
        </div>
    @endif

    <div class="single-product-details">
        <span class="product-name">{{ $product->name }}</span>

        <span class="product-price">
            {{ product_price($product) }}
        </span>
    </div>
</a>
<form method="POST" action="{{ route('cart.items.store') }}" class="clearfix">
                    {{ csrf_field() }}

                    <input type="hidden" name="product_id" value="{{ $product->id }}">

                    <div class="product-variants clearfix">
                        @foreach ($product->options as $option)
                            <div class="row">
                                <div class="col-sm-8 col-xs-10">
                                    @includeIf("public.products.partials.product.options.{$option->type}")
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="quantity pull-left clearfix">
                        <label class="pull-left" for="qty">{{ trans('storefront::product.qty') }}</label>

                        <div class="input-group-quantity pull-left clearfix">
                            <input type="text" name="qty" value="1" class="input-number input-quantity pull-left" id="qty" min="1" max="{{ $product->manage_stock ? $product->qty : '' }}">

                            <span class="pull-left btn-wrapper">
                                <button type="button" class="btn btn-number btn-plus" data-type="plus"> + </button>
                                <button type="button" class="btn btn-number btn-minus" data-type="minus" disabled> &#8211; </button>
                            </span>
                        </div>
                    </div>

                    <button type="submit" class="add-to-cart btn btn-primary pull-left" {{ $product->isOutOfStock() ? 'disabled' : '' }} data-loading>
                        {{ trans('storefront::product.add_to_cart') }}
                    </button>
                </form>