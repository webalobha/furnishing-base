<ul>
    <?php 
            $array = $slug = [];
    ?>
    @foreach ($subCategories as $subCategory)
        <?php
            $split_array = explode('-', $subCategory->name);

            $name = isset($split_array[2]) ? $split_array[0] .' '. $split_array[1] : $split_array[0];

            $final_name = isset($split_array[3]) ? $name.' '.$split_array[2] : $name;
            
            if(!in_array($final_name, $array))
            {
                array_push($array, $name);
                array_push($slug, $subCategory->slug);
                continue;    
            }
        ?>
    @endforeach

    @forelse($array as $key => $value)
        @if(isset($value))
        <li class="{{ request('category') === $slug[$key] ? 'active' : '' }}">
            <a href="{{ request()->fullUrlWithQuery(['category' => $slug[$key]]) }}">
                {{ ucfirst($value) }}
            </a>

            {{-- @if ($subCategory->items->isNotEmpty())
                @include('public.products.partials.sub_category_filter', ['subCategories' => $subCategory->items])
            @endif --}}
        </li>
        @endif
    @empty
    @endforelse
        
</ul>
