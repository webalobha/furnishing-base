<ul class="dropdown-menu">
    <li>
        <div class="fluid-menu-content">
            <?php $__currentLoopData = $menu->subMenus()->chunk(3); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $chunk): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="row">
                    <?php $__currentLoopData = $chunk; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subMenu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-sm-4">
                            <a href="<?php echo e($subMenu->url()); ?>" class="title" target="<?php echo e($subMenu->target()); ?>">
                                <?php echo e($subMenu->name()); ?>

                            </a>

                            <ul class="list-inline">
                                <?php $__currentLoopData = $subMenu->items(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e($item->url()); ?>" target="<?php echo e($subMenu->target()); ?>">
                                            <?php echo e($item->name()); ?>

                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </li>
</ul>
