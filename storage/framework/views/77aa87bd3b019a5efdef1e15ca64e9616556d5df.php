<?php $__env->startPush('styles'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />
<style>
    [v-cloak] {
        display: none;
    }

</style>
<?php $__env->stopPush(); ?>
<?php $__env->startComponent('admin::components.page.header'); ?>
<?php $__env->slot('title', trans('flashsale::flash_sales.products')); ?>
<li class="active"><?php echo e(trans('flashsale::flash_sales.flash_sales')); ?></li>
<?php echo $__env->renderComponent(); ?>
<?php $__env->startComponent('admin::components.page.index_table'); ?>
<div class="row" style="padding-bottom: 55px;">
    <div class="col-lg-6">
        <select id="select_flash_sale_id" class="pull-left form-control">
            <?php $__currentLoopData = $flash_sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $flash_sale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($flash_sale->id); ?>"><?php echo e($flash_sale->title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
    </div>
    <div class="col-lg-6">
        <button type="button" class="btn btn-danger pull-right" id="button_reset_products">
            Reset Changes
        </button>
        <button type="button" class="btn btn-success pull-right" id="button_sync_products">
            Save Changes
        </button>
        <button type="button" class="btn btn-default pull-right" id="button_add_product">
            Add Product
        </button>
    </div>
</div>
<div class="modal-body" id="div_app_products" v-cloak>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Product Name</th>
                <th>Original Price</th>
                <th>Special Price</th>
                <th>Flash Sale Price</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(product, index) in products">
                <td>{{ product.id }}</td>
                <td>{{ product.name }}</td>
                <td>{{ product.price ? product.price.formatted : 'N/A' }}</td>
                <td>{{ product.special_price ? product.special_price.formatted : 'N/A' }}</td>
                <td>{{ product.flash_sale_price ? product.flash_sale_price.formatted : 'N/A' }}</td>
                <td>
                    <a href="javascript:void(0);" v-on:click="edit_product(product)">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="javascript:void(0);" v-on:click="delete_product(product)">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a href="javascript:void(0);" v-on:click="moveup_product(index)">
                        <i class="fa fa-arrow-up"></i>
                    </a>
                    <a href="javascript:void(0);" v-on:click="movedown_product(index)">
                        <i class="fa fa-arrow-down"></i>
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div id="modal_add_product" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Product</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group ">
                        <label for='slug' class='col-md-3 control-label text-left'>Product<span class="m-l-5 text-red">*</span></label>
                        <div class='col-md-9' id="div_products_dropdown"></div>
                    </div>
                    <?php echo e(Form::number('price', 'Flash Sale Price', $errors, null, ['min' => 0, 'required' => true])); ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-primary" id="button_submit_product">Submit</button>
                <button type="button" class="btn btn-default btn-primary" id="button_update_product">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->renderComponent(); ?>
<?php $__env->startPush('scripts'); ?>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="http://malsup.github.io/jquery.blockUI.js"></script>
<script>
    window.custom_x = {
        data: {},
        methods: {},
        flags: {},
        routes: {},
        responses: {},
        errors: {}
    };

    window.custom_x.methods.block_ui = function () {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }
    window.custom_x.methods.unblock_ui = function () {
        $.unblockUI();
    }
    window.custom_x.routes.flash_sales_products_list = "<?php echo e(route('admin.flash_sales.products_list')); ?>";
    window.custom_x.routes.flash_sales_products_sync = "<?php echo e(route('admin.flash_sales.products_sync')); ?>";
    window.custom_x.routes.flash_sales_products_dropdown = "<?php echo e(route('admin.flash_sales.products_dropdown')); ?>";

    window.custom_x.methods.get_current_flash_sale_id = function () {
        let flash_sale_id = $("#select_flash_sale_id").val().trim();
        return flash_sale_id;
    }

    var app_products = new Vue({
        el: '#div_app_products',
        data: {
            products: []
        },
        methods: {
            edit_product: function (product) {
                window.current_edit_product = product;
                window.custom_x.methods.block_ui();
                axios.post(window.custom_x.routes.flash_sales_products_dropdown, {
                    flash_sale_id: window.custom_x.methods.get_current_flash_sale_id(),
                    product_id: product.id
                }).then(function (response) {
                    $("#div_products_dropdown").html(response.data);
                    $("#div_products_dropdown select").select2();
                    $("#price").val(product.flash_sale_price.amount);
                    $("#button_submit_product").hide();
                    $("#button_update_product").show();
                    $("#modal_add_product").modal('show');
                }).catch(function (error) {

                }).finally(function () {
                    window.custom_x.methods.unblock_ui();
                });
            },
            delete_product: function (product) {
                this.products = this.products.filter(function (p) {
                    return (p.id != product.id);
                });
            },
            moveup_product: function (index) {
                if (index != 0) {
                    const temp = this.products[index];
                    this.$set(this.products, index, this.products[index - 1]);
                    this.$set(this.products, index - 1, temp);
                }
            },
            movedown_product: function (index) {
                if (index != (this.products.length - 1)) {
                    const temp = this.products[index];
                    this.$set(this.products, index, this.products[index + 1]);
                    this.$set(this.products, index + 1, temp);
                }
            }
        }
    });

    const products_list = function () {
        window.custom_x.methods.block_ui();
        axios.post(window.custom_x.routes.flash_sales_products_list, {
            flash_sale_id: window.custom_x.methods.get_current_flash_sale_id()
        }).then(function (response) {
            console.log(response);
            app_products.products = response.data.data.products;
            $("#myModal").modal('show');
        }).catch(function (error) {
            console.log(error);
        }).finally(function () {
            console.log('finally executed');
            window.custom_x.methods.unblock_ui();
        });
    }
    products_list();


    const add_product = function () {
        window.custom_x.methods.block_ui();
        axios.post(window.custom_x.routes.flash_sales_products_dropdown, {
            flash_sale_id: window.custom_x.methods.get_current_flash_sale_id()
        }).then(function (response) {
            $("#div_products_dropdown").html(response.data);
            $("#div_products_dropdown select").select2();
            $("#button_submit_product").show();
            $("#button_update_product").hide();
            $("#modal_add_product").modal('show');
        }).catch(function (error) {

        }).finally(function () {
            window.custom_x.methods.unblock_ui();
        });
    }

    const sync_products = function () {
        const product_ids = [];
        app_products.products.map(function (x, index) {
            product_ids.push({
                id: x.id,
                data: {
                    price: x.flash_sale_price.amount,
                    position: index
                }
            });
        });
        window.custom_x.methods.block_ui();
        axios.post(window.custom_x.routes.flash_sales_products_sync, {
            flash_sale_id: window.custom_x.methods.get_current_flash_sale_id(),
            product_ids: product_ids
        }).then(function (response) {
            console.log('Sync Finished');
            products_list();
        }).catch(function (error) {

        }).finally(function () {
            window.custom_x.methods.unblock_ui();
        });
    }

    $(document).ready(function () {
        $("#select_flash_sale_id").on('change', function () {
            products_list();
        });

        $("#button_add_product").on('click', function () {
            add_product();
        });

        $("#button_sync_products").on('click', function () {
            sync_products();
        });

        $("#button_reset_products").on('click', function () {
            products_list();
        });

        $("#button_submit_product").on('click', function () {
            const product_id = $("#div_products_dropdown select").val().trim();
            const price = $("#price").val().trim();
            if (product_id == "0") {
                alert('Please select a product');
            } else if (price == "") {
                alert('Please enter flash sale price');
            } else {
                const currency_prefix = $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[1].trim()[0];
                var product = {
                    id: $("#div_products_dropdown select").val().trim(),
                    name: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[0].trim(),
                    price: { formatted: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[1].trim() },
                    special_price: { formatted: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[2].trim() },
                    flash_sale_price: { amount: $("#price").val(), formatted: currency_prefix + $("#price").val() }
                };
                app_products.products.push(product);
                $("#modal_add_product").modal('hide');
                $("#div_products_dropdown select").val("0").change();
                $("#price").val('');
            }
        });

        $("#button_update_product").on('click', function () {
            const product_id = $("#div_products_dropdown select").val().trim();
            const price = $("#price").val().trim();
            if (product_id == "0") {
                alert('Please select a product');
            } else if (price == "") {
                alert('Please enter flash sale price');
            } else {
                const currency_prefix = $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[1].trim()[0];
                var product = {
                    id: $("#div_products_dropdown select").val().trim(),
                    name: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[0].trim(),
                    price: { formatted: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[1].trim() },
                    special_price: { formatted: $("option:selected", $("#div_products_dropdown select")).text().trim().split("##")[2].trim() },
                    flash_sale_price: { amount: $("#price").val(), formatted: currency_prefix + $("#price").val() }
                };
                app_products.products = app_products.products.map(function (p) {
                    if (p.id == window.current_edit_product.id) {
                        return product;
                    }
                    return p;
                });
                $("#modal_add_product").modal('hide');
                $("#div_products_dropdown select").val("0").change();
                $("#price").val('');
            }
        });
    });
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin::layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>