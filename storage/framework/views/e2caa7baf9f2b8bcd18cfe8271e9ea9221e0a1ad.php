<?php $__env->startSection('title', trans('storefront::account.links.my_orders')); ?>

<?php $__env->startSection('account_breadcrumb'); ?>
    <li class="active"><?php echo e(trans('storefront::account.links.my_orders')); ?></li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content_right'); ?>
    <div class="index-table">
        <?php if($orders->isEmpty()): ?>
            <h3 class="text-center"><?php echo e(trans('storefront::account.orders.no_orders')); ?></h3>
        <?php else: ?>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th><?php echo e(trans('storefront::account.orders.order_id')); ?></th>
                            <th><?php echo e(trans('storefront::account.orders.date')); ?></th>
                            <th><?php echo e(trans('storefront::account.orders.status')); ?></th>
                            <th><?php echo e(trans('storefront::account.orders.total')); ?></th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td>#<?php echo e($order->id); ?></td>
                                <td><?php echo e($order->created_at->toFormattedDateString()); ?></td>
                                <td><?php echo e($order->status()); ?></td>
                                <td><?php echo e($order->total->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                                <td>
                                    <a href="<?php echo e(route('account.orders.show', $order)); ?>" class="btn-view" data-toggle="tooltip" title="<?php echo e(trans('storefront::account.orders.view_order')); ?>" rel="tooltip">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>

    <?php if($orders->isNotEmpty()): ?>
        <div class="pull-right">
            <?php echo $orders->links(); ?>

        </div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('public.account.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>