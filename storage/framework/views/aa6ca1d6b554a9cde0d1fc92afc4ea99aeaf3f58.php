<select style="width:100%;">
    <option value="0">Product ## Price ## Special Price</option>
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($product->id); ?>" <?php echo e($product->id == $product_id  ? 'selected' : ''); ?>>
        <?php echo e($product->name.' ## '.($product->price ? $product->price->format() : 'N/A').' ## '.($product->special_price ? $product->special_price->format() : 'N/A')); ?>

    </option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</select>
