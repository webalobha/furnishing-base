<?php $__env->startComponent('admin::components.page.header'); ?>
    <?php $__env->slot('title', trans('admin::resource.create', ['resource' => trans('menu::menus.menu')])); ?>

    <li><a href="<?php echo e(route('admin.menus.index')); ?>"><?php echo e(trans('menu::menus.menus')); ?></a></li>
    <li class="active"><?php echo e(trans('admin::resource.create', ['resource' => trans('menu::menus.menu')])); ?></li>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startSection('content'); ?>
    <form method="POST" action="<?php echo e(route('admin.menus.store')); ?>" class="form-horizontal" id="menu-create-form" novalidate>
        <?php echo e(csrf_field()); ?>


        <?php echo $__env->make('menu::admin.menus.form.fields', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('menu::admin.menus.partials.shortcuts', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php echo $__env->make('admin::layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>