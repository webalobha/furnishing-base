<?php $__env->startSection('title', trans('storefront::account.view_order.view_order')); ?>

<?php $__env->startSection('breadcrumb'); ?>
    <li><a href="<?php echo e(route('account.dashboard.index')); ?>"><?php echo e(trans('storefront::account.links.my_account')); ?></a></li>
    <li><a href="<?php echo e(route('account.orders.index')); ?>"><?php echo e(trans('storefront::account.links.my_orders')); ?></a></li>
    <li class="active"><?php echo e(trans('storefront::account.orders.view_order')); ?></li>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="orders-view-wrapper clearfix">
        <div class="row">
            <h3><?php echo e(trans('storefront::account.view_order.view_order')); ?></h3>

            <div class="col-md-6 col-sm-6">
                <div class="order-details">
                    <h5><?php echo e(trans('storefront::account.view_order.order_details')); ?></h5>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.telephone')); ?>:</td>
                                    <td><?php echo e($order->customer_phone ?: trans('storefront::account.view_order.n/a')); ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.email')); ?>:</td>
                                    <td><?php echo e($order->customer_email); ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.date')); ?>:</td>
                                    <td><?php echo e($order->created_at->toFormattedDateString()); ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.shipping_method')); ?>:</td>
                                    <td><?php echo e($order->shipping_method); ?></td>
                                </tr>

                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.payment_method')); ?>:</td>
                                    <td><?php echo e($order->payment_method); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="order-address">
                    <h5><?php echo e(trans('storefront::account.view_order.billing_address')); ?></h5>
                    <span><?php echo e($order->billing_full_name); ?></span>
                    <span><?php echo e($order->billing_address_1); ?></span>
                    <span><?php echo e($order->billing_address_2); ?></span>
                    <span><?php echo e($order->billing_city); ?>, <?php echo e($order->billing_state_name); ?> <?php echo e($order->billing_zip); ?></span>
                    <span><?php echo e($order->billing_country_name); ?></span>
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="order-address">
                    <h5><?php echo e(trans('storefront::account.view_order.shipping_address')); ?></h5>
                    <span><?php echo e($order->shipping_full_name); ?></span>
                    <span><?php echo e($order->shipping_address_1); ?></span>
                    <span><?php echo e($order->shipping_address_2); ?></span>
                    <span><?php echo e($order->shipping_city); ?>, <?php echo e($order->shipping_state_name); ?> <?php echo e($order->shipping_zip); ?></span>
                    <span><?php echo e($order->shipping_country_name); ?></span>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 clearfix">
            <div class="row">
                <div class="cart-list">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="hidden-xs">
                                <tr>
                                    <th><?php echo e(trans('storefront::account.view_order.product')); ?></th>
                                    <th><?php echo e(trans('storefront::account.view_order.unit_price')); ?></th>
                                    <th><?php echo e(trans('storefront::account.view_order.quantity')); ?></th>
                                    <th><?php echo e(trans('storefront::account.view_order.line_total')); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $__currentLoopData = $order->products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td>
                                            <?php if(isset($product->product)): ?>
                                            <h5>
                                                <a href="<?php echo e(route('products.show', ['slug' => $product->product->slug])); ?>">
                                                    <?php echo e($product->product->name); ?>

                                                </a>
                                            </h5>
                                            <?php endif; ?>

                                            <?php if($product->hasAnyOption()): ?>
                                                <div class="option">
                                                    <?php $__currentLoopData = $product->options; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <span><?php echo e($option->name); ?>: <span><?php echo e($option->values->implode('label', ', ')); ?></span></span>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </div>
                                            <?php endif; ?>
                                        </td>

                                        <td>
                                            <label><?php echo e(trans('storefront::account.view_order.unit_price')); ?>:</label>
                                            <span><?php echo e($product->unit_price->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></span>
                                        </td>

                                        <td>
                                            <label><?php echo e(trans('storefront::account.view_order.quantity')); ?>:</label>
                                            <span><?php echo e(intl_number($product->qty)); ?></span>
                                        </td>

                                        <td>
                                            <label><?php echo e(trans('storefront::account.view_order.line_total')); ?>:</label>
                                            <span><?php echo e($product->line_total->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="total-wrapper pull-right">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><?php echo e(trans('storefront::account.view_order.subtotal')); ?></td>
                                <td><?php echo e($order->sub_total->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                            </tr>

                            <?php if($order->hasShippingMethod()): ?>
                                <tr>
                                    <td><?php echo e($order->shipping_method); ?></td>
                                    <td><?php echo e($order->shipping_cost->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                                </tr>
                            <?php endif; ?>

                            <?php if($order->hasCoupon()): ?>
                                <tr>
                                    <td><?php echo e(trans('storefront::account.view_order.coupon')); ?> (<span class="coupon-code"><?php echo e($order->coupon->code); ?></span>)</td>
                                    <td>&#8211;<?php echo e($order->discount->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                                </tr>
                            <?php endif; ?>

                            <?php $__currentLoopData = $order->taxes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tax): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($tax->name); ?></td>
                                    <td><?php echo e($tax->order_tax->amount->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                                <td><?php echo e(trans('storefront::account.view_order.total')); ?></td>
                                <td><?php echo e($order->total->convert($order->currency, $order->currency_rate)->format($order->currency)); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('public.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>