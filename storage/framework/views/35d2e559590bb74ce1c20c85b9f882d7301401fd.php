<div class="slider-banner">
    <?php echo $__env->make('public.home.sections.partials.single_banner', ['banner' => $sliderBanners[1]], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('public.home.sections.partials.single_banner', ['banner' => $sliderBanners[2]], \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
