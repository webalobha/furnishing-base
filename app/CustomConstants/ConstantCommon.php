<?php
namespace FleetCart\CustomConstants;

define("ConstantCommon", [
    "auth_types"=> [
        "plain"=>[
            "slug"=>"plain",
            "display"=>"Plain"
        ],
        "oauth"=>[
            "slug"=>"oauth",
            "display"=>"Oauth"
        ]
    ]
]);
