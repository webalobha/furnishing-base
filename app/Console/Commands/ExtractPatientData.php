<?php

namespace FleetCart\Console\Commands;

use Carbon\Carbon;
use File as mainFile;
use Illuminate\Console\Command;
use Modules\Media\Entities\File;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Product\Entities\Product;
use Modules\Media\Entities\EntityFile;
use Modules\Category\Entities\Category;
use Illuminate\Support\Facades\Storage;
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Entities\AttributeSet;
use Modules\Product\Entities\ProductCategory;
use Modules\Attribute\Entities\AttributeValue;
use Modules\Attribute\Entities\ProductAttribute;
use Modules\Category\Entities\CategoryTranslation;
use Modules\Attribute\Entities\AttributeTranslation;
use Modules\Attribute\Entities\ProductAttributeValue;
use Modules\Attribute\Entities\AttributeSetTranslation;
use Modules\Attribute\Entities\AttributeValueTranslation;

class ExtractPatientData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'extract:patientData {fileName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to Extract Patient Data to Database';

    /**
     * Bind Model Customer
     * 
     * @var string
     */
    protected $customer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
                                File                          $file,
                                Product                       $product,
                                Category                      $category,
                                Attribute                     $attribute,
                                EntityFile                    $entityFiles,
                                AttributeSet                  $attributeSet,
                                AttributeValue                $attributeValue,
                                ProductAttribute              $productAttribute,
                                ProductCategory               $product_categories,
                                CategoryTranslation           $categoryTranslation,
                                AttributeTranslation          $attributeTranslation,
                                ProductAttributeValue         $productAttributeValue,
                                AttributeSetTranslation       $attributeSetTranslation,
                                AttributeValueTranslation     $attributeValueTranslation
                               )
    {
        parent::__construct();
        $this->file                      =  $file;
        $this->product                   =  $product;
        $this->category                  =  $category;
        $this->entityFiles               =  $entityFiles;
        $this->attribute                 =  $attribute;
        $this->attributeSet              =  $attributeSet;
        $this->attributeValue            =  $attributeValue;
        $this->productAttribute          =  $productAttribute;
        $this->product_categories        =  $product_categories;
        $this->categoryTranslation       =  $categoryTranslation;
        $this->attributeTranslation      =  $attributeTranslation;
        $this->productAttributeValue     =  $productAttributeValue;
        $this->attributeSetTranslation   =  $attributeSetTranslation;
        $this->attributeValueTranslation =  $attributeValueTranslation;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        # Get the File Name
        $fileName = $this->argument('fileName');

        # Explode the file to get The extension
        $explodedFile = explode('.', $fileName);
    
        # Count the Explode Array File 
        $countExplode = count($explodedFile);

        # Get the Extension Of File
        $extension = $explodedFile[$countExplode - 1];
        
        if($extension != 'csv') {
            $this->info('Please Upload CSV File');
        } else {
            # Get all the Files form Storage Folder
            $allFiles = mainFile::allFiles(storage_path('app/productDetail'));

            $fileNames = [];

            # Get the names of all the Files in Storage Folder
            foreach ($allFiles as $key => $file) {
              array_push($fileNames, $file->getFileName());
            }

            # check wheter File is Present inside Storage/patientFiles Folder or Not
            if(!in_array($fileName, $fileNames)) {
                $this->info('File not present in Storage Folder');
            } else {
                # Get the File
                $file = storage_path('app/productDetail/'.$fileName);

                # Load The file in Excel and Extract the File
                Excel::load($file, function ($reader) {
                    
                  foreach ($reader->toArray() as $key => $customer) {
                    # get description of product.
                 
                    $resultDescription = $this->setDescription($customer);
                   
                    #defining the variable
                    $shortDescription           =   '';
                    $style1                     =   '';
                    $style2                     =   '';
                    $color                      =   '';
                    $productType                =   '';
                    $item                       =   '';
                    
    
                    if(!empty($customer['short_description'])) {
                        $shortDescription = $customer['short_description'];
                    }
                    
                    
                    $data = [
                            'slug'              =>   $customer['name'] ?? '',
                            'price'             =>   $customer['price'] ?? '',
                            'description'       =>   $resultDescription['description'] ?? '',
                            'short_description' =>   $shortDescription ?? '',
                            'is_active'         =>   '1',
                            'manage_stock'      =>   '0',
                            'in_stock'          =>   '1',
                            'qty'               =>   $customer['qty'] ?? '1',
                            'name'              =>   $customer['name'] ?? '',
                            'item_number'       =>   $customer['item'] ?? '',
                            ];
    
                    # store newly created product data
                    $productid = $this->product->create($data);

                    # create category and subcategory with his product category relation and category translation.
                    $this->storeCategory($customer['category'], $customer['subcategory'], $productid->id);
                  
                    # upload the files (Refrence and image file).
                    $result = $this->storeAndUploadFiles( $productid, $customer);
                   

                    #uplode color, Product_Type,style1 and  style2
                    $this->storeFeature( $customer, $productid->id );
                     
                  } // end foreach
                });

                unlink($file);
                $this->info('Product Data has been stored Successfully.');
            }
        }
    }

    /**
     * set description data.s
     * 
     * @param $customer Array.
     */
    public function setDescription($customer)
    { 
        $longDescription         =    '';
        $productDimension        =    '';
        $material                =    '';
        $material1               =    '';
        $material2               =    '';
        $feature1                =    '';
        $feature2                =    '';
        $feature3                =    '';
        $feature4                =    '';
        $feature5                =    '';
        $feature6                =    '';
        $feature7                =    '';
        $feature8                =    '';
        $feature9                =    '';
        $feature10               =    '';
        $summary                 =    '';

        if(!empty($customer['long_description'])) {
            $longDescription = '<p>'.$customer['long_description'].'</p>';
        }
        
        $ul     = "<ul>";
        $endUl  =  "</ul><p>&nbsp</p>";

        

        if(!empty($customer['product_dimension_inch'])) {
           $productDimension  = '<li>Product Dimension (Inch) : '.
                                $customer['product_dimension_inch'].'</li>'; 
        }

        if(!empty($customer['material'])){
           $material   = '<li>Material : '.$customer['material'].'</li>'; 
        }

        if(!empty($customer['material_1'])) {
          $material1     = '<li> Material %1 :' . $customer['material_1'] . '</li>';
        }

        if(!empty($customer['material_2'])) {
          $material2   = '<li>Material %2 :'. $customer['material_2'] . '</li>'; 
        }

        if(!empty($customer['summary'])) {
          $summary  = '<li>Summary :'. $customer['summary'] . '</li>';
        }

        if(!empty($customer['feature_1'])) {
          $feature1    = $customer['feature_1' ] . ','; 
        } 

        if(!empty($customer['feature_2'])) {
          $feature2    = $customer['feature_2'].','; 
        }else {
           $feature2 = '';
        }
        if(!empty($customer['feature_3'])) {
          $feature3    = $customer['feature_3'].','; 
        }

        if(!empty($customer['feature_4'])) {
          $feature4   = $customer['feature_4'].','; 
        }

        if(!empty($customer['feature_5'])) {
          $feature5   = $customer['feature_5'].','; 
        }

        if(!empty($customer['feature_6'])) {
          $feature6   = $customer['feature_6'].','; 
        }

        if(!empty($customer['feature_7'])) {
          $feature7   = $customer['feature_7'].','; 
        }

        if(!empty($customer['feature_8'])) {
          $feature8   = $customer['feature_8'].','; 
        }

       if(!empty($customer['feature_9'])) {
          $feature9   = $customer['feature_9'].','; 
        }

        if(!empty($customer['feature_10'])) {
          $feature10  = $customer['feature_10'].','; 
        }
        

        $features     = '<li>Features : '. $feature1 . $feature2 . $feature3 
                                         . $feature4 . $feature5 . $feature6 
                                         . $feature7 . $feature8 . $feature9 
                                         . $feature10 .
                        '</li>'; 

        $description =  $longDescription  . $ul . 
                        $productDimension . $material . $material1 . $material2 . 
                        $features . $summary .
                        $endUl;

        return ['description'  =>  $description];
    }

   /**
    * create new category
    * 
    * @param category name.
    */
   public function storeCategory($categoryName, $subCategoryName, $productId)
   { 
     $categorySlugName = slugify($categoryName);

     $category = $this->category->where('slug', $categorySlugName)->first();

     if (!empty($category)) {
        # create the product category.
        $this->storeProductCategory($category->id, $productId);

        if (!empty($subCategoryName)) {
          $this->storeSubCategory($subCategoryName, $category->id, $productId);
        }

      } else {

        if (!empty($categoryName)) {
           #set data for product and is category
           $productData = [
                            'slug'           =>  $categoryName,
                            'is_searchable'  =>  1,
                            'is_active'      =>  1,
                          ];
           
            #storing newly created product_category                          
            $categoryId = $this->category->create($productData);
           
            # generate category translation
            $this->storeCatgeoryTranslation($categoryId);

            # create the product category.
            $this->storeProductCategory($categoryId->id, $productId);

            if (!empty($subCategoryName)) {
              $this->storeSubCategory($subCategoryName, $categoryId->id, $productId);
            }
        }
     }
   }

   /**
    * create new sub-category
    * 
    * @param sub-category name.
    */
   public function storeSubCategory($subCategoryName, $categoryId, $productId)
   {
      $subCategorySlugName = slugify($subCategoryName);

      $subCategory = $this->category
                        ->where('slug', $subCategorySlugName)
                        ->where('parent_id', $categoryId)
                        ->first();

      if (!empty($subCategory)) {
          # create the product category.
          $this->storeProductCategory($subCategory->id, $productId);

      } else {

          if (!empty($subCategoryName)) {
            #set data for product and is category
            $productData = [
                            'parent_id'      =>  $categoryId,
                            'slug'           =>  $subCategoryName,
                            'is_searchable'  =>  0,
                            'is_active'      =>  1,
                          ];

            #storing newly created product_category                          
            $subCategoryId = $this->category->create($productData);

            # generate category translation
            $this->storeCatgeoryTranslation($subCategoryId);

            # create the product category.
            $this->storeProductCategory($subCategoryId->id, $productId);
          }
      }
   }

   /**
    * store category lang translation data.
    * 
    * @param $category Array
    */
   public function storeCatgeoryTranslation($category)
   {
      $catgeoryTranslationData  = [
                                  'category_id'  =>  $category->id,
                                  'locale'       =>  'en',
                                  'name'         =>  $category->slug,
                                  ];

      $this->categoryTranslation->create($catgeoryTranslationData);
   }

   /**
    * store product and category relation
    * 
    * @param category id, product id.
    */
   public function storeProductCategory($categoryId, $productId)
   {
      $productCategory  =  $this->product_categories
                                ->where('category_id', $categoryId)
                                ->where('product_id', $productId)
                                ->first();

      if (empty($productCategory)) {
        #set data for product and is category
        $productData =  [
                          'product_id'   => $productId,
                          'category_id'  => $categoryId,
                        ];

        #storing newly created product_category                          
        $this->product_categories->create($productData);
      }
   }

   /**
    * Store and upload the files.
    * 
    * @param $image array, $productId
    */
   public function storeAndUploadFiles($productId, $image )
   {
      if (!empty($image['reference_image'])) {

        $result  = $this->fileExistsOrNot($image['reference_image']);

        if ($result['message'] == 1) {
            # store file 
            $this->storeFileNameAndUrl( 'base_image', $result, $productId );
        }
      }

      if (!empty($image['image_1'])) {

        $result  = $this->fileExistsOrNot($image['image_1']);
        
        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_2'])) {

        $result  = $this->fileExistsOrNot($image['image_2']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_3'])) {

        $result  = $this->fileExistsOrNot($image['image_3']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_4'])) {

        $result  = $this->fileExistsOrNot($image['image_4']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_5'])) {

        $result  = $this->fileExistsOrNot($image['image_5']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_6'])) {

        $result  = $this->fileExistsOrNot($image['image_6']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_7'])) {

        $result  = $this->fileExistsOrNot($image['image_7']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_8'])) {

        $result  = $this->fileExistsOrNot($image['image_8']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_9'])) {

        $result  = $this->fileExistsOrNot($image['image_9']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_10'])) {

        $result  = $this->fileExistsOrNot($image['image_10']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }
   }
   
   /**
    * check file exist or not
    * 
    * @param File name.
    */
   public function fileExistsOrNot($fileName)
   {    

      if ( file_exists(storage_path('app/public/media1/' . $fileName)) ) {

        # Get the File
        $file = storage_path('app/public/media1/'. $fileName);

        # Explode the file to get The extension
        $explodedFile = explode('.', $file);

        # get file actual name for set path
        $pathFileName = explode('media1', $file);

        $output  =  [
                      'message'   =>  1,
                      'file_name' =>  $fileName,
                      'extension' =>  end($explodedFile),
                      'size'      =>  filesize($file),
                      'path'      =>  'media1' . end($pathFileName),
                      'mime'      =>   mime_content_type($file)
                    ];

        return $output;

      } else {
        return ['message' => 2];
      } 
   }

   /**
    * store the file path
    * 
    * @param $type, $result, $productId
    */
   public function storeFileNameAndUrl( $type, $result, $productId )
   {
      $fileData   =  [
                          'user_id'   => '1',
                          'disk'      => 'public_storage',
                          'filename'  =>  $result['file_name'] ?? '',
                          'path'      =>  $result['path'] ?? '',
                          'extension' =>  $result['extension'] ?? '',
                          'mime'      =>  $result['mime'] ?? '',
                          'size'      =>  $result['size'] ?? '',
                     ];

      $file = $this->file->create($fileData);

      # storing newly created file reference to entity table
      $entityData = [
                         'file_id'       =>  $file->id,
                         'entity_type'   =>  'Modules\Product\Entities\Product',
                         'entity_id'     =>  $productId->id ?? '',
                         'zone'          =>  $type ?? '',
                     ];

      $this->entityFiles->create($entityData);
   }

   /**
    * Store feature
    * 
    * @param
    */
   public function storeFeature($colorStyle, $productId)
   {
      # set the specification variable.
      $specificationAttributeSetTranslation = 'Specifications';

      # check name alrady in db or not.
      $checkSpecification  =  $this->attributeSetTranslation
                                    ->where('name', $specificationAttributeSetTranslation)
                                    ->first();

      # check is exist or not.
      if (!empty($checkSpecification)) {
        
        $this->storeColorSizeStyleAttribute($colorStyle, $productId, $checkSpecification->attribute_set_id);

      } else {

        $attributeSet = $this->attributeSet->create();

        $attributeSetTranslationData =  [ 
                                          'attribute_set_id' =>  $attributeSet->id ?? '',
                                          'locale'           =>  'en',
                                          'name'             =>  $specificationAttributeSetTranslation,
                                        ];

        $attributeSetTranslationId  =  $this->attributeSetTranslation->create($attributeSetTranslationData);

        $this->storeColorSizeStyleAttribute($colorStyle, $productId, $attributeSetTranslationId->attribute_set_id);
      }
   }

   /**
    * store specification attributes.
    * 
    * @param
    */
   public function storeColorSizeStyleAttribute($colorStyle, $productId, $attributeSetId)
   {
      $color         = 'Color';
      $size          = 'Size'; # product type
      $style         = 'Style';
      $style1        = 'Style1';
      $item_number   ='Item Number';
      $shippingLengthInch      =    'Shipping Length (Inch)';
      $shippingWidthInch       =    'Shipping Width (Inch)';
      $shippingHeight          =    'Shipping Height (Inch)';
      $shippingVolume          =    'Shipping Volume (CuFt)';
      $shippingWeight          =    'Shipping Weight (LB)';

      $colorId  =  $this->attributeTranslation->where('name', $color)->first();

      if (!empty($colorId)) {
        $this->storeAttributeValue($colorId->attribute_id, $colorStyle, $productId, $color);
      } else {
        $this->storeNewAttribute($attributeSetId, $color, $colorStyle, $productId);
      }

      $sizeId  =  $this->attributeTranslation->where('name', $size)->first();
      if (!empty($sizeId)) {
        $this->storeAttributeValue($sizeId->attribute_id, $colorStyle, $productId, $size);
      } else {
        $this->storeNewAttribute($attributeSetId, $size, $colorStyle, $productId);
      }

      $styleId  =  $this->attributeTranslation->where('name', $style)->first();
      if (!empty($styleId)) {
        $this->storeAttributeValue($styleId->attribute_id, $colorStyle, $productId, $style);
      } else {
        $this->storeNewAttribute($attributeSetId, $style, $colorStyle, $productId);
      }

      $styleId1  =  $this->attributeTranslation->where('name', $style1)->first();
      if (!empty($styleId1)) {
        $this->storeAttributeValue($styleId1->attribute_id, $colorStyle, $productId, $style1);
      } else {
        $this->storeNewAttribute($attributeSetId, $style1, $colorStyle, $productId);
      }
      

      $shippingLengthInchId  =  $this->attributeTranslation->where('name', $shippingLengthInch)->first();
      if (!empty($shippingLengthInchId)) {
        $this->storeAttributeValue($shippingLengthInchId->attribute_id, $colorStyle, $productId, $shippingLengthInch);
      } else {
        $this->storeNewAttribute($attributeSetId, $shippingLengthInch, $colorStyle, $productId);
      }

      $shippingWidthInchId  =  $this->attributeTranslation->where('name', $shippingWidthInch)->first();
      if (!empty($shippingWidthInchId)) {
        $this->storeAttributeValue($shippingWidthInchId->attribute_id, $colorStyle, $productId, $shippingWidthInch);
      } else {
        $this->storeNewAttribute($attributeSetId, $shippingWidthInch, $colorStyle, $productId);
      }

      $shippingHeightId  =  $this->attributeTranslation->where('name', $shippingHeight)->first();
      if (!empty($shippingHeightId)) {
        $this->storeAttributeValue($shippingHeightId->attribute_id, $colorStyle, $productId, $shippingHeight);
      } else {
        $this->storeNewAttribute($attributeSetId, $shippingHeight, $colorStyle, $productId);
      }

      $shippingVolumeId  =  $this->attributeTranslation->where('name', $shippingVolume)->first();
      if (!empty($shippingVolumeId)) {
        $this->storeAttributeValue($shippingVolumeId->attribute_id, $colorStyle, $productId, $shippingVolume);
      } else {
        $this->storeNewAttribute($attributeSetId, $shippingVolume, $colorStyle, $productId);
      }

      $shippingWeightId  =  $this->attributeTranslation->where('name', $shippingWeight)->first();
      if (!empty($shippingWeightId)) {
        $this->storeAttributeValue($shippingWeightId->attribute_id, $colorStyle, $productId, $shippingWeight);
      } else {
        $this->storeNewAttribute($attributeSetId, $shippingWeight, $colorStyle, $productId);
      }

      /* $item_numberId  =  $this->attributeTranslation->where('name', $item_number)->first();
      if (!empty($item_numberId)) {
        $this->storeAttributeValue($item_numberId->attribute_id, $colorStyle, $productId, $item_number);
      } else {
        $this->storeNewAttribute($attributeSetId, $item_number, $colorStyle, $productId);
      }*/


   }

   /**
    * store the color size
    * 
    * @param 
    */
   public function storeNewAttribute($attributeSetId, $name, $colorStyle, $productId)
   {
      $attributes =   [ 
                        'attribute_set_id'  =>  $attributeSetId ?? '',
                        'is_filterable'     =>  1,
                      ];

      $attributesId  = $this->attribute->create($attributes);

      $attributeTranslationData = [ 
                                  'attribute_id'     =>  $attributesId->id ?? '',
                                  'locale'           =>  'en',
                                  'name'             =>  $name,
                                  ];

      $attributeTranslationId  =  $this->attributeTranslation->create($attributeTranslationData);

      $this->storeAttributeValue($attributesId->id, $colorStyle, $productId, $name);

   }
   
   /**
    * Store the attribute value.
    * 
    * @param
    */
   public function storeAttributeValue($attributesId, $colorStyle, $productId, $type)
   {
      if ($type == 'Color') {
        if(!empty($colorStyle['color'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['color'], $productId);
        }
      }

      if ($type == 'Size') {
        if(!empty($colorStyle['product_type'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['product_type'], $productId);
        }
      }

      if ($type == 'Style') {
        if(!empty($colorStyle['style_1'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['style_1'], $productId);
        }
      }

      if ($type == 'Style1') {
        if(!empty($colorStyle['style_2'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['style_2'], $productId);
        }
      }

      if ($type == 'Shipping Length (Inch)') {
        if(!empty($colorStyle['shipping_length_inch'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['shipping_length_inch'], $productId);
        }
      }

      if ($type == 'Shipping Width (Inch)') {
        if(!empty($colorStyle['shipping_width_inch'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['shipping_width_inch'], $productId);
        }
      }

      if ($type == 'Shipping Height (Inch)') {
        if(!empty($colorStyle['shipping_height_inch'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['shipping_height_inch'], $productId);
        }
      }

      if ($type == 'Shipping Volume (CuFt)') {
        if(!empty($colorStyle['shipping_volume_cuft'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['shipping_volume_cuft'], $productId);
        }
      }

      if ($type == 'Shipping Weight (LB)') {
        if(!empty($colorStyle['shipping_weight_lb'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['shipping_weight_lb'], $productId);
        }
      }

      /* if ($type == 'Item Number') {
        if(!empty($colorStyle['item'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['item'], $productId);
        }
      }*/
   }

   /**
    * store attribute value.
    * 
    *
    */
   public function storeAttributeValueTemplate($attributesId, $value, $productId)
   {
      if(!empty($value)) {

        $translationValue = slugify($value);

        $attributeTranslationValue = $this->attributeValueTranslation
                                          ->where('value', $translationValue)
                                          ->first();

        if(empty($attributeTranslationValue)) {

          $attributeValue  =  ['attribute_id'  =>  $attributesId, 'position'  => 0];

          $attributeValueId =  $this->attributeValue->create($attributeValue);
          
          $attributeValueTranslation  =  [
                                         'attribute_value_id'  =>  $attributeValueId->id, 
                                         'value'               =>  $value ?? '', 
                                         'locale'              =>  'en'
                                         ];
           
          $attributeValueTranslationId = $this->attributeValueTranslation
                                                          ->updateOrCreate($attributeValueTranslation);

          $this->storeRelationProductWithValue($attributesId, $productId, $attributeValueId->id);                                                

        } else {

          $this->storeRelationProductWithValue($attributesId,
                                               $productId, 
                                               $attributeTranslationValue->attribute_value_id
                                              );

        }
      }
          
   }

   /**
    * store relation of product with value.
    * 
    * @param
    */
   public function storeRelationProductWithValue($attributesId, $productId, $attributeValueId)
   {
      /*
      $attributeValue  =  ['attribute_id'  =>  $attributesId, 'product_id'  => $productId];
      
      $productAttributeId  =  $this->productAttribute->create($attributeValue);*/
      $productAttributeId  =  new ProductAttribute();
      $productAttributeId->attribute_id =  $attributesId;
      $productAttributeId->product_id   =   $productId;
      $productAttributeId->save();

     /* $productAttributeValueDatas  =  [
                                          'product_attribute_id'  =>  $productAttributeId->id, 
                                          'attribute_value_id'    =>  $attributeValueId
                                      ];*/
      $productAttributeValue  = new ProductAttributeValue();    
      $productAttributeValue->product_attribute_id = $productAttributeId->id;
      $productAttributeValue->attribute_value_id   =  $attributeValueId;
      $productAttributeValue->save();
      
      /*$this->productAttributeValue->create($productAttributeValueDatas);*/
   }
}
