<?php
namespace FleetCart\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller;
use FleetCart\CustomHelpers\CustomResponse;
use FleetCart\CustomHelpers\CustomException;
use FleetCart\CustomHelpers\CustomRequest;

use Modules\Brand\Entities\Brand;

class BrandController extends Controller
{
    public function list(Request $request)
    {
        try {
            $response = new CustomResponse();
            $brands = Brand::list_api();
            $response->data["brands"] = $brands;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }

    public function get(Request $request)
    {
        try {
            $response = new CustomResponse();
            // validations start
            $rules = [
                "slug"=> ["required","max:190"],
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            $slug = $params["slug"];
            $brand = Brand::get_api($slug);
            if (is_null($brand)) {
                throw new CustomException("Brand Not Found");
            }
            $response->data["brand"] = $brand;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }
}
