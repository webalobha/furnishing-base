<?php
namespace FleetCart\Http\Controllers\Api;

use Exception;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller;
use FleetCart\CustomHelpers\CustomResponse;
use FleetCart\CustomHelpers\CustomException;
use FleetCart\CustomHelpers\CustomRequest;
use Modules\Product\Entities\Product;
use Modules\ShoppingCart\Entities\ShoppingCart;
use Modules\ShoppingCart\Entities\ShoppingCartItem;

class ShoppingCartController extends Controller
{
    public function get(Request $request)
    {
        try {
            $response = new CustomResponse();
            $shopping_cart = ShoppingCart::firstOrCreate([
                "user_id"=>Auth::user()->id
            ]);
            $shopping_cart->load('shopping_cart_items');
            $response->data["shopping_cart"] = $shopping_cart;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }

    public function item_add(Request $request)
    {
        try {
            $response = new CustomResponse();
            // validations start
            $rules = [
                "slug"=> ["required","max:190"],
                "qty"=> ["required","integer"]
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            $product = Product::where(["slug"=>$params["slug"]])->first();
            if (is_null($product)) {
                throw new CustomException("Product not found.");
            }
            if ($product->manage_stock) {
                if (!$product->in_stock || !$product->qty) {
                    throw new CustomException("Product out of stock");
                } elseif ($product->qty < $params["qty"]) {
                    throw new CustomException("Sorry, we have only $product->qty quantity in stock for $product->name");
                }
            } else {
                if (!$product->in_stock) {
                    throw new CustomException("Product out of stock");
                }
            }
            $shopping_cart = ShoppingCart::firstOrCreate([
                "user_id"=>Auth::user()->id
            ]);
            $shopping_cart_item = ShoppingCartItem::firstOrCreate([
                "shopping_cart_id"=>$shopping_cart->id,
                "product_id"=>$product->id
            ]);
            $shopping_cart_item->qty = $params["qty"];
            $shopping_cart_item->save();
            $shopping_cart->load("shopping_cart_items");
            $response->data["shopping_cart"] = $shopping_cart;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }

    public function item_remove(Request $request)
    {
        try {
            $response = new CustomResponse();
            // validations start
            $rules = [
                "slug"=> ["required","max:190"]
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            $product = Product::where(["slug"=>$params["slug"]])->first();
            if (is_null($product)) {
                throw new CustomException("Product not found.");
            }
            $shopping_cart = ShoppingCart::where([
                "user_id"=>Auth::user()->id
            ])->first();
            $shopping_cart_item = $shopping_cart->shopping_cart_items()->where([
                "shopping_cart_id"=>$shopping_cart->id,
                "product_id"=>$product->id
            ])->first();
            if (is_null($shopping_cart_item)) {
                throw new CustomException("Shopping cart does not contain this Product.");
            }
            $shopping_cart_item->delete();
            $shopping_cart->load("shopping_cart_items");
            $response->data["shopping_cart"] = $shopping_cart;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }
}
