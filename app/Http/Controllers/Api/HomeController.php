<?php
namespace FleetCart\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use FleetCart\CustomHelpers\CustomResponse;
use FleetCart\CustomHelpers\CustomException;
use FleetCart\CustomHelpers\CustomRequest;

use Modules\Slider\Entities\Slider;
use Modules\Brand\Entities\Brand;
use Modules\FeaturedCategory\Entities\FeaturedCategory;
use Modules\Product\Entities\Product;
use Modules\FlashSale\Entities\FlashSale;

class HomeController extends Controller
{
    public function core()
    {
        $response = new CustomResponse();
        try {
            $slider = Slider::findWithSlides(setting("storefront_slider"));
            $brands = Brand::list_api();
            $categories_top = FeaturedCategory::list_api("mobile_home_top");
            $categories_bottom = FeaturedCategory::list_api("mobile_home_bottom");
            $products_latest = Product::newArrivals(10);
            $deal_of_the_day = FlashSale::deal_of_the_day();
            $flash_sales = FlashSale::start_to_end()->sortBy("end_datetime")->values()->all();

            $response->data["slider"] = $slider;
            $response->data["brands"] = $brands;
            $response->data["categories"] = [
                "top"=> $categories_top,
                "bottom"=> $categories_bottom
            ];
            $response->data["products"] = [
                "latest"=> $products_latest
            ];
            $response->data["deal_of_the_day"] = $deal_of_the_day;
            $response->data["flash_sales"] = $flash_sales;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }
}
