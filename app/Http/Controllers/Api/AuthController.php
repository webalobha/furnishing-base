<?php

namespace FleetCart\Http\Controllers\Api;

use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Rule;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Modules\User\Entities\User;
use FleetCart\CustomHelpers\CustomResponse;
use FleetCart\CustomHelpers\CustomException;
use FleetCart\CustomHelpers\CustomRequest;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $response = new CustomResponse();
        try {
            // validations start
            $rules = [
                "contact"=> ["required","max:100"],
                "password"=> ["max:100"],
                "type"=> ["required","max:100",Rule::in([ConstantCommon["auth_types"]["plain"]["slug"], ConstantCommon["auth_types"]["oauth"]["slug"]])]
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            $user = User::where(["email"=>$params["contact"]])->orWhere(["mobile_number"=>$params["contact"]])->first(["id","email","password","first_name","last_name"]);
            if (!$user || ($params["type"]==ConstantCommon["auth_types"]["plain"]["slug"] && (!isset($params["password"]) || !Hash::check($params["password"], $user->password)))) {
                throw new CustomException("Invalid credentials");
            }
            Auth::login($user);
            $user->api_token = Str::random(60);
            $user->save();
            $response->data["user"] = $user;
            $response->message = "Login successful";
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }

    public function register(Request $request)
    {
        $response = new CustomResponse();
        try {
            $rules = [
                "first_name"=> ["max:100"],
                "last_name"=> ["max:100"],
                "email"=> ["email","max:100"],
                "mobile_number"=>["max:100"],
                "password"=>["max:100"],
                "type"=> ["required","max:100",Rule::in([ConstantCommon["auth_types"]["plain"]["slug"], ConstantCommon["auth_types"]["oauth"]["slug"]])]
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            if (!isset($params["email"]) && !isset($params["mobile_number"])) {
                throw new CustomException("Either email or mobile number is required");
            }
            if ($params["type"]==ConstantCommon["auth_types"]["plain"]["slug"] && !isset($params["password"])) {
                throw new CustomException("Password is required");
            }
            if ($params["type"]==ConstantCommon["auth_types"]["oauth"]["slug"] && !isset($params["email"])) {
                throw new CustomException("Email is required for oauth type");
            }
            $contact = isset($params["email"]) ? "email" : "mobile_number";
            if (User::where([$contact=>($params[$contact])])->exists()) {
                throw new CustomException("Account with this ".str_replace("_", " ", $contact)." already exists");
            }

            $user = new User($params);
            $user->password = bcrypt($params["password"] ?? Str::random());
            $user->api_token = Str::random(60);
            $user->save();
            $response->data["user"] = $user;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }
}
