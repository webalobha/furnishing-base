<?php
namespace FleetCart\Http\Controllers\Api;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Routing\Controller;
use FleetCart\CustomHelpers\CustomResponse;
use FleetCart\CustomHelpers\CustomException;
use FleetCart\CustomHelpers\CustomRequest;
use Modules\Product\Entities\Product;
use Modules\Product\Filters\ProductFilter;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    public function list(Product $model, ProductFilter $productFilter)
    {
        try
        {
            $request = request();
            $response = new CustomResponse();
            // validations start
            $rules = [
                "query"=> ["max:190"],
                "category"=> ["max:190", "exists:categories,slug"],
                "sort"=> ["max:190", Rule::in(['relevance', 'alphabetic', 'topRated', 'latest', 'priceLowToHigh', 'priceHighToLow'])],
                "fromPrice"=> ["integer"],
                "toPrice"=> ["integer"]
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end

            $productIds = [];
            if (isset($params["query"])) {
                $model = $model->search($params["query"]);
                $productIds = $model->keys();
            }
            $query = $model->filter($productFilter);
            if (isset($params["category"])) {
                $productIds = (clone $query)->select('products.id')->resetOrders()->pluck('id');
            }
            $products = $query->paginate(request('perPage', 15))
                ->appends(request()->query());

            $response->data["products"] = $products;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }

    public function get(Request $request)
    {
        try {
            $response = new CustomResponse();
            // validations start
            $rules = [
                "slug"=> ["required","max:190"],
            ];
            $custom_request = new CustomRequest($request, $rules);
            $validator = $custom_request->validate();
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
            $params = $validator->valid();
            // validations end
            $slug = $params["slug"];

            $product = Product::findBySlug($slug);
            $relatedProducts = $product->relatedProducts()->forCard()->get();
            $upSellProducts = $product->upSellProducts()->forCard()->get();
            $reviews = $product->reviews()->get();

            $response->data["product"] = $product;
            $response->data["related_products"] = $relatedProducts;
            $response->data["up_sell_products"] = $upSellProducts;
            $response->data["reviews"] = $reviews;
        } catch (Exception $e) {
            CustomException::handle_exception($e, $response, $validator ?? null);
        }
        return response()->json($response->get(), $response->status);
    }
}
