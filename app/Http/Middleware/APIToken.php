<?php

namespace FleetCart\Http\Middleware;

use Auth;
use Modules\User\Entities\User;

use Closure;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header("Authorization")) {
            $api_token = explode(" ", $request->header("Authorization"))[1] ?? "";
            $user = User::where(["api_token"=> $api_token])->first();
            if (!$user) {
                return response()->json([
                    "message" => "Invalid Authorization Token.",
                ], 401);
            }
            Auth::login($user);
            return $next($request);
        }
        return response()->json([
            "message" => "Authorization Token Header Missing.",
        ], 401);
    }
}
