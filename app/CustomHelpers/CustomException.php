<?php

namespace FleetCart\CustomHelpers;

use Exception;
use Illuminate\Validation\ValidationException;

class CustomException extends Exception
{
    public $status = null;
    public $message = null;

    public function __construct($message, $status=400)
    {
        $this->message = $message;
        $this->status = $status;
    }

    public static function handle_exception($e, $response, $validator=null)
    {
        if ($e instanceof ValidationException) {
            $response->add_errors($validator->errors()->all());
        } elseif ($e instanceof self) {
            $response->add_errors([$e->message]);
            $response->status = $e->status;
        } else {
            $response->add_errors([$e->getMessage()]);
            $response->status = 500;
        }
    }
}
