<?php
namespace FleetCart\CustomHelpers;

use Log;

class CustomResponse
{
    public $data = [];
    public $errors = [];
    public $message = null;
    public $status = null;

    public function add_errors($errors)
    {
        $this->errors = array_merge($this->errors, $errors);
    }

    public function get($status=null)
    {
        $this->status = $this->status ?? $status;
        if (!empty($this->errors)) {
            $this->message = $this->errors[0];
            $this->status = $this->status ?? 400;
        }
        if ($this->status==500) {
            if (config('app.debug') == false) {
                $this->message="Internal Server Error";
            }
        }
        if ($this->errors) {
            Log::info($this->errors);
        }
        $this->status = $this->status ?? 200;
        return $this;
    }
}
