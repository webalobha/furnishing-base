<?php

namespace FleetCart\CustomHelpers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CustomRequest
{
    protected static $base_messages = [
        "required"=> "The :attribute field is required.",
        "in"=> "The :attribute value must be one of [:values].",
        "max"=> "The :attribute value must be of :value length.",
        "email"=> "The :attribute format is invalid.",
        "exists"=> "This :attribute does not exists.",
        "integer"=> "The :attribute value must be an integer"
    ];
    
    protected static $base_attributes = [
        "contact"=> "Contact",
        "password"=> "Password",
        "type"=> "Type",
        "first_name"=> "First Name",
        "last_name"=> "Last Name",
        "email"=>"Email",
        "mobile_number"=>"Mobile Number"
    ];

    public $request = null;
    public $rules = [];
    public $messages = [];
    public $attributes = [];

    public function __construct($request, $rules, $messages=null, $attributes=null)
    {
        $this->request = $request;
        $this->rules = $rules;
        $this->messages = $messages ?? self::$base_messages;
        $this->attributes = $attributes ?? self::$base_attributes;
    }
    
    public function validate()
    {
        $request_params = $this->request->all();
        $request_params = array_filter($request_params, function ($value) {
            return trim($value);
        });
        $validator = Validator::make($request_params, $this->rules, $this->messages, $this->attributes);
        return $validator;
    }
    public function get_base_messages()
    {
        return self::$base_messages;
    }
    public function get_base_attributes()
    {
        return self::$base_attributes;
    }
}
