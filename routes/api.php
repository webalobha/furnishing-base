<?php

//auth
Route::post('api/auth/login', 'Api\AuthController@login')->name('api.auth.login');
Route::post('api/auth/register', 'Api\AuthController@register')->name('api.auth.register');

// home
Route::post('api/home/core', 'Api\HomeController@core')->name('api.home.core');

// brands
Route::post('api/brands/list', 'Api\BrandController@list')->name('api.brands.list');
Route::post('api/brands/get', 'Api\BrandController@get')->name('api.brands.get');

//products
Route::post('api/products/list', 'Api\ProductController@list')->name('api.products.list');
Route::post('api/products/get', 'Api\ProductController@get')->name('api.products.get');

//cart
Route::middleware(['APIToken'])->group(function () {
    Route::post('api/shopping_cart/get', 'Api\ShoppingCartController@get')->name('api.shopping_cart.get');
    Route::post('api/shopping_cart/item/add', 'Api\ShoppingCartController@item_add')->name('api.shopping_cart.item.add');
    Route::post('api/shopping_cart/item/remove', 'Api\ShoppingCartController@item_remove')->name('api.shopping_cart.item.remove');
});
