-- MySQL dump 10.13  Distrib 5.7.26, for linux-glibc2.12 (x86_64)
--
-- Host: localhost    Database: laraecom
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES UTF8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activations_user_id_index` (`user_id`),
  CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'njYmkYg4Ym9sNTvgB4CM1V9jjpEE8nQr',1,'2019-06-20 08:16:32','2019-06-20 02:46:32','2019-06-20 02:46:32');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_categories`
--

DROP TABLE IF EXISTS `attribute_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_categories` (
  `attribute_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`attribute_id`,`category_id`),
  KEY `attribute_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `attribute_categories_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `attribute_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_categories`
--

LOCK TABLES `attribute_categories` WRITE;
/*!40000 ALTER TABLE `attribute_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_set_translations`
--

DROP TABLE IF EXISTS `attribute_set_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_set_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_set_translations_attribute_set_id_locale_unique` (`attribute_set_id`,`locale`),
  CONSTRAINT `attribute_set_translations_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_set_translations`
--

LOCK TABLES `attribute_set_translations` WRITE;
/*!40000 ALTER TABLE `attribute_set_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_set_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_sets`
--

DROP TABLE IF EXISTS `attribute_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_sets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_sets`
--

LOCK TABLES `attribute_sets` WRITE;
/*!40000 ALTER TABLE `attribute_sets` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_sets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_translations`
--

DROP TABLE IF EXISTS `attribute_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_translations_attribute_id_locale_unique` (`attribute_id`,`locale`),
  CONSTRAINT `attribute_translations_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_translations`
--

LOCK TABLES `attribute_translations` WRITE;
/*!40000 ALTER TABLE `attribute_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_value_translations`
--

DROP TABLE IF EXISTS `attribute_value_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_value_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_value_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attribute_value_translations_attribute_value_id_locale_unique` (`attribute_value_id`,`locale`),
  CONSTRAINT `attribute_value_translations_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_value_translations`
--

LOCK TABLES `attribute_value_translations` WRITE;
/*!40000 ALTER TABLE `attribute_value_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_value_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attribute_values`
--

DROP TABLE IF EXISTS `attribute_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attribute_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_id` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_values_attribute_id_index` (`attribute_id`),
  CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attribute_values`
--

LOCK TABLES `attribute_values` WRITE;
/*!40000 ALTER TABLE `attribute_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `attribute_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attribute_set_id` int(10) unsigned NOT NULL,
  `is_filterable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attributes_attribute_set_id_index` (`attribute_set_id`),
  CONSTRAINT `attributes_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'apple','Apple','Apple','Discover the innovative world of Apple and shop everything iPhone, iPad, Apple Watch, Mac, and Apple TV, plus explore accessories, entertainment and expert device support.','2019-09-18 12:32:57','2019-09-18 12:32:57'),(2,'nike','Nike','Nike','Nike delivers innovative products, experiences and services to inspire athletes. Free delivery and returns on every order with NikePlus.','2019-09-18 12:33:53','2019-09-18 12:33:53'),(3,'reebok','Reebok','Reebok','Shop for your look with Reebok footwear and apparel. Sign up for 20% off your first order, early access to new products, &amp; free shipping on all future orders.','2019-09-18 12:34:59','2019-09-18 12:34:59');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) unsigned DEFAULT NULL,
  `is_searchable` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,'fashion-men',NULL,1,1,'2019-09-18 12:37:38','2019-09-18 12:37:38'),(2,NULL,'fashion-girl',NULL,1,1,'2019-09-18 12:38:00','2019-09-18 12:38:00'),(3,NULL,'computer',NULL,1,1,'2019-09-18 12:38:31','2019-09-18 12:38:31'),(4,NULL,'smartphone',NULL,1,1,'2019-09-18 12:38:40','2019-09-18 12:38:40'),(5,3,'printer',NULL,1,1,'2019-09-18 12:40:03','2019-09-18 12:40:03'),(6,3,'monitor',NULL,1,1,'2019-09-18 12:40:17','2019-09-18 12:40:17'),(8,1,'clothing',NULL,1,1,'2019-09-18 12:42:25','2019-09-18 12:42:25'),(9,1,'shoes',NULL,0,0,'2019-09-18 12:42:55','2019-09-18 12:42:55'),(10,1,'watches',NULL,1,1,'2019-09-18 12:43:48','2019-09-18 12:43:48');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_translations`
--

DROP TABLE IF EXISTS `category_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_translations`
--

LOCK TABLES `category_translations` WRITE;
/*!40000 ALTER TABLE `category_translations` DISABLE KEYS */;
INSERT INTO `category_translations` VALUES (1,1,'en','Fashion Man'),(2,2,'en','Fashion Girl'),(3,3,'en','Computers'),(4,4,'en','SmartPhones'),(5,5,'en','Printers'),(6,6,'en','Monitors'),(8,8,'en','Clothing'),(9,9,'en','Shoes'),(10,10,'en','Watches');
/*!40000 ALTER TABLE `category_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_categories`
--

DROP TABLE IF EXISTS `coupon_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_categories` (
  `coupon_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_id`,`category_id`,`exclude`),
  KEY `coupon_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `coupon_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `coupon_categories_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_categories`
--

LOCK TABLES `coupon_categories` WRITE;
/*!40000 ALTER TABLE `coupon_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_products`
--

DROP TABLE IF EXISTS `coupon_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_products` (
  `coupon_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`coupon_id`,`product_id`),
  KEY `coupon_products_product_id_foreign` (`product_id`),
  CONSTRAINT `coupon_products_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE,
  CONSTRAINT `coupon_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_products`
--

LOCK TABLES `coupon_products` WRITE;
/*!40000 ALTER TABLE `coupon_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupon_translations`
--

DROP TABLE IF EXISTS `coupon_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupon_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `coupon_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `coupon_translations_coupon_id_locale_unique` (`coupon_id`,`locale`),
  CONSTRAINT `coupon_translations_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupon_translations`
--

LOCK TABLES `coupon_translations` WRITE;
/*!40000 ALTER TABLE `coupon_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupon_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(18,4) unsigned DEFAULT NULL,
  `is_percent` tinyint(1) NOT NULL,
  `free_shipping` tinyint(1) NOT NULL,
  `minimum_spend` decimal(18,4) unsigned DEFAULT NULL,
  `maximum_spend` decimal(18,4) unsigned DEFAULT NULL,
  `usage_limit_per_coupon` int(10) unsigned DEFAULT NULL,
  `usage_limit_per_customer` int(10) unsigned DEFAULT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coupons_code_index` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cross_sell_products`
--

DROP TABLE IF EXISTS `cross_sell_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cross_sell_products` (
  `product_id` int(10) unsigned NOT NULL,
  `cross_sell_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`cross_sell_product_id`),
  KEY `cross_sell_products_cross_sell_product_id_foreign` (`cross_sell_product_id`),
  CONSTRAINT `cross_sell_products_cross_sell_product_id_foreign` FOREIGN KEY (`cross_sell_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `cross_sell_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cross_sell_products`
--

LOCK TABLES `cross_sell_products` WRITE;
/*!40000 ALTER TABLE `cross_sell_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `cross_sell_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency_rates`
--

DROP TABLE IF EXISTS `currency_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `currency_rates_currency_unique` (`currency`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency_rates`
--

LOCK TABLES `currency_rates` WRITE;
/*!40000 ALTER TABLE `currency_rates` DISABLE KEYS */;
INSERT INTO `currency_rates` VALUES (1,'USD',1.0000,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(2,'INR',1.0000,'2019-06-20 08:18:58','2019-06-20 08:18:58');
/*!40000 ALTER TABLE `currency_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_files`
--

DROP TABLE IF EXISTS `entity_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entity_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL,
  `zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `entity_files_entity_type_entity_id_index` (`entity_type`,`entity_id`),
  KEY `entity_files_file_id_index` (`file_id`),
  KEY `entity_files_zone_index` (`zone`),
  CONSTRAINT `entity_files_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_files`
--

LOCK TABLES `entity_files` WRITE;
/*!40000 ALTER TABLE `entity_files` DISABLE KEYS */;
INSERT INTO `entity_files` VALUES (1,6,'Modules\\Brand\\Entities\\Brand',1,'banner_image','2019-09-18 12:32:57','2019-09-18 12:32:57'),(2,7,'Modules\\Brand\\Entities\\Brand',2,'banner_image','2019-09-18 12:33:53','2019-09-18 12:33:53'),(3,8,'Modules\\Brand\\Entities\\Brand',3,'banner_image','2019-09-18 12:34:59','2019-09-18 12:34:59');
/*!40000 ALTER TABLE `entity_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `files_user_id_index` (`user_id`),
  KEY `files_filename_index` (`filename`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (1,1,'assets.zip','public_storage','media/N7EewIME9hEMlAS76FBzO2D4AqR8H0PzLHhklLBi.zip','zip','application/zip','17462258','2019-09-18 12:23:35','2019-09-18 12:23:35'),(2,1,'slider_image_1.jpeg','public_storage','media/jcu2ThPkAyomc0bc89YLsEyPc8MfpLyBrbHKNxot.jpeg','jpeg','image/jpeg','74629','2019-09-18 12:23:55','2019-09-18 12:23:55'),(3,1,'slider_image_2.jpeg','public_storage','media/fpaTLnVw68RIf6eqKLNcShjEQvSo8O3iZSnkXOCe.jpeg','jpeg','image/jpeg','137733','2019-09-18 12:23:55','2019-09-18 12:23:55'),(4,1,'slider_image_3.jpeg','public_storage','media/nOihLrQz6O9F3MUYz0A2lySFawYTvAwsICf74v7H.jpeg','jpeg','image/jpeg','56841','2019-09-18 12:23:55','2019-09-18 12:23:55'),(5,1,'slider_image_4.jpeg','public_storage','media/AKmJEhQyA0tPFV1be5nMHIoGtqjRop47UWpY0bbg.jpeg','jpeg','image/jpeg','145849','2019-09-18 12:23:55','2019-09-18 12:23:55'),(6,1,'apple.png','public_storage','media/WlDauglMqQxERkX9OwA6xOfLC3lACitxfOvQuRvy.png','png','image/png','2225','2019-09-18 12:31:06','2019-09-18 12:31:06'),(7,1,'nike.png','public_storage','media/wPFWikgjYXvVJNEDr7vFEvFtHWY2WMXyltzl2Eo0.png','png','image/png','1979','2019-09-18 12:31:06','2019-09-18 12:31:06'),(8,1,'reebok.png','public_storage','media/fXCvep3KzZTLxgtYkEYeQI7y25OeIt7qtr18QLOL.png','png','image/png','2554','2019-09-18 12:31:06','2019-09-18 12:31:06');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_item_translations`
--

DROP TABLE IF EXISTS `menu_item_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_item_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_item_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_item_translations_menu_item_id_locale_unique` (`menu_item_id`,`locale`),
  CONSTRAINT `menu_item_translations_menu_item_id_foreign` FOREIGN KEY (`menu_item_id`) REFERENCES `menu_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_item_translations`
--

LOCK TABLES `menu_item_translations` WRITE;
/*!40000 ALTER TABLE `menu_item_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_item_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `category_id` int(10) unsigned DEFAULT NULL,
  `page_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) unsigned DEFAULT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT '0',
  `is_fluid` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_items_parent_id_foreign` (`parent_id`),
  KEY `menu_items_category_id_foreign` (`category_id`),
  KEY `menu_items_page_id_foreign` (`page_id`),
  KEY `menu_items_menu_id_index` (`menu_id`),
  CONSTRAINT `menu_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  CONSTRAINT `menu_items_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `menu_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menu_items` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_translations`
--

DROP TABLE IF EXISTS `menu_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menu_translations_menu_id_locale_unique` (`menu_id`,`locale`),
  CONSTRAINT `menu_translations_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_translations`
--

LOCK TABLES `menu_translations` WRITE;
/*!40000 ALTER TABLE `menu_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_data`
--

DROP TABLE IF EXISTS `meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_data_entity_type_entity_id_index` (`entity_type`,`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_data`
--

LOCK TABLES `meta_data` WRITE;
/*!40000 ALTER TABLE `meta_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_data_translations`
--

DROP TABLE IF EXISTS `meta_data_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_data_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meta_data_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_data_translations_meta_data_id_locale_unique` (`meta_data_id`,`locale`),
  CONSTRAINT `meta_data_translations_meta_data_id_foreign` FOREIGN KEY (`meta_data_id`) REFERENCES `meta_data` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_data_translations`
--

LOCK TABLES `meta_data_translations` WRITE;
/*!40000 ALTER TABLE `meta_data_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_data_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_07_02_230147_migration_cartalyst_sentinel',1),(2,'2014_10_14_200250_create_settings_table',1),(3,'2014_10_26_162751_create_files_table',1),(4,'2014_10_30_191858_create_pages_table',1),(5,'2014_11_31_125848_create_page_translations_table',1),(6,'2015_02_27_105241_create_entity_files_table',1),(7,'2015_11_20_184604486385_create_translations_table',1),(8,'2015_11_20_184604743083_create_translation_translations_table',1),(9,'2017_05_29_155126144426_create_products_table',1),(10,'2017_05_30_155126416338_create_product_translations_table',1),(11,'2017_08_02_153217_create_options_table',1),(12,'2017_08_02_153348_create_option_translations_table',1),(13,'2017_08_02_153406_create_option_values_table',1),(14,'2017_08_02_153736_create_option_value_translations_table',1),(15,'2017_08_03_156576_create_product_options_table',1),(16,'2017_08_17_170128_create_related_products_table',1),(17,'2017_08_17_175236_create_up_sell_products_table',1),(18,'2017_08_17_175828_create_cross_sell_products_table',1),(19,'2017_11_09_141332910964_create_categories_table',1),(20,'2017_11_09_141332931539_create_category_translations_table',1),(21,'2017_11_26_083614526622_create_meta_data_table',1),(22,'2017_11_26_083614526828_create_meta_data_translations_table',1),(23,'2018_01_24_125642_create_product_categories_table',1),(24,'2018_02_04_150917488267_create_coupons_table',1),(25,'2018_02_04_150917488698_create_coupon_translations_table',1),(26,'2018_03_11_181317_create_coupon_products_table',1),(27,'2018_03_15_091937_create_coupon_categories_table',1),(28,'2018_04_18_154028776225_create_reviews_table',1),(29,'2018_05_17_115822452977_create_currency_rates_table',1),(30,'2018_07_03_124153537506_create_sliders_table',1),(31,'2018_07_03_124153537695_create_slider_translations_table',1),(32,'2018_07_03_133107770172_create_slider_slides_table',1),(33,'2018_07_03_133107770486_create_slider_slide_translations_table',1),(34,'2018_07_28_190524758357_create_attribute_sets_table',1),(35,'2018_07_28_190524758497_create_attribute_set_translations_table',1),(36,'2018_07_28_190524758646_create_attributes_table',1),(37,'2018_07_28_190524758877_create_attribute_translations_table',1),(38,'2018_07_28_190524759461_create_product_attributes_table',1),(39,'2018_08_01_001919718631_create_tax_classes_table',1),(40,'2018_08_01_001919718935_create_tax_class_translations_table',1),(41,'2018_08_01_001919723551_create_tax_rates_table',1),(42,'2018_08_01_001919723781_create_tax_rate_translations_table',1),(43,'2018_08_03_195922206748_create_attribute_values_table',1),(44,'2018_08_03_195922207019_create_attribute_value_translations_table',1),(45,'2018_08_04_190524764275_create_product_attribute_values_table',1),(46,'2018_08_07_135631306565_create_orders_table',1),(47,'2018_08_07_135631309451_create_order_products_table',1),(48,'2018_08_07_135631309512_create_order_product_options_table',1),(49,'2018_08_07_135631309624_create_order_product_option_values_table',1),(50,'2018_09_11_213926106353_create_transactions_table',1),(51,'2018_09_19_081602135631_create_order_taxes_table',1),(52,'2018_09_19_103745_create_setting_translations_table',1),(53,'2018_10_01_224852175056_create_wish_lists_table',1),(54,'2018_10_04_185608_create_search_terms_table',1),(55,'2018_11_03_160015_create_menus_table',1),(56,'2018_11_03_160138_create_menu_translations_table',1),(57,'2018_11_03_160753_create_menu_items_table',1),(58,'2018_11_03_160804_create_menu_item_translation_table',1),(59,'2019_02_05_162605_add_position_to_slider_slides_table',1),(60,'2019_02_09_164343_remove_file_id_from_slider_slides_table',1),(61,'2019_02_09_164434_add_file_id_to_slider_slide_translations_table',1),(62,'2019_02_14_103408_create_attribute_categories_table',1),(63,'2019_09_12_142200_migration_user_add_mobile_columns',2),(64,'2019_09_12_142201_migration_user_set_email_nullable',2),(65,'2019_09_12_142202_migration_user_set_name_fields_nullable',2),(66,'2019_09_17_001_create_brands_table',3),(67,'2019_09_18_001_migration_add_column_brand_id_to_products_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `option_translations`
--

DROP TABLE IF EXISTS `option_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option_translations_option_id_locale_unique` (`option_id`,`locale`),
  CONSTRAINT `option_translations_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `option_translations`
--

LOCK TABLES `option_translations` WRITE;
/*!40000 ALTER TABLE `option_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `option_value_translations`
--

DROP TABLE IF EXISTS `option_value_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_value_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_value_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option_value_translations_option_value_id_locale_unique` (`option_value_id`,`locale`),
  CONSTRAINT `option_value_translations_option_value_id_foreign` FOREIGN KEY (`option_value_id`) REFERENCES `option_values` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `option_value_translations`
--

LOCK TABLES `option_value_translations` WRITE;
/*!40000 ALTER TABLE `option_value_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_value_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `option_values`
--

DROP TABLE IF EXISTS `option_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` int(10) unsigned NOT NULL,
  `price` decimal(18,4) unsigned DEFAULT NULL,
  `price_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `option_values_option_id_index` (`option_id`),
  CONSTRAINT `option_values_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `option_values`
--

LOCK TABLES `option_values` WRITE;
/*!40000 ALTER TABLE `option_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `option_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL,
  `is_global` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(10) unsigned DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product_option_values`
--

DROP TABLE IF EXISTS `order_product_option_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product_option_values` (
  `order_product_option_id` int(10) unsigned NOT NULL,
  `option_value_id` int(10) unsigned NOT NULL,
  `price` decimal(18,4) unsigned DEFAULT NULL,
  PRIMARY KEY (`order_product_option_id`,`option_value_id`),
  KEY `order_product_option_values_option_value_id_foreign` (`option_value_id`),
  CONSTRAINT `order_product_option_values_option_value_id_foreign` FOREIGN KEY (`option_value_id`) REFERENCES `option_values` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_product_option_values_order_product_option_id_foreign` FOREIGN KEY (`order_product_option_id`) REFERENCES `order_product_options` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product_option_values`
--

LOCK TABLES `order_product_option_values` WRITE;
/*!40000 ALTER TABLE `order_product_option_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_product_option_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product_options`
--

DROP TABLE IF EXISTS `order_product_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product_options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_product_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_product_options_order_product_id_option_id_unique` (`order_product_id`,`option_id`),
  KEY `order_product_options_option_id_foreign` (`option_id`),
  CONSTRAINT `order_product_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_product_options_order_product_id_foreign` FOREIGN KEY (`order_product_id`) REFERENCES `order_products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product_options`
--

LOCK TABLES `order_product_options` WRITE;
/*!40000 ALTER TABLE `order_product_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_product_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_products`
--

DROP TABLE IF EXISTS `order_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `unit_price` decimal(18,4) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `line_total` decimal(18,4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_products_order_id_foreign` (`order_id`),
  KEY `order_products_product_id_foreign` (`product_id`),
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_products`
--

LOCK TABLES `order_products` WRITE;
/*!40000 ALTER TABLE `order_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_taxes`
--

DROP TABLE IF EXISTS `order_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_taxes` (
  `order_id` int(10) unsigned NOT NULL,
  `tax_rate_id` int(10) unsigned NOT NULL,
  `amount` decimal(15,4) unsigned NOT NULL,
  PRIMARY KEY (`order_id`,`tax_rate_id`),
  KEY `order_taxes_tax_rate_id_foreign` (`tax_rate_id`),
  CONSTRAINT `order_taxes_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `order_taxes_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_taxes`
--

LOCK TABLES `order_taxes` WRITE;
/*!40000 ALTER TABLE `order_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address_1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(18,4) unsigned NOT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_cost` decimal(18,4) unsigned NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `discount` decimal(18,4) unsigned NOT NULL,
  `total` decimal(18,4) unsigned NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` decimal(18,4) NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_customer_id_index` (`customer_id`),
  KEY `orders_coupon_id_index` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_translations`
--

DROP TABLE IF EXISTS `page_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_translations`
--

LOCK TABLES `page_translations` WRITE;
/*!40000 ALTER TABLE `page_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistences`
--

DROP TABLE IF EXISTS `persistences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `persistences_code_unique` (`code`),
  KEY `persistences_user_id_foreign` (`user_id`),
  CONSTRAINT `persistences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistences`
--

LOCK TABLES `persistences` WRITE;
/*!40000 ALTER TABLE `persistences` DISABLE KEYS */;
INSERT INTO `persistences` VALUES (1,1,'U51gV4L4m52bg0GDSSMZqJkvl0x3kugi','2019-06-20 02:46:49','2019-06-20 02:46:49'),(2,1,'sBXFrGY6chO7fdUpZhKtovpVE8l0LlZ7','2019-09-18 12:16:49','2019-09-18 12:16:49');
/*!40000 ALTER TABLE `persistences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attribute_values`
--

DROP TABLE IF EXISTS `product_attribute_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attribute_values` (
  `product_attribute_id` int(10) unsigned NOT NULL,
  `attribute_value_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_attribute_id`,`attribute_value_id`),
  KEY `product_attribute_values_attribute_value_id_foreign` (`attribute_value_id`),
  CONSTRAINT `product_attribute_values_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_attribute_values_product_attribute_id_foreign` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attributes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attribute_values`
--

LOCK TABLES `product_attribute_values` WRITE;
/*!40000 ALTER TABLE `product_attribute_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attribute_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_attributes`
--

DROP TABLE IF EXISTS `product_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_attributes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `attribute_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_attributes_product_id_index` (`product_id`),
  KEY `product_attributes_attribute_id_index` (`attribute_id`),
  CONSTRAINT `product_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_attributes`
--

LOCK TABLES `product_attributes` WRITE;
/*!40000 ALTER TABLE `product_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categories` (
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`),
  KEY `product_categories_category_id_foreign` (`category_id`),
  CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_categories`
--

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_options`
--

DROP TABLE IF EXISTS `product_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_options` (
  `product_id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`option_id`),
  KEY `product_options_option_id_foreign` (`option_id`),
  CONSTRAINT `product_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_options`
--

LOCK TABLES `product_options` WRITE;
/*!40000 ALTER TABLE `product_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_translations`
--

DROP TABLE IF EXISTS `product_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`),
  FULLTEXT KEY `name` (`name`),
  CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_translations`
--

LOCK TABLES `product_translations` WRITE;
/*!40000 ALTER TABLE `product_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(10) unsigned DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(18,4) unsigned NOT NULL,
  `special_price` decimal(18,4) unsigned DEFAULT NULL,
  `special_price_start` date DEFAULT NULL,
  `special_price_end` date DEFAULT NULL,
  `selling_price` decimal(18,4) unsigned DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manage_stock` tinyint(1) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `viewed` int(10) unsigned NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `new_from` datetime DEFAULT NULL,
  `new_to` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `brand_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`),
  KEY `products_brand_id_foreign` (`brand_id`),
  CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `related_products`
--

DROP TABLE IF EXISTS `related_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `related_products` (
  `product_id` int(10) unsigned NOT NULL,
  `related_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`related_product_id`),
  KEY `related_products_related_product_id_foreign` (`related_product_id`),
  CONSTRAINT `related_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `related_products_related_product_id_foreign` FOREIGN KEY (`related_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `related_products`
--

LOCK TABLES `related_products` WRITE;
/*!40000 ALTER TABLE `related_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `related_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reminders_user_id_foreign` (`user_id`),
  CONSTRAINT `reminders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `rating` int(11) NOT NULL,
  `reviewer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reviews_reviewer_id_index` (`reviewer_id`),
  KEY `reviews_product_id_index` (`product_id`),
  CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_translations`
--

DROP TABLE IF EXISTS `role_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_translations_role_id_locale_unique` (`role_id`,`locale`),
  CONSTRAINT `role_translations_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_translations`
--

LOCK TABLES `role_translations` WRITE;
/*!40000 ALTER TABLE `role_translations` DISABLE KEYS */;
INSERT INTO `role_translations` VALUES (1,1,'en','Admin'),(2,2,'en','Customer');
/*!40000 ALTER TABLE `role_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'{\"admin.users.index\":true,\"admin.users.create\":true,\"admin.users.edit\":true,\"admin.users.destroy\":true,\"admin.roles.index\":true,\"admin.roles.create\":true,\"admin.roles.edit\":true,\"admin.roles.destroy\":true,\"admin.products.index\":true,\"admin.products.create\":true,\"admin.products.edit\":true,\"admin.products.destroy\":true,\"admin.attributes.index\":true,\"admin.attributes.create\":true,\"admin.attributes.edit\":true,\"admin.attributes.destroy\":true,\"admin.attribute_sets.index\":true,\"admin.attribute_sets.create\":true,\"admin.attribute_sets.edit\":true,\"admin.attribute_sets.destroy\":true,\"admin.options.index\":true,\"admin.options.create\":true,\"admin.options.edit\":true,\"admin.options.destroy\":true,\"admin.filters.index\":true,\"admin.filters.create\":true,\"admin.filters.edit\":true,\"admin.filters.destroy\":true,\"admin.reviews.index\":true,\"admin.reviews.create\":true,\"admin.reviews.edit\":true,\"admin.reviews.destroy\":true,\"admin.categories.index\":true,\"admin.categories.create\":true,\"admin.categories.edit\":true,\"admin.categories.destroy\":true,\"admin.orders.index\":true,\"admin.orders.show\":true,\"admin.orders.edit\":true,\"admin.transactions.index\":true,\"admin.coupons.index\":true,\"admin.coupons.create\":true,\"admin.coupons.edit\":true,\"admin.coupons.destroy\":true,\"admin.menus.index\":true,\"admin.menus.create\":true,\"admin.menus.edit\":true,\"admin.menus.destroy\":true,\"admin.menu_items.index\":true,\"admin.menu_items.create\":true,\"admin.menu_items.edit\":true,\"admin.menu_items.destroy\":true,\"admin.media.index\":true,\"admin.media.create\":true,\"admin.media.destroy\":true,\"admin.pages.index\":true,\"admin.pages.create\":true,\"admin.pages.edit\":true,\"admin.pages.destroy\":true,\"admin.currency_rates.index\":true,\"admin.currency_rates.edit\":true,\"admin.taxes.index\":true,\"admin.taxes.create\":true,\"admin.taxes.edit\":true,\"admin.taxes.destroy\":true,\"admin.translations.index\":true,\"admin.translations.edit\":true,\"admin.sliders.index\":true,\"admin.sliders.create\":true,\"admin.sliders.edit\":true,\"admin.sliders.destroy\":true,\"admin.reports.index\":true,\"admin.settings.edit\":true,\"admin.storefront.edit\":true,\"admin.brands.index\":true,\"admin.brands.create\":true,\"admin.brands.edit\":true,\"admin.brands.destroy\":true}','2019-06-20 02:46:31','2019-09-18 12:28:06'),(2,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_terms`
--

DROP TABLE IF EXISTS `search_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_terms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `results` int(10) unsigned NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `search_terms_term_unique` (`term`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_terms`
--

LOCK TABLES `search_terms` WRITE;
/*!40000 ALTER TABLE `search_terms` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_translations`
--

DROP TABLE IF EXISTS `setting_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setting_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `setting_translations_setting_id_locale_unique` (`setting_id`,`locale`),
  CONSTRAINT `setting_translations_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_translations`
--

LOCK TABLES `setting_translations` WRITE;
/*!40000 ALTER TABLE `setting_translations` DISABLE KEYS */;
INSERT INTO `setting_translations` VALUES (1,1,'en','s:9:\"Shiv Ecom\";'),(2,20,'en','s:13:\"Free Shipping\";'),(3,21,'en','s:12:\"Local Pickup\";'),(4,22,'en','s:9:\"Flat Rate\";'),(5,23,'en','s:14:\"PayPal Express\";'),(6,24,'en','s:28:\"Pay via your PayPal account.\";'),(7,25,'en','s:6:\"Stripe\";'),(8,26,'en','s:29:\"Pay via credit or debit card.\";'),(9,27,'en','s:16:\"Cash On Delivery\";'),(10,28,'en','s:28:\"Pay with cash upon delivery.\";'),(11,29,'en','s:13:\"Bank Transfer\";'),(12,30,'en','s:100:\"Make your payment directly into our bank account. Please use your Order ID as the payment reference.\";'),(13,31,'en','s:19:\"Check / Money Order\";'),(14,32,'en','s:33:\"Please send a check to our store.\";'),(15,55,'en','N;'),(16,56,'en','N;'),(17,57,'en','N;'),(18,107,'en','N;'),(19,33,'en','s:92:\"Copyright © <a href=\"{{ store_url }}\">{{ store_name }}</a> {{ year }}. All rights reserved.\";'),(20,108,'en','N;'),(21,109,'en','N;'),(22,110,'en','N;'),(23,111,'en','N;'),(24,112,'en','N;'),(25,113,'en','N;'),(26,114,'en','N;'),(27,115,'en','N;'),(28,116,'en','N;'),(29,117,'en','N;'),(30,118,'en','N;'),(31,119,'en','N;'),(32,120,'en','N;'),(33,121,'en','N;'),(34,35,'en','s:13:\"Free Delivery\";'),(35,36,'en','s:15:\"Orders over $60\";'),(36,38,'en','s:12:\"99% Customer\";'),(37,39,'en','s:9:\"Feedbacks\";'),(38,41,'en','s:7:\"Payment\";'),(39,42,'en','s:14:\"Secured system\";'),(40,44,'en','s:12:\"24/7 Support\";'),(41,45,'en','s:14:\"Helpline - 121\";'),(42,122,'en','N;'),(43,123,'en','N;'),(44,124,'en','N;'),(45,125,'en','N;'),(46,126,'en','N;'),(47,127,'en','N;'),(48,128,'en','N;'),(49,129,'en','N;'),(50,130,'en','N;'),(51,131,'en','N;'),(52,132,'en','N;'),(53,133,'en','N;'),(54,134,'en','N;'),(55,135,'en','N;'),(56,136,'en','N;'),(57,137,'en','N;'),(58,138,'en','N;'),(59,139,'en','N;'),(60,140,'en','N;'),(61,141,'en','N;'),(62,142,'en','N;'),(63,143,'en','N;'),(64,144,'en','N;'),(65,145,'en','N;'),(66,146,'en','N;'),(67,47,'en','s:15:\"Recently Viewed\";');
/*!40000 ALTER TABLE `setting_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_translatable` tinyint(1) NOT NULL DEFAULT '0',
  `plain_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=190 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'store_name',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(2,'store_email',0,'s:17:\"omshiv@gmaill.com\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(3,'search_engine',0,'s:5:\"mysql\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(4,'algolia_app_id',0,'N;','2019-06-20 02:46:32','2019-06-20 02:46:32'),(5,'algolia_secret',0,'N;','2019-06-20 02:46:32','2019-06-20 02:46:32'),(6,'active_theme',0,'s:10:\"Storefront\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(7,'supported_countries',0,'a:1:{i:0;s:2:\"IN\";}','2019-06-20 02:46:32','2019-06-20 02:47:59'),(8,'default_country',0,'s:2:\"IN\";','2019-06-20 02:46:32','2019-06-20 02:47:59'),(9,'supported_locales',0,'a:1:{i:0;s:2:\"en\";}','2019-06-20 02:46:32','2019-06-20 02:46:32'),(10,'default_locale',0,'s:2:\"en\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(11,'default_timezone',0,'s:12:\"Asia/Kolkata\";','2019-06-20 02:46:32','2019-06-20 02:47:59'),(12,'customer_role',0,'s:1:\"2\";','2019-06-20 02:46:32','2019-06-20 02:47:59'),(13,'reviews_enabled',0,'s:1:\"1\";','2019-06-20 02:46:32','2019-06-20 02:47:59'),(14,'auto_approve_reviews',0,'s:1:\"1\";','2019-06-20 02:46:32','2019-06-20 02:47:59'),(15,'supported_currencies',0,'a:1:{i:0;s:3:\"INR\";}','2019-06-20 02:46:32','2019-06-20 08:18:58'),(16,'default_currency',0,'s:3:\"INR\";','2019-06-20 02:46:32','2019-06-20 08:18:58'),(17,'send_order_invoice_email',0,'b:0;','2019-06-20 02:46:32','2019-06-20 02:46:32'),(18,'local_pickup_cost',0,'s:1:\"0\";','2019-06-20 02:46:32','2019-06-20 02:48:00'),(19,'flat_rate_cost',0,'s:1:\"0\";','2019-06-20 02:46:32','2019-06-20 02:48:00'),(20,'free_shipping_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(21,'local_pickup_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(22,'flat_rate_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(23,'paypal_express_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(24,'paypal_express_description',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(25,'stripe_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(26,'stripe_description',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(27,'cod_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(28,'cod_description',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(29,'bank_transfer_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(30,'bank_transfer_description',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(31,'check_payment_label',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(32,'check_payment_description',1,NULL,'2019-06-20 02:46:32','2019-06-20 02:46:32'),(33,'storefront_copyright_text',1,'s:92:\"Copyright © <a href=\"{{ store_url }}\">{{ store_name }}</a> {{ year }}. All rights reserved.\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(34,'storefront_feature_1_icon',0,'s:11:\"fa fa-truck\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(35,'storefront_feature_1_title',1,'s:13:\"Free Delivery\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(36,'storefront_feature_1_subtitle',1,'s:15:\"Orders over $60\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(37,'storefront_feature_2_icon',0,'s:18:\"fa fa-commenting-o\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(38,'storefront_feature_2_title',1,'s:12:\"99% Customer\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(39,'storefront_feature_2_subtitle',1,'s:9:\"Feedbacks\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(40,'storefront_feature_3_icon',0,'s:17:\"fa fa-credit-card\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(41,'storefront_feature_3_title',1,'s:7:\"Payment\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(42,'storefront_feature_3_subtitle',1,'s:14:\"Secured system\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(43,'storefront_feature_4_icon',0,'s:16:\"fa fa-headphones\";','2019-06-20 02:46:32','2019-06-20 02:46:32'),(44,'storefront_feature_4_title',1,'s:12:\"24/7 Support\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(45,'storefront_feature_4_subtitle',1,'s:14:\"Helpline - 121\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(46,'storefront_recently_viewed_section_enabled',0,'s:1:\"1\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(47,'storefront_recently_viewed_section_title',1,'s:15:\"Recently Viewed\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(48,'storefront_recently_viewed_section_total_products',0,'s:1:\"5\";','2019-06-20 02:46:32','2019-09-18 12:24:34'),(49,'welcome_email',0,'s:1:\"0\";','2019-06-20 02:47:59','2019-06-20 02:47:59'),(50,'admin_order_email',0,'s:1:\"0\";','2019-06-20 02:47:59','2019-06-20 02:47:59'),(51,'order_status_email',0,'s:1:\"0\";','2019-06-20 02:47:59','2019-06-20 02:47:59'),(52,'invoice_email',0,'s:1:\"0\";','2019-06-20 02:47:59','2019-06-20 02:47:59'),(53,'maintenance_mode',0,'s:1:\"0\";','2019-06-20 02:47:59','2019-06-20 02:47:59'),(54,'allowed_ips',0,'N;','2019-06-20 02:47:59','2019-06-20 02:47:59'),(55,'store_tagline',1,NULL,'2019-06-20 02:47:59','2019-06-20 02:47:59'),(56,'bank_transfer_instructions',1,NULL,'2019-06-20 02:47:59','2019-06-20 02:47:59'),(57,'check_payment_instructions',1,NULL,'2019-06-20 02:47:59','2019-06-20 02:47:59'),(58,'store_phone',0,'N;','2019-06-20 02:47:59','2019-06-20 02:47:59'),(59,'store_address_1',0,'N;','2019-06-20 02:47:59','2019-06-20 02:47:59'),(60,'store_address_2',0,'N;','2019-06-20 02:47:59','2019-06-20 02:47:59'),(61,'store_city',0,'N;','2019-06-20 02:47:59','2019-06-20 02:47:59'),(62,'store_country',0,'s:2:\"IN\";','2019-06-20 02:47:59','2019-06-20 08:18:23'),(63,'store_state',0,'s:2:\"UP\";','2019-06-20 02:47:59','2019-06-20 08:18:23'),(64,'store_zip',0,'s:6:\"201301\";','2019-06-20 02:47:59','2019-06-20 08:18:23'),(65,'currency_rate_exchange_service',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(66,'fixer_access_key',0,'s:10:\"Alobha@123\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(67,'forge_api_key',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(68,'currency_data_feed_api_key',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(69,'auto_refresh_currency_rates',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(70,'auto_refresh_currency_rate_frequency',0,'s:5:\"daily\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(71,'mail_from_address',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(72,'mail_from_name',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(73,'mail_host',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(74,'mail_port',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(75,'mail_username',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(76,'mail_password',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(77,'mail_encryption',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(78,'custom_header_assets',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(79,'custom_footer_assets',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(80,'facebook_login_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(81,'facebook_login_app_id',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(82,'facebook_login_app_secret',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(83,'google_login_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(84,'google_login_client_id',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(85,'google_login_client_secret',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(86,'free_shipping_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(87,'free_shipping_min_amount',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(88,'local_pickup_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(89,'flat_rate_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(90,'paypal_express_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(91,'paypal_express_test_mode',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(92,'paypal_express_username',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(93,'paypal_express_password',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(94,'paypal_express_signature',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(95,'stripe_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(96,'stripe_publishable_key',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(97,'stripe_secret_key',0,'N;','2019-06-20 02:48:00','2019-06-20 02:48:00'),(98,'cod_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(99,'bank_transfer_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(100,'check_payment_enabled',0,'s:1:\"0\";','2019-06-20 02:48:00','2019-06-20 02:48:00'),(101,'storefront_theme',0,'s:10:\"theme-blue\";','2019-09-18 12:24:33','2019-09-18 12:24:33'),(102,'storefront_mail_theme',0,'s:10:\"theme-blue\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(103,'storefront_layout',0,'s:7:\"default\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(104,'storefront_slider',0,'s:1:\"1\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(105,'storefront_terms_page',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(106,'storefront_privacy_page',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(107,'storefront_footer_address',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(108,'storefront_category_menu_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(109,'storefront_footer_menu_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(110,'storefront_banner_section_1_banner_1_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(111,'storefront_banner_section_1_banner_1_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(112,'storefront_banner_section_1_banner_1_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(113,'storefront_banner_section_1_banner_1_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(114,'storefront_banner_section_1_banner_2_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(115,'storefront_banner_section_1_banner_2_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(116,'storefront_banner_section_1_banner_2_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(117,'storefront_banner_section_1_banner_2_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(118,'storefront_banner_section_1_banner_3_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(119,'storefront_banner_section_1_banner_3_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(120,'storefront_banner_section_1_banner_3_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(121,'storefront_banner_section_1_banner_3_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(122,'storefront_product_carousel_section_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(123,'storefront_recent_products_section_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(124,'storefront_banner_section_2_banner_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(125,'storefront_banner_section_2_banner_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(126,'storefront_banner_section_2_banner_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(127,'storefront_banner_section_2_banner_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(128,'storefront_three_column_vertical_product_carousel_section_column_1_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(129,'storefront_three_column_vertical_product_carousel_section_column_2_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(130,'storefront_three_column_vertical_product_carousel_section_column_3_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(131,'storefront_landscape_products_section_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(132,'storefront_banner_section_3_banner_1_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(133,'storefront_banner_section_3_banner_1_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(134,'storefront_banner_section_3_banner_1_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(135,'storefront_banner_section_3_banner_1_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(136,'storefront_banner_section_3_banner_2_file_id',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(137,'storefront_banner_section_3_banner_2_caption_1',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(138,'storefront_banner_section_3_banner_2_caption_2',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(139,'storefront_banner_section_3_banner_2_call_to_action_text',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(140,'storefront_product_tabs_section_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(141,'storefront_product_tabs_section_tab_1_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(142,'storefront_product_tabs_section_tab_2_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(143,'storefront_product_tabs_section_tab_3_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(144,'storefront_product_tabs_section_tab_4_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(145,'storefront_two_column_product_carousel_section_column_1_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(146,'storefront_two_column_product_carousel_section_column_2_title',1,NULL,'2019-09-18 12:24:34','2019-09-18 12:24:34'),(147,'storefront_primary_menu',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(148,'storefront_category_menu',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(149,'storefront_footer_menu',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(150,'storefront_fb_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(151,'storefront_twitter_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(152,'storefront_instagram_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(153,'storefront_linkedin_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(154,'storefront_pinterest_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(155,'storefront_google_plus_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(156,'storefront_youtube_link',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(157,'storefront_banner_section_1_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(158,'storefront_banner_section_1_banner_1_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(159,'storefront_banner_section_1_banner_1_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(160,'storefront_banner_section_1_banner_2_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(161,'storefront_banner_section_1_banner_2_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(162,'storefront_banner_section_1_banner_3_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(163,'storefront_banner_section_1_banner_3_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(164,'storefront_features_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(165,'storefront_product_carousel_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(166,'storefront_recent_products_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(167,'storefront_recent_products_section_total_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(168,'storefront_banner_section_2_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(169,'storefront_banner_section_2_banner_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(170,'storefront_banner_section_2_banner_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(171,'storefront_three_column_vertical_product_carousel_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(172,'storefront_landscape_products_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(173,'storefront_banner_section_3_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(174,'storefront_banner_section_3_banner_1_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(175,'storefront_banner_section_3_banner_1_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(176,'storefront_banner_section_3_banner_2_call_to_action_url',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(177,'storefront_banner_section_3_banner_2_open_in_new_window',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(178,'storefront_product_tabs_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(179,'storefront_two_column_product_carousel_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(180,'storefront_brand_section_enabled',0,'s:1:\"0\";','2019-09-18 12:24:34','2019-09-18 12:24:34'),(181,'storefront_product_carousel_section_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(182,'storefront_three_column_vertical_product_carousel_section_column_2_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(183,'storefront_landscape_products_section_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(184,'storefront_product_tabs_section_tab_1_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(185,'storefront_product_tabs_section_tab_2_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(186,'storefront_product_tabs_section_tab_3_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(187,'storefront_product_tabs_section_tab_4_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(188,'storefront_two_column_product_carousel_section_column_1_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34'),(189,'storefront_two_column_product_carousel_section_column_2_products',0,'N;','2019-09-18 12:24:34','2019-09-18 12:24:34');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_slide_translations`
--

DROP TABLE IF EXISTS `slider_slide_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_slide_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider_slide_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_id` int(10) unsigned DEFAULT NULL,
  `caption_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `call_to_action_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slider_slide_translations_slider_slide_id_locale_unique` (`slider_slide_id`,`locale`),
  CONSTRAINT `slider_slide_translations_slider_slide_id_foreign` FOREIGN KEY (`slider_slide_id`) REFERENCES `slider_slides` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_slide_translations`
--

LOCK TABLES `slider_slide_translations` WRITE;
/*!40000 ALTER TABLE `slider_slide_translations` DISABLE KEYS */;
INSERT INTO `slider_slide_translations` VALUES (1,1,'en',2,'Hot Deals','New Arrivals','40% off on first purchase','Shop Now'),(2,2,'en',3,'Best Deals','Male Fashion','Summer Collection 2018','Shop Now'),(3,3,'en',4,'Trend Collection','Free Shipping','On first purchase','Shop Now'),(4,4,'en',5,'Save 50% on','Smartphones','Grab the best deal now','Buy Now');
/*!40000 ALTER TABLE `slider_slide_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_slides`
--

DROP TABLE IF EXISTS `slider_slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int(10) unsigned NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `call_to_action_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_in_new_window` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `slider_slides_slider_id_foreign` (`slider_id`),
  CONSTRAINT `slider_slides_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_slides`
--

LOCK TABLES `slider_slides` WRITE;
/*!40000 ALTER TABLE `slider_slides` DISABLE KEYS */;
INSERT INTO `slider_slides` VALUES (1,1,'{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}','#',0,0,'2019-09-18 12:18:05','2019-09-18 12:22:01'),(2,1,'{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}','#',0,1,'2019-09-18 12:24:16','2019-09-18 12:24:16'),(3,1,'{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}','#',0,2,'2019-09-18 12:24:16','2019-09-18 12:24:16'),(4,1,'{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}','#',0,3,'2019-09-18 12:24:16','2019-09-18 12:24:16');
/*!40000 ALTER TABLE `slider_slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_translations`
--

DROP TABLE IF EXISTS `slider_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slider_translations_slider_id_locale_unique` (`slider_id`,`locale`),
  CONSTRAINT `slider_translations_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_translations`
--

LOCK TABLES `slider_translations` WRITE;
/*!40000 ALTER TABLE `slider_translations` DISABLE KEYS */;
INSERT INTO `slider_translations` VALUES (1,1,'en','home_slider');
/*!40000 ALTER TABLE `slider_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `autoplay` tinyint(1) DEFAULT NULL,
  `autoplay_speed` int(11) DEFAULT NULL,
  `arrows` tinyint(1) DEFAULT NULL,
  `dots` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,1,3000,1,NULL,'2019-09-18 12:18:05','2019-09-18 12:18:05');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_class_translations`
--

DROP TABLE IF EXISTS `tax_class_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_class_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_class_translations_tax_class_id_locale_unique` (`tax_class_id`,`locale`),
  CONSTRAINT `tax_class_translations_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_class_translations`
--

LOCK TABLES `tax_class_translations` WRITE;
/*!40000 ALTER TABLE `tax_class_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_class_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_classes`
--

DROP TABLE IF EXISTS `tax_classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `based_on` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_classes`
--

LOCK TABLES `tax_classes` WRITE;
/*!40000 ALTER TABLE `tax_classes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rate_translations`
--

DROP TABLE IF EXISTS `tax_rate_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_rate_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_rate_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_rate_translations_tax_rate_id_locale_unique` (`tax_rate_id`,`locale`),
  CONSTRAINT `tax_rate_translations_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rate_translations`
--

LOCK TABLES `tax_rate_translations` WRITE;
/*!40000 ALTER TABLE `tax_rate_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_rate_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax_rates`
--

DROP TABLE IF EXISTS `tax_rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax_rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tax_class_id` int(10) unsigned NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,4) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tax_rates_tax_class_id_index` (`tax_class_id`),
  CONSTRAINT `tax_rates_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax_rates`
--

LOCK TABLES `tax_rates` WRITE;
/*!40000 ALTER TABLE `tax_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `tax_rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `throttle`
--

DROP TABLE IF EXISTS `throttle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_foreign` (`user_id`),
  CONSTRAINT `throttle_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `throttle`
--

LOCK TABLES `throttle` WRITE;
/*!40000 ALTER TABLE `throttle` DISABLE KEYS */;
/*!40000 ALTER TABLE `throttle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(10) unsigned NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactions_order_id_unique` (`order_id`),
  CONSTRAINT `transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation_translations`
--

DROP TABLE IF EXISTS `translation_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `translation_id` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translation_translations_translation_id_locale_unique` (`translation_id`,`locale`),
  CONSTRAINT `translation_translations_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation_translations`
--

LOCK TABLES `translation_translations` WRITE;
/*!40000 ALTER TABLE `translation_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translation_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translations_key_index` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `up_sell_products`
--

DROP TABLE IF EXISTS `up_sell_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `up_sell_products` (
  `product_id` int(10) unsigned NOT NULL,
  `up_sell_product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`up_sell_product_id`),
  KEY `up_sell_products_up_sell_product_id_foreign` (`up_sell_product_id`),
  CONSTRAINT `up_sell_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `up_sell_products_up_sell_product_id_foreign` FOREIGN KEY (`up_sell_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `up_sell_products`
--

LOCK TABLES `up_sell_products` WRITE;
/*!40000 ALTER TABLE `up_sell_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `up_sell_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_role_id_foreign` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1,'2019-06-20 02:46:32','2019-06-20 02:46:32');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number_otp_valid_till` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_number_unique` (`mobile_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'rohit','kumar','rohit.alobha@gmail.com','$2y$10$jjkBzRQJ9b9du7xDZ6nOIuKIzGWsVckac2rL7ICeGD5Pl0tSfuVR6',NULL,'2019-09-18 12:16:49','2019-06-20 02:46:32','2019-09-18 12:16:49',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wish_lists`
--

DROP TABLE IF EXISTS `wish_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wish_lists` (
  `user_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`product_id`),
  KEY `wish_lists_product_id_foreign` (`product_id`),
  CONSTRAINT `wish_lists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  CONSTRAINT `wish_lists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wish_lists`
--

LOCK TABLES `wish_lists` WRITE;
/*!40000 ALTER TABLE `wish_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `wish_lists` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-18  7:17:13
