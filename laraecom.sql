-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 30, 2020 at 10:46 PM
-- Server version: 8.0.17
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laraecom`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'njYmkYg4Ym9sNTvgB4CM1V9jjpEE8nQr', 1, '2019-06-20 08:16:32', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(2, 7, 'mT7u9zE7Gjoo9jBVSdGzeA6SKKOiynUY', 1, '2019-12-30 12:10:43', '2019-12-30 12:10:43', '2019-12-30 12:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_set_id` int(10) UNSIGNED NOT NULL,
  `is_filterable` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `attribute_set_id`, `is_filterable`, `created_at`, `updated_at`) VALUES
(2, 3, 1, '2019-12-30 10:24:08', '2019-12-30 10:24:08'),
(7, 3, 1, '2019-12-30 11:51:13', '2019-12-30 11:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_categories`
--

CREATE TABLE `attribute_categories` (
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_categories`
--

INSERT INTO `attribute_categories` (`attribute_id`, `category_id`) VALUES
(2, 4),
(7, 4),
(2, 5),
(2, 7),
(7, 7),
(2, 8),
(7, 8),
(2, 9),
(7, 9),
(2, 10),
(7, 10),
(2, 11),
(7, 11),
(2, 12),
(7, 12),
(2, 14),
(7, 14),
(2, 15),
(7, 15),
(2, 16),
(7, 16),
(2, 17),
(7, 17),
(2, 18),
(7, 18),
(2, 19),
(7, 19),
(2, 20),
(7, 20),
(2, 21),
(7, 21),
(2, 22),
(7, 22),
(2, 23),
(7, 23);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_sets`
--

CREATE TABLE `attribute_sets` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_sets`
--

INSERT INTO `attribute_sets` (`id`, `created_at`, `updated_at`) VALUES
(3, '2019-12-30 10:03:38', '2019-12-30 10:03:38');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_set_translations`
--

CREATE TABLE `attribute_set_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_set_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_set_translations`
--

INSERT INTO `attribute_set_translations` (`id`, `attribute_set_id`, `locale`, `name`) VALUES
(3, 3, 'en', 'Specifications');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_translations`
--

CREATE TABLE `attribute_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_translations`
--

INSERT INTO `attribute_translations` (`id`, `attribute_id`, `locale`, `name`) VALUES
(2, 2, 'en', 'Color'),
(7, 7, 'en', 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_values`
--

CREATE TABLE `attribute_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_values`
--

INSERT INTO `attribute_values` (`id`, `attribute_id`, `position`, `created_at`, `updated_at`) VALUES
(1, 2, 0, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(2, 2, 1, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(3, 2, 2, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(4, 2, 3, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(5, 2, 4, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(6, 2, 5, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(7, 2, 6, '2019-12-30 10:29:58', '2019-12-30 10:29:58'),
(13, 7, 0, '2019-12-30 11:51:13', '2019-12-30 11:51:13'),
(14, 7, 1, '2019-12-30 11:51:13', '2019-12-30 11:51:13');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_value_translations`
--

CREATE TABLE `attribute_value_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_value_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute_value_translations`
--

INSERT INTO `attribute_value_translations` (`id`, `attribute_value_id`, `locale`, `value`) VALUES
(1, 1, 'en', 'Black'),
(2, 2, 'en', 'Silver'),
(3, 3, 'en', 'Grey'),
(4, 4, 'en', 'Brown'),
(5, 5, 'en', 'Gold'),
(6, 6, 'en', 'Blue'),
(7, 7, 'en', 'Multi Color'),
(13, 13, 'en', 'Twin'),
(14, 14, 'en', 'Queen');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `slug`, `display_name`, `title`, `description`, `created_at`, `updated_at`) VALUES
(4, 'lexington.com', 'Lexington', 'classic Lexington', 'The Lexington: The classic Lexington is part of our Carriage Collection and made from our quality imported hardwood', '2019-12-27 14:35:53', '2019-12-27 14:35:53'),
(5, 'hooker.com', 'Hooker', 'Featuring designs inspired by America\'s great Mansions', 'Hooker Furniture has been one of the leading manufacturers in the home furnishings industry for over 90 years. ..', '2019-12-27 14:38:21', '2019-12-27 14:38:21'),
(6, 'universal.com', 'UNIVERSAL', 'Universal Furniture', 'For Universal Furniture, good design celebrates styling that has an enduring appeal. It is always on-trend but never fad-focused', '2019-12-27 14:41:09', '2019-12-27 14:41:09');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `is_searchable` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `slug`, `position`, `is_searchable`, `is_active`, `created_at`, `updated_at`) VALUES
(4, NULL, 'bedroom', 20, 1, 1, '2019-12-27 12:33:44', '2020-01-31 11:49:14'),
(5, 4, 'headboards', 22, 0, 1, '2019-12-27 12:34:58', '2020-01-31 11:49:14'),
(7, 4, 'nightstands', 21, 0, 1, '2019-12-27 12:35:29', '2020-01-31 11:49:14'),
(8, NULL, 'dining-room', 29, 1, 1, '2019-12-27 12:35:46', '2020-01-31 11:49:14'),
(9, 8, 'chairs', 31, 1, 1, '2019-12-27 12:35:59', '2020-01-31 11:49:14'),
(10, 8, 'benches', 30, 1, 1, '2019-12-27 12:36:14', '2020-01-31 11:49:14'),
(11, NULL, 'living-room', 24, 1, 1, '2019-12-27 12:36:31', '2020-01-31 11:49:14'),
(12, 11, 'coffee-tables', 28, 0, 1, '2019-12-27 12:36:54', '2020-01-31 11:49:14'),
(14, 11, 'end-tables', 27, 0, 1, '2019-12-27 12:37:17', '2020-01-31 11:49:14'),
(15, 11, 'console-tables', 25, 0, 1, '2019-12-27 12:37:32', '2020-01-31 11:49:14'),
(16, 11, 'tv-console-s', 26, 0, 1, '2019-12-27 12:39:19', '2020-01-31 11:49:14'),
(17, NULL, 'accent', 0, 1, 1, '2019-12-27 12:39:31', '2020-01-31 11:49:14'),
(18, 17, 'bar-stools', 7, 0, 1, '2019-12-27 12:39:47', '2020-01-31 11:49:14'),
(19, 17, 'desks', 6, 0, 1, '2019-12-27 12:41:55', '2020-01-31 11:49:14'),
(20, 17, 'ottomans', 5, 0, 1, '2019-12-27 12:42:33', '2020-01-31 11:49:14'),
(21, 17, 'outdoor', 4, 0, 1, '2019-12-27 12:42:55', '2020-01-31 11:49:14'),
(22, 17, 'shelves', 3, 0, 1, '2019-12-27 12:43:18', '2020-01-31 11:49:14'),
(23, 17, 'vanities', 2, 0, 1, '2019-12-27 12:43:39', '2020-01-31 11:49:14'),
(27, 17, 'bench', 1, 0, 1, '2020-01-30 11:23:07', '2020-01-31 11:49:14'),
(32, 4, 'coffee-table', 23, 0, 1, '2020-01-30 12:49:13', '2020-01-31 11:49:14'),
(34, 8, 'dining-set', 32, 0, 1, '2020-01-31 10:41:49', '2020-01-31 11:49:14'),
(35, 8, 'dining-table', 33, 0, 1, '2020-01-31 10:42:07', '2020-01-31 11:49:14'),
(36, 8, 'bar', 34, 0, 1, '2020-01-31 10:42:23', '2020-01-31 11:49:14'),
(37, 8, 'regular-ht', 35, 0, 1, '2020-01-31 10:42:48', '2020-01-31 11:49:14'),
(38, 8, 'dining-chair', 36, 0, 1, '2020-01-31 10:43:08', '2020-01-31 11:49:14'),
(39, 17, 'chair', 9, 0, 1, '2020-01-31 10:46:11', '2020-01-31 11:49:14'),
(40, 17, 'table-stand', 10, 0, 1, '2020-01-31 10:46:35', '2020-01-31 11:49:14'),
(41, 17, 'lighting-accessory', 11, 0, 1, '2020-01-31 10:47:46', '2020-01-31 11:49:14'),
(42, 17, 'shelf-storage', 12, 0, 1, '2020-01-31 10:50:13', '2020-01-31 11:49:14'),
(43, 17, 'table', 8, 0, 1, '2020-01-31 10:50:41', '2020-01-31 11:49:14'),
(44, 17, 'accent-chair', 13, 0, 1, '2020-01-31 10:50:56', '2020-01-31 11:49:14'),
(45, 17, 'hallway', 14, 0, 1, '2020-01-31 10:51:16', '2020-01-31 11:49:14'),
(46, 17, 'bench-ottoman', 15, 0, 1, '2020-01-31 10:51:36', '2020-01-31 11:49:14'),
(47, 17, 'love-seat', 16, 0, 1, '2020-01-31 10:51:54', '2020-01-31 11:49:14'),
(48, 17, 'bar-MXFdkhvD', 17, 0, 1, '2020-01-31 10:52:07', '2020-01-31 11:49:14'),
(49, 17, 'office', 18, 0, 1, '2020-01-31 10:53:17', '2020-01-31 11:49:14'),
(50, 17, 'office-desk', 19, 0, 1, '2020-01-31 10:53:47', '2020-01-31 11:49:14'),
(51, NULL, 'youth', 49, 1, 1, '2020-01-31 11:15:53', '2020-01-31 11:49:14'),
(52, NULL, 'game', 46, 1, 1, '2020-01-31 11:16:19', '2020-01-31 11:49:14'),
(53, NULL, 'outdoor-hIWJgbo8', 37, 1, 1, '2020-01-31 11:16:36', '2020-01-31 11:49:14'),
(54, 51, 'headboard', 50, 0, 1, '2020-01-31 11:16:59', '2020-01-31 11:49:14'),
(55, 51, 'chest-accessory', 51, 0, 1, '2020-01-31 11:17:12', '2020-01-31 11:49:14'),
(56, 51, 'night-stand', 52, 0, 1, '2020-01-31 11:17:34', '2020-01-31 11:49:14'),
(57, 51, 'bed', 53, 0, 1, '2020-01-31 11:17:52', '2020-01-31 11:49:14'),
(58, 51, 'youth-bedroom', 54, 0, 1, '2020-01-31 11:18:04', '2020-01-31 11:49:14'),
(59, 51, 'desk', 55, 0, 1, '2020-01-31 11:18:20', '2020-01-31 11:49:14'),
(60, 51, 'chest', 56, 0, 1, '2020-01-31 11:25:23', '2020-01-31 11:49:14'),
(61, 52, 'chair-1dj0byCd', 47, 0, 1, '2020-01-31 11:28:45', '2020-01-31 11:49:14'),
(62, 52, 'table-3Xasnkgd', 48, 0, 1, '2020-01-31 11:28:58', '2020-01-31 11:49:14'),
(63, 53, 'chair-h8AlqCPG', 38, 0, 1, '2020-01-31 11:30:22', '2020-01-31 11:49:14'),
(64, 53, 'fireplace', 39, 0, 1, '2020-01-31 11:30:35', '2020-01-31 11:49:14'),
(65, 53, 'outdoor-seating-sets', 40, 0, 1, '2020-01-31 11:30:51', '2020-01-31 11:49:14'),
(66, 53, 'table-U0xgUm9s', 41, 0, 1, '2020-01-31 11:31:09', '2020-01-31 11:49:14'),
(67, 53, 'sofa', 42, 0, 1, '2020-01-31 11:31:26', '2020-01-31 11:49:14'),
(68, 53, 'coffee-table-60g9zI4G', 43, 0, 1, '2020-01-31 11:31:37', '2020-01-31 11:49:14'),
(69, 53, 'outdoor-dining', 44, 0, 1, '2020-01-31 11:31:57', '2020-01-31 11:49:14'),
(70, 53, 'dining-set-raxh8W7Q', 45, 0, 1, '2020-01-31 11:32:19', '2020-01-31 11:49:14');

-- --------------------------------------------------------

--
-- Table structure for table `category_translations`
--

CREATE TABLE `category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_translations`
--

INSERT INTO `category_translations` (`id`, `category_id`, `locale`, `name`) VALUES
(4, 4, 'en', 'BEDROOM'),
(5, 5, 'en', 'Headboards'),
(7, 7, 'en', 'Nightstands'),
(8, 8, 'en', 'DINING ROOM'),
(9, 9, 'en', 'Chairs'),
(10, 10, 'en', 'Benches'),
(11, 11, 'en', 'LIVING ROOM'),
(12, 12, 'en', 'Coffee Tables'),
(14, 14, 'en', 'End Tables'),
(15, 15, 'en', 'Console Tables'),
(16, 16, 'en', 'TV Consoles'),
(17, 17, 'en', 'ACCENT'),
(18, 18, 'en', 'Bar Stools'),
(19, 19, 'en', 'Desks'),
(20, 20, 'en', 'Ottomans'),
(21, 21, 'en', 'Outdoor'),
(22, 22, 'en', 'Shelves'),
(23, 23, 'en', 'Vanities'),
(27, 27, 'en', 'Bench'),
(32, 32, 'en', 'Coffee Table'),
(34, 34, 'en', 'Dining Set'),
(35, 35, 'en', 'Dining Table'),
(36, 36, 'en', 'Bar'),
(37, 37, 'en', 'Regular Ht'),
(38, 38, 'en', 'Dining Chair'),
(39, 39, 'en', 'Chair'),
(40, 40, 'en', 'Table/Stand'),
(41, 41, 'en', 'Lighting & Accessory'),
(42, 42, 'en', 'Shelf/Storage'),
(43, 43, 'en', 'Table'),
(44, 44, 'en', 'Accent Chair'),
(45, 45, 'en', 'Hallway'),
(46, 46, 'en', 'Bench & Ottoman'),
(47, 47, 'en', 'Love Seat'),
(48, 48, 'en', 'Bar'),
(49, 49, 'en', 'Office'),
(50, 50, 'en', 'Office & Desk'),
(51, 51, 'en', 'YOUTH'),
(52, 52, 'en', 'GAME'),
(53, 53, 'en', 'OUTDOOR'),
(54, 54, 'en', 'Headboard'),
(55, 55, 'en', 'Chest & Accessory'),
(56, 56, 'en', 'Night Stand'),
(57, 57, 'en', 'Bed'),
(58, 58, 'en', 'Youth Bedroom'),
(59, 59, 'en', 'Desk'),
(60, 60, 'en', 'Chest'),
(61, 61, 'en', 'Chair'),
(62, 62, 'en', 'Table'),
(63, 63, 'en', 'Chair'),
(64, 64, 'en', 'Fireplace'),
(65, 65, 'en', 'Outdoor Seating Sets'),
(66, 66, 'en', 'Table'),
(67, 67, 'en', 'Sofa'),
(68, 68, 'en', 'Coffee Table'),
(69, 69, 'en', 'Outdoor Dining'),
(70, 70, 'en', 'Dining Set');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(18,4) UNSIGNED DEFAULT NULL,
  `is_percent` tinyint(1) NOT NULL,
  `free_shipping` tinyint(1) NOT NULL,
  `minimum_spend` decimal(18,4) UNSIGNED DEFAULT NULL,
  `maximum_spend` decimal(18,4) UNSIGNED DEFAULT NULL,
  `usage_limit_per_coupon` int(10) UNSIGNED DEFAULT NULL,
  `usage_limit_per_customer` int(10) UNSIGNED DEFAULT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_categories`
--

CREATE TABLE `coupon_categories` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_products`
--

CREATE TABLE `coupon_products` (
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `exclude` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_translations`
--

CREATE TABLE `coupon_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `coupon_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cross_sell_products`
--

CREATE TABLE `cross_sell_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `cross_sell_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currency_rates`
--

CREATE TABLE `currency_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_rates`
--

INSERT INTO `currency_rates` (`id`, `currency`, `rate`, `created_at`, `updated_at`) VALUES
(1, 'USD', '1.0000', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(2, 'INR', '1.0000', '2019-06-20 08:18:58', '2019-06-20 08:18:58');

-- --------------------------------------------------------

--
-- Table structure for table `entity_files`
--

CREATE TABLE `entity_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `entity_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) UNSIGNED NOT NULL,
  `zone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `entity_files`
--

INSERT INTO `entity_files` (`id`, `file_id`, `entity_type`, `entity_id`, `zone`, `created_at`, `updated_at`) VALUES
(1, 7, 'Modules\\Brand\\Entities\\Brand', 1, 'banner_image', '2019-09-23 10:52:35', '2019-09-23 10:52:35'),
(2, 5, 'Modules\\Brand\\Entities\\Brand', 2, 'banner_image', '2019-09-23 10:52:53', '2019-09-23 10:52:53'),
(3, 6, 'Modules\\Brand\\Entities\\Brand', 3, 'banner_image', '2019-09-23 10:53:36', '2019-09-23 10:53:36'),
(4, 9, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 1, 'banner_image', '2019-09-23 10:57:17', '2019-09-23 10:57:17'),
(5, 8, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 2, 'banner_image', '2019-09-23 10:57:34', '2019-09-23 10:57:34'),
(6, 10, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 3, 'banner_image', '2019-09-23 10:57:50', '2019-09-23 10:57:50'),
(12, 5, 'Modules\\FlashSale\\Entities\\FlashSale', 3, 'banner_image', '2019-09-25 17:30:04', '2019-09-25 17:30:04'),
(14, 6, 'Modules\\FlashSale\\Entities\\FlashSale', 2, 'banner_image', '2019-10-04 16:03:57', '2019-10-04 16:03:57'),
(15, 8, 'Modules\\FlashSale\\Entities\\FlashSale', 1, 'banner_image', '2019-10-04 16:04:55', '2019-10-04 16:04:55'),
(16, 11, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 4, 'banner_image', '2019-12-27 12:50:34', '2019-12-27 12:50:34'),
(17, 14, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 5, 'banner_image', '2019-12-27 12:55:10', '2019-12-27 12:55:10'),
(24, 17, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 6, 'banner_image', '2019-12-27 13:24:39', '2019-12-27 13:24:39'),
(38, 27, 'Modules\\Brand\\Entities\\Brand', 4, 'banner_image', '2019-12-27 14:36:00', '2019-12-27 14:36:00'),
(39, 28, 'Modules\\Brand\\Entities\\Brand', 5, 'banner_image', '2019-12-27 14:38:21', '2019-12-27 14:38:21'),
(40, 30, 'Modules\\Brand\\Entities\\Brand', 6, 'banner_image', '2019-12-27 14:41:09', '2019-12-27 14:41:09'),
(90, 14, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 7, 'banner_image', '2019-12-27 16:18:58', '2019-12-27 16:18:58'),
(91, 39, 'Modules\\FeaturedCategory\\Entities\\FeaturedCategory', 8, 'banner_image', '2019-12-27 16:19:31', '2019-12-27 16:19:31'),
(238, 26, 'Modules\\Product\\Entities\\Product', 24, 'base_image', '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(239, 24, 'Modules\\Product\\Entities\\Product', 24, 'additional_images', '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(240, 25, 'Modules\\Product\\Entities\\Product', 24, 'additional_images', '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(244, 32, 'Modules\\Product\\Entities\\Product', 25, 'base_image', '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(245, 106, 'Modules\\Product\\Entities\\Product', 25, 'additional_images', '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(246, 71, 'Modules\\Product\\Entities\\Product', 23, 'base_image', '2019-12-30 13:22:35', '2019-12-30 13:22:35'),
(247, 23, 'Modules\\Product\\Entities\\Product', 23, 'additional_images', '2019-12-30 13:22:35', '2019-12-30 13:22:35'),
(248, 21, 'Modules\\Product\\Entities\\Product', 23, 'additional_images', '2019-12-30 13:22:35', '2019-12-30 13:22:35'),
(252, 39, 'Modules\\Product\\Entities\\Product', 27, 'base_image', '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(253, 44, 'Modules\\Product\\Entities\\Product', 27, 'additional_images', '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(254, 43, 'Modules\\Product\\Entities\\Product', 27, 'additional_images', '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(255, 45, 'Modules\\Product\\Entities\\Product', 27, 'additional_images', '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(256, 35, 'Modules\\Product\\Entities\\Product', 26, 'base_image', '2019-12-30 13:30:40', '2019-12-30 13:30:40'),
(257, 37, 'Modules\\Product\\Entities\\Product', 26, 'additional_images', '2019-12-30 13:30:40', '2019-12-30 13:30:40'),
(258, 36, 'Modules\\Product\\Entities\\Product', 26, 'additional_images', '2019-12-30 13:30:40', '2019-12-30 13:30:40'),
(259, 48, 'Modules\\Product\\Entities\\Product', 28, 'base_image', '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(260, 46, 'Modules\\Product\\Entities\\Product', 28, 'additional_images', '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(261, 47, 'Modules\\Product\\Entities\\Product', 28, 'additional_images', '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(262, 73, 'Modules\\Product\\Entities\\Product', 29, 'base_image', '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(263, 76, 'Modules\\Product\\Entities\\Product', 30, 'base_image', '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(264, 75, 'Modules\\Product\\Entities\\Product', 30, 'additional_images', '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(265, 74, 'Modules\\Product\\Entities\\Product', 30, 'additional_images', '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(266, 77, 'Modules\\Product\\Entities\\Product', 31, 'base_image', '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(267, 78, 'Modules\\Product\\Entities\\Product', 31, 'additional_images', '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(268, 79, 'Modules\\Product\\Entities\\Product', 31, 'additional_images', '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(269, 80, 'Modules\\Product\\Entities\\Product', 31, 'additional_images', '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(270, 83, 'Modules\\Product\\Entities\\Product', 32, 'base_image', '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(271, 81, 'Modules\\Product\\Entities\\Product', 32, 'additional_images', '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(272, 82, 'Modules\\Product\\Entities\\Product', 32, 'additional_images', '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(273, 88, 'Modules\\Product\\Entities\\Product', 33, 'base_image', '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(274, 84, 'Modules\\Product\\Entities\\Product', 33, 'additional_images', '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(275, 87, 'Modules\\Product\\Entities\\Product', 33, 'additional_images', '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(276, 85, 'Modules\\Product\\Entities\\Product', 33, 'additional_images', '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(277, 92, 'Modules\\Product\\Entities\\Product', 34, 'base_image', '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(278, 90, 'Modules\\Product\\Entities\\Product', 34, 'additional_images', '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(279, 89, 'Modules\\Product\\Entities\\Product', 34, 'additional_images', '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(280, 91, 'Modules\\Product\\Entities\\Product', 34, 'additional_images', '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(281, 96, 'Modules\\Product\\Entities\\Product', 35, 'base_image', '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(282, 95, 'Modules\\Product\\Entities\\Product', 35, 'additional_images', '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(283, 94, 'Modules\\Product\\Entities\\Product', 35, 'additional_images', '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(284, 93, 'Modules\\Product\\Entities\\Product', 35, 'additional_images', '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(285, 100, 'Modules\\Product\\Entities\\Product', 36, 'base_image', '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(286, 99, 'Modules\\Product\\Entities\\Product', 36, 'additional_images', '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(287, 97, 'Modules\\Product\\Entities\\Product', 36, 'additional_images', '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(288, 98, 'Modules\\Product\\Entities\\Product', 36, 'additional_images', '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(289, 105, 'Modules\\Product\\Entities\\Product', 37, 'base_image', '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(290, 104, 'Modules\\Product\\Entities\\Product', 37, 'additional_images', '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(291, 103, 'Modules\\Product\\Entities\\Product', 37, 'additional_images', '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(292, 102, 'Modules\\Product\\Entities\\Product', 37, 'additional_images', '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(293, 101, 'Modules\\Product\\Entities\\Product', 37, 'additional_images', '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(294, 42, 'Modules\\Product\\Entities\\Product', 21, 'base_image', '2019-12-30 14:39:54', '2019-12-30 14:39:54'),
(295, 17, 'Modules\\Product\\Entities\\Product', 21, 'additional_images', '2019-12-30 14:39:54', '2019-12-30 14:39:54'),
(296, 16, 'Modules\\Product\\Entities\\Product', 21, 'additional_images', '2019-12-30 14:39:54', '2019-12-30 14:39:54'),
(297, 18, 'Modules\\Product\\Entities\\Product', 22, 'base_image', '2019-12-30 14:40:11', '2019-12-30 14:40:11'),
(298, 20, 'Modules\\Product\\Entities\\Product', 22, 'additional_images', '2019-12-30 14:40:11', '2019-12-30 14:40:11'),
(299, 19, 'Modules\\Product\\Entities\\Product', 22, 'additional_images', '2019-12-30 14:40:11', '2019-12-30 14:40:11'),
(300, 76, 'Modules\\FlashSale\\Entities\\FlashSale', 4, 'banner_image', '2019-12-30 15:01:17', '2019-12-30 15:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `featured_categories`
--

CREATE TABLE `featured_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `feature_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `featured_categories`
--

INSERT INTO `featured_categories` (`id`, `feature_type`, `position`, `category_id`, `created_at`, `updated_at`) VALUES
(7, 'mobile_home_top', 0, 4, '2019-12-27 16:18:58', '2019-12-27 17:41:05'),
(8, 'mobile_home_top', 1, 8, '2019-12-27 16:19:31', '2019-12-27 17:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `featured_category_translations`
--

CREATE TABLE `featured_category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `featured_category_id` int(11) NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `disk` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `user_id`, `filename`, `disk`, `path`, `extension`, `mime`, `size`, `created_at`, `updated_at`) VALUES
(1, 1, 'slider_image_1.jpeg', 'public_storage', 'media/hQfen1vyTMECY5aCHZBj92BHVd9gv0Do5O0LbXvT.jpeg', 'jpeg', 'image/jpeg', '74629', '2019-09-23 10:44:50', '2019-09-23 10:44:50'),
(2, 1, 'slider_image_2.jpeg', 'public_storage', 'media/xX7U7GiN92peRUTCx3FuiOBE3d6c3RsfMYNaNwT9.jpeg', 'jpeg', 'image/jpeg', '137733', '2019-09-23 10:44:50', '2019-09-23 10:44:50'),
(3, 1, 'slider_image_3.jpeg', 'public_storage', 'media/lUfSwSGFft14pRE4Qc550KSHigLn8tnBrPHTK2Xx.jpeg', 'jpeg', 'image/jpeg', '56841', '2019-09-23 10:44:50', '2019-09-23 10:44:50'),
(4, 1, 'slider_image_4.jpeg', 'public_storage', 'media/6YnKhamxhUzMKbJ2ixQfUJBUPvH36TbFHrZ3EXKq.jpeg', 'jpeg', 'image/jpeg', '145849', '2019-09-23 10:44:51', '2019-09-23 10:44:51'),
(5, 1, 'apple.png', 'public_storage', 'media/MhBR8LmsGkBu5S8Wb0zHgg74Q24kYPiCvd3RXJs4.png', 'png', 'image/png', '2225', '2019-09-23 10:52:12', '2019-09-23 10:52:12'),
(6, 1, 'nike.png', 'public_storage', 'media/HucB7hdMzLBPAHDPFeOHdjeGrtY9hB7GwNeu70mJ.png', 'png', 'image/png', '1979', '2019-09-23 10:52:12', '2019-09-23 10:52:12'),
(7, 1, 'reebok.png', 'public_storage', 'media/9RGb7HtWZKlAocr5Oozt7tyhiv5yGhFR36a6wd4F.png', 'png', 'image/png', '2554', '2019-09-23 10:52:13', '2019-09-23 10:52:13'),
(8, 1, 'laptops.jpeg', 'public_storage', 'media/0GRpFRVmVRoUHGAs3MSQt6tz6sKUQgRKnEgi2bTX.jpeg', 'jpeg', 'image/jpeg', '8793', '2019-09-23 10:57:10', '2019-09-23 10:57:10'),
(9, 1, 'electronics.jpeg', 'public_storage', 'media/wU5WrAzA4goECSJkSiIk1Z85MhfRolhJjVpIfGuX.jpeg', 'jpeg', 'image/jpeg', '14766', '2019-09-23 10:57:10', '2019-09-23 10:57:10'),
(10, 1, 'mobiles.jpeg', 'public_storage', 'media/StvRopwLyHZgC7fVV0ROCppZ6gO00BQVLJtRLC4R.jpeg', 'jpeg', 'image/jpeg', '10550', '2019-09-23 10:57:10', '2019-09-23 10:57:10'),
(11, 1, 'adara-headboard-fritz-charcoal_5.jpg', 'public_storage', 'media/x5ES4HsnRBURGrzuYrSBzjO7NmL10faJJ9M9uv2C.jpeg', 'jpeg', 'image/jpeg', '1058', '2019-12-27 12:49:34', '2019-12-27 12:49:34'),
(12, 1, 'FURNISHING-BASE-Favicon.png', 'public_storage', 'media/EUIB8WhLug7Jl5lZglgJCwZISpKkzra67HX0wyI1.png', 'png', 'image/png', '4745', '2019-12-27 12:52:25', '2019-12-27 12:52:25'),
(13, 1, 'f-logo.png', 'public_storage', 'media/qZy9UhruOhOK332YiUDg9PxflCRmU8dTFEzb3T6P.png', 'png', 'image/png', '76957', '2019-12-27 12:52:45', '2019-12-27 12:52:45'),
(14, 1, 'bed.jpg', 'public_storage', 'media/gdL9yy3bRUiw4dvMDesi6HCg1WCKmu5PUfuTWM4K.jpeg', 'jpeg', 'image/jpeg', '21472', '2019-12-27 12:55:05', '2019-12-27 12:55:05'),
(15, 1, 'adara-headboard-zuma-gray.jpg', 'public_storage', 'media/84odyTd3pqdGVXBsflyKxIPwIbMKizbHPhGFXied.jpeg', 'jpeg', 'image/jpeg', '855', '2019-12-27 13:19:06', '2019-12-27 13:19:06'),
(16, 1, 'adara-headboard.jpg', 'public_storage', 'media/9fOJFJccpLxqAyzCry0DPGoQPhDwQK7vYBTa74zD.jpeg', 'jpeg', 'image/jpeg', '16350', '2019-12-27 13:19:06', '2019-12-27 13:19:06'),
(17, 1, 'adara-headboard-zuma-gray_2_1.jpg', 'public_storage', 'media/1sieUmkKEVVHkPrXMa9CwoI2VvDdYJgHmOH2zMTz.jpeg', 'jpeg', 'image/jpeg', '1140', '2019-12-27 13:19:06', '2019-12-27 13:19:06'),
(18, 1, 'Candice_Headboard.jpg', 'public_storage', 'media/dtlaIzJXcuXuHZ8QMLptlv7JZ0lPHNdEcEIvGdGx.jpeg', 'jpeg', 'image/jpeg', '69857', '2019-12-27 13:31:35', '2019-12-27 13:31:35'),
(19, 1, 'Candice+Upholstered+Wingback+Headboard (1).jpg', 'public_storage', 'media/nf0it9usQH82kIISS1DcDabDBX3kmXbg0EgZz7l1.jpeg', 'jpeg', 'image/jpeg', '3794', '2019-12-27 13:31:35', '2019-12-27 13:31:35'),
(20, 1, 'Candice+Upholstered+Wingback+Headboard.jpg', 'public_storage', 'media/x06EKSrylVQdo4xJj96Chh2z3qgEFGqL9jxWUpZA.jpeg', 'jpeg', 'image/jpeg', '3837', '2019-12-27 13:31:35', '2019-12-27 13:31:35'),
(21, 1, 'Leflore+2+Drawer+Nightstand (2).jpg', 'public_storage', 'media/VPXrZGRk9Ngnm4jMtofOYjPijqhKo7JOtmkXnVmd.jpeg', 'jpeg', 'image/jpeg', '396', '2019-12-27 13:36:27', '2019-12-27 13:36:27'),
(22, 1, 'Leflore+2+Drawer+Nightstand (1).jpg', 'public_storage', 'media/LlDV7TAi30Ta2FFcTr4aJ57ZFF2F4uq7tNW1g7um.jpeg', 'jpeg', 'image/jpeg', '1301', '2019-12-27 13:36:27', '2019-12-27 13:36:27'),
(23, 1, 'Leflore+2+Drawer+Nightstand.jpg', 'public_storage', 'media/V3KZ7cE3VtjfiiE4yEHk9gQi6gDPoK5RKalXZUYr.jpeg', 'jpeg', 'image/jpeg', '33730', '2019-12-27 13:36:28', '2019-12-27 13:36:28'),
(24, 1, 'Darius+End+Table+with+Storage (1).jpg', 'public_storage', 'media/lMvqLQdYrREFdHHWjMkBuYWEIcb3YV7Xx65YMts7.jpeg', 'jpeg', 'image/jpeg', '30160', '2019-12-27 13:40:07', '2019-12-27 13:40:07'),
(25, 1, 'Darius+End+Table+with+Storage (2).jpg', 'public_storage', 'media/g18M9qMBBruLRFqCnM7Ny5ySPl973l17WU3Gi6o0.jpeg', 'jpeg', 'image/jpeg', '32015', '2019-12-27 13:40:07', '2019-12-27 13:40:07'),
(26, 1, 'Darius+End+Table+with+Storage.jpg', 'public_storage', 'media/t72UqPjAyooRN92mbhyzPaLCCyGOrW9CikvlLebp.jpeg', 'jpeg', 'image/jpeg', '78251', '2019-12-27 13:40:08', '2019-12-27 13:40:08'),
(27, 1, 'Brand1.jpg', 'public_storage', 'media/uIrRsu8Wp8q3uiGGysY5Pjy4NUCqLx6btMYpsFHF.jpeg', 'jpeg', 'image/jpeg', '4082', '2019-12-27 14:31:28', '2019-12-27 14:31:28'),
(28, 1, 'brand2.jpg', 'public_storage', 'media/LjYvvJLOOanYcMHUvmJ2McUZl1W4HSl3blyZjWdT.jpeg', 'jpeg', 'image/jpeg', '5436', '2019-12-27 14:31:29', '2019-12-27 14:31:29'),
(29, 1, 'brand3.jpg', 'public_storage', 'media/8krSGSczdAZfkMplNfDbQg9EKDtNWK8P34rR3LMI.jpeg', 'jpeg', 'image/jpeg', '110608', '2019-12-27 14:31:29', '2019-12-27 14:31:29'),
(30, 1, 'brand4.jpg', 'public_storage', 'media/2kPAFgBkhhLK0Hfb0xZPL4EwqqJbrumHCpgUMLQB.jpeg', 'jpeg', 'image/jpeg', '4788', '2019-12-27 14:40:22', '2019-12-27 14:40:22'),
(31, 1, 'Decker+Papasan+Chair (1).jpg', 'public_storage', 'media/U0Wy9JfuV33NTdG1aDnpvN0zUYfKO6LcxxsgIvYk.jpeg', 'jpeg', 'image/jpeg', '4104', '2019-12-27 14:46:04', '2019-12-27 14:46:04'),
(32, 1, 'Decker+Papasan+Chair.jpg', 'public_storage', 'media/B4NTxcprsLqX6f14qMus0AyM9sUUyAbnmk9Dg9Rx.jpeg', 'jpeg', 'image/jpeg', '79809', '2019-12-27 14:46:04', '2019-12-27 14:46:04'),
(33, 1, 'STL_Room+featuring+Decker+Papasan+Chair_49669910.jpg', 'public_storage', 'media/GXgG9jS2AUVT6iO9Mfbl5dYSvVv2AgWU28YLDR97.jpeg', 'jpeg', 'image/jpeg', '4982', '2019-12-27 14:46:04', '2019-12-27 14:46:04'),
(34, 1, 'montello-upholstered-bench.jpg', 'public_storage', 'media/m1xSitaRDK2GPAXQY4k4Y2IBklOLfXWhWkP1cG14.jpeg', 'jpeg', 'image/jpeg', '17387', '2019-12-27 14:49:30', '2019-12-27 14:49:30'),
(35, 1, 'Montello+Upholstered+Bench (1).jpg', 'public_storage', 'media/R3hCtCLV4QdhFCYfE8tMWZaK4Z3YTcsMYOCOR6bu.jpeg', 'jpeg', 'image/jpeg', '67910', '2019-12-27 14:49:30', '2019-12-27 14:49:30'),
(36, 1, 'Montello+Upholstered+Bench (2).jpg', 'public_storage', 'media/cpsuhZWA3c8GXJzMJM7Ul8VL6FFqWRMp3UznWHtB.jpeg', 'jpeg', 'image/jpeg', '1117', '2019-12-27 14:49:30', '2019-12-27 14:49:30'),
(37, 1, 'Montello+Upholstered+Bench.jpg', 'public_storage', 'media/r2RHjyM11U5h57FIBCyC777SUG6BGtEWpxiBS7Pp.jpeg', 'jpeg', 'image/jpeg', '1216', '2019-12-27 14:49:31', '2019-12-27 14:49:31'),
(38, 1, 'Denning+Coffee+Table+with+Storage (1).jpg', 'public_storage', 'media/hxcyGW1e34V542XMoRFDKTkYrYuERA3HNAv6Sd3n.jpeg', 'jpeg', 'image/jpeg', '1367', '2019-12-27 14:52:55', '2019-12-27 14:52:55'),
(39, 1, 'Denning+Coffee.jpg', 'public_storage', 'media/x6id51vmCTt9uyKcqRBNYLPiCERNZcvn1ME5U8Cp.jpeg', 'jpeg', 'image/jpeg', '114493', '2019-12-27 14:52:55', '2019-12-27 14:52:55'),
(40, 1, 'Denning+Coffee+Table+with+Storage (2).jpg', 'public_storage', 'media/2HpkLdnJPHmeNlQ30y41u8QiIlSBre2SXMboL1mY.jpeg', 'jpeg', 'image/jpeg', '1353', '2019-12-27 14:52:55', '2019-12-27 14:52:55'),
(41, 1, 'Denning+Coffee+Table+with+Storage.jpg', 'public_storage', 'media/Jgam47DmPUH7yW6aLqZOjI4j1EkBjtmsOg3Ov8Lg.jpeg', 'jpeg', 'image/jpeg', '1293', '2019-12-27 14:52:56', '2019-12-27 14:52:56'),
(42, 1, 'adara-headboard.jpg', 'public_storage', 'media/QOutHHmKbatrgUsinUqXNtHn4cuqHh2vsltMma9C.jpeg', 'jpeg', 'image/jpeg', '16350', '2019-12-27 14:55:42', '2019-12-27 14:55:42'),
(43, 1, 'Denning+Coffee+Table+with+Storage (3).jpg', 'public_storage', 'media/kwivR1rGA2xtfnjdbmHix5bLxg1aaBwOGrK7BJRa.jpeg', 'jpeg', 'image/jpeg', '58425', '2019-12-27 15:06:06', '2019-12-27 15:06:06'),
(44, 1, 'Denning+Coffee+Table+with+Storage (1).jpg', 'public_storage', 'media/eqA0Wvdi3y52bf1UbPrRuLYIvEbdXPX9BHMdzkgn.jpeg', 'jpeg', 'image/jpeg', '99563', '2019-12-27 15:06:06', '2019-12-27 15:06:06'),
(45, 1, 'Denning+Coffee+Table+with+Storage.jpg', 'public_storage', 'media/kpr6bwB2PLfnuoimFGAoT5vJBwG1MHaxOWyRqj64.jpeg', 'jpeg', 'image/jpeg', '127304', '2019-12-27 15:06:06', '2019-12-27 15:06:06'),
(46, 1, 'Odum+Coffee+Table (1).jpg', 'public_storage', 'media/VY7JI8XyGcDrSlWq68mUSSsATLLkCb4Z7lnVoJY1.jpeg', 'jpeg', 'image/jpeg', '39837', '2019-12-27 15:11:11', '2019-12-27 15:11:11'),
(47, 1, 'Odum+Coffee+Table (2).jpg', 'public_storage', 'media/oEcEjTY9SjVlm58hN4bU9lM0fmuOac8lHVtSjkhm.jpeg', 'jpeg', 'image/jpeg', '75781', '2019-12-27 15:11:11', '2019-12-27 15:11:11'),
(48, 1, 'Odum+Coffee+Table.jpg', 'public_storage', 'media/dxsWtyTutYM60OYFzpKnmUsCVK0jASCn5KFuy68k.jpeg', 'jpeg', 'image/jpeg', '105397', '2019-12-27 15:11:12', '2019-12-27 15:11:12'),
(49, 1, 'default_name (1).jpg', 'public_storage', 'media/C6JXfnTa4MGzb5KwUMtQ8qYXZCmAGKzRVAAjVKf2.jpeg', 'jpeg', 'image/jpeg', '20445', '2019-12-27 15:15:25', '2019-12-27 15:15:25'),
(50, 1, 'default_name.jpg', 'public_storage', 'media/RSMPNPgtYBAfoBQsXRvDtuTEiC41wC5khinvLjt5.jpeg', 'jpeg', 'image/jpeg', '47304', '2019-12-27 15:15:25', '2019-12-27 15:15:25'),
(51, 1, 'Gillett+Coffee+Table.jpg', 'public_storage', 'media/NqezXkYStXSl3wQ3pR9Oh8EjMBBBoHvL2kxSKKU1.jpeg', 'jpeg', 'image/jpeg', '77458', '2019-12-27 15:15:25', '2019-12-27 15:15:25'),
(52, 1, 'Henjes+Coffee+Table.jpg', 'public_storage', 'media/Vmp3Eso9bjj35cRO7g4nfd5Umpfe8JSEaDugC3gA.jpeg', 'jpeg', 'image/jpeg', '103742', '2019-12-27 15:15:25', '2019-12-27 15:15:25'),
(53, 1, 'Rainey+Coffee+Table.jpg', 'public_storage', 'media/06lRKI9MSMrh8FyCWO6BP0ETV4RLQKTqqsHMMNNd.jpeg', 'jpeg', 'image/jpeg', '116609', '2019-12-27 15:15:25', '2019-12-27 15:15:25'),
(54, 1, 'Rhea+Coffee+Table.jpg', 'public_storage', 'media/WCfXCkXG2HotqAJVaDtiiPDa2okk538ARwDKat42.jpeg', 'jpeg', 'image/jpeg', '100217', '2019-12-27 15:15:26', '2019-12-27 15:15:26'),
(55, 1, 'Sands+Upholstered+Bench.jpg', 'public_storage', 'media/TwSEtYc6DRbG3PrqUlYlCx0tVhHeXMEjhJebCmWA.jpeg', 'jpeg', 'image/jpeg', '101532', '2019-12-27 15:30:09', '2019-12-27 15:30:09'),
(56, 1, '4ef6e68c-b101-4482-b369-ca866e096f00.jpg', 'public_storage', 'media/y3Dd6MDqDWmzdnYZHbb06pWY7xEPr9UEQzmOmLsz.jpeg', 'jpeg', 'image/jpeg', '54167', '2019-12-27 15:36:58', '2019-12-27 15:36:58'),
(57, 1, 'download.jpg', 'public_storage', 'media/04816IPy0ecvRXDJ2k19Et7V9WMrGdAJ3YFLXwUA.jpeg', 'jpeg', 'image/jpeg', '6014', '2019-12-27 15:43:24', '2019-12-27 15:43:24'),
(58, 1, 'images (1).jpg', 'public_storage', 'media/xVJBkMZ0DjPofFu8NJQDqFZgrYDQ6fDNroE7kc0R.jpeg', 'jpeg', 'image/jpeg', '5488', '2019-12-27 15:44:35', '2019-12-27 15:44:35'),
(59, 1, 'spicer-console-table.jpg', 'public_storage', 'media/HHExRZ4V6neoP0Z6FzCQszLLhxMsoYtek2DGDr71.jpeg', 'jpeg', 'image/jpeg', '9709', '2019-12-27 15:51:09', '2019-12-27 15:51:09'),
(60, 1, '5553-series-console-table.jpg', 'public_storage', 'media/DJvyN06dPwc9jlpodRL0VlaVe6KDch12mwP1REuy.jpeg', 'jpeg', 'image/jpeg', '16767', '2019-12-27 15:51:09', '2019-12-27 15:51:09'),
(61, 1, 'images.jpg', 'public_storage', 'media/Fb9k7A9EifZAc6FZhseNr5EXWD6Mcf0HmnEeAqim.jpeg', 'jpeg', 'image/jpeg', '6300', '2019-12-27 15:54:31', '2019-12-27 15:54:31'),
(62, 1, 'images (2).jpg', 'public_storage', 'media/d4XM5uSqEXkaLrdFro7b79icOXOELdMR4VcgmqlI.jpeg', 'jpeg', 'image/jpeg', '4466', '2019-12-27 15:54:43', '2019-12-27 15:54:43'),
(63, 1, '1.jpg', 'public_storage', 'media/0oQC7tNs2thoo35j07Hw2AkGoq0KFJLmSj3NiGIE.jpeg', 'jpeg', 'image/jpeg', '31653', '2019-12-27 15:59:11', '2019-12-27 15:59:11'),
(64, 1, '2.jpg', 'public_storage', 'media/ig8vVeJhtzy5AAiMxvZ3SPOVSElRoCUm6VrYA5WP.jpeg', 'jpeg', 'image/jpeg', '25372', '2019-12-27 15:59:11', '2019-12-27 15:59:11'),
(65, 1, '41HvOck6l+L._SY355_.jpg', 'public_storage', 'media/eG30sikoPCLRJKKtVRxr7TUPCgPX1AHG2vWHa9az.jpeg', 'jpeg', 'image/jpeg', '6126', '2019-12-27 15:59:11', '2019-12-27 15:59:11'),
(66, 1, 'images (1).jpg', 'public_storage', 'media/rvDP2ATgsPA0ipZGHRsx6V9oaHPjajhfT8P2D6WF.jpeg', 'jpeg', 'image/jpeg', '29336', '2019-12-27 16:38:37', '2019-12-27 16:38:37'),
(67, 1, '1.png', 'public_storage', 'media/xyhraIcJ2Tv3hS400SoZAgHo03cHj1FrvR6r0Unj.png', 'png', 'image/png', '167467', '2019-12-27 16:38:37', '2019-12-27 16:38:37'),
(68, 1, 'images (2).jpg', 'public_storage', 'media/OLrncAPcsFmJVPweH0VyH60dahQehu4iTtDwLV3o.jpeg', 'jpeg', 'image/jpeg', '30229', '2019-12-27 16:38:38', '2019-12-27 16:38:38'),
(69, 1, 'images (4.jpg', 'public_storage', 'media/lHjxbPgATAXW7EvEQW0hJLZT6SuGkouA7jrvxs70.jpeg', 'jpeg', 'image/jpeg', '21506', '2019-12-27 16:38:38', '2019-12-27 16:38:38'),
(70, 1, 'images.jpg', 'public_storage', 'media/0aWLo1RUtpAmXfNqXmnJwB4NQCJvsoBO4yXwRCMR.jpeg', 'jpeg', 'image/jpeg', '33420', '2019-12-27 16:38:38', '2019-12-27 16:38:38'),
(71, 1, 'Gillett+Coffee+Table.jpg', 'public_storage', 'media/W9jP4W0DsX4yKqotJsIh2VdFJ1SNmMs0JWpG9qy3.jpeg', 'jpeg', 'image/jpeg', '77458', '2019-12-27 16:44:18', '2019-12-27 16:44:18'),
(72, 1, 'brand3.jpg', 'public_storage', 'media/9ELEC1me7hdvTvtuK6hgCqdtx12RlqFcJ2xqkNs9.jpeg', 'jpeg', 'image/jpeg', '110608', '2019-12-27 18:15:51', '2019-12-27 18:15:51'),
(73, 1, 'Ginny+Pet+Crate.jpg', 'public_storage', 'media/ChcK7scPr86opNpqp6i8Zlh8xzhzoBEqONhNBTxh.jpeg', 'jpeg', 'image/jpeg', '92657', '2019-12-27 18:16:29', '2019-12-27 18:16:29'),
(74, 1, 'Orin+Sofa+Entryway+Hallway+Hall+Console+Table (1).jpg', 'public_storage', 'media/xNj56nsS9LPJbHV9yrab9VskIcR2F5Puq16AwnM0.jpeg', 'jpeg', 'image/jpeg', '33928', '2019-12-27 18:22:28', '2019-12-27 18:22:28'),
(75, 1, 'Orin+Sofa+Entryway+Hallway+Hall+Console+Table (2).jpg', 'public_storage', 'media/tG9Iewy7sA5dSpQ17TxmhdmqsnvPIW0hEfHanvAU.jpeg', 'jpeg', 'image/jpeg', '34561', '2019-12-27 18:22:28', '2019-12-27 18:22:28'),
(76, 1, 'Orin+Sofa+Entryway+Hallway+Hall+Console+Table.jpg', 'public_storage', 'media/HSAvGXHqoZSjlbG6TZV9yhmVHFitjqhOkvTKcimo.jpeg', 'jpeg', 'image/jpeg', '74560', '2019-12-27 18:22:28', '2019-12-27 18:22:28'),
(77, 1, 'Caiden+TV.jpg', 'public_storage', 'media/Xavfy0EdNc2BnyarjNlRYq9W98W4mWWJZyPZi0wZ.jpeg', 'jpeg', 'image/jpeg', '93635', '2019-12-27 18:28:06', '2019-12-27 18:28:06'),
(78, 1, 'Caiden+T.jpg', 'public_storage', 'media/7MG3BNT1vEuUBP5CX70Vlzwyyx4bgbrmJer0Q3qn.jpeg', 'jpeg', 'image/jpeg', '62164', '2019-12-27 18:28:06', '2019-12-27 18:28:06'),
(79, 1, 'Caiden+TV1.jpg', 'public_storage', 'media/oRZqAmT6kbsIbc0T3jL1f4erBaMu2rbo7FfpXGja.jpeg', 'jpeg', 'image/jpeg', '29347', '2019-12-27 18:28:07', '2019-12-27 18:28:07'),
(80, 1, 'Caiden+TV+Stand+for+TVs+up+to+65+inches.jpg', 'public_storage', 'media/fh21cDks9ZfEchZflyLGxFwqCTtNGvboAc6jUSgW.jpeg', 'jpeg', 'image/jpeg', '54175', '2019-12-27 18:28:07', '2019-12-27 18:28:07'),
(81, 1, 'Jennifer+Wood+24%22+Bar+Stool (2).jpg', 'public_storage', 'media/tHubazggyU4rH1QFIpEaoiaSEincaN6I5aK4TvBh.jpeg', 'jpeg', 'image/jpeg', '35251', '2019-12-27 18:33:46', '2019-12-27 18:33:46'),
(82, 1, 'Jennifer+Wood+24%22+Bar+Stool (1).jpg', 'public_storage', 'media/28AHf7YzEAUNTRakW1EEAlqpcgkxenz7zfxfuzLA.jpeg', 'jpeg', 'image/jpeg', '55938', '2019-12-27 18:33:46', '2019-12-27 18:33:46'),
(83, 1, 'Jennifer+Wood+24%22+Bar+Stool.jpg', 'public_storage', 'media/5R2lrJH0N2yqGSRIXUJzV41Qj7y37syIo0OCqxPr.jpeg', 'jpeg', 'image/jpeg', '86462', '2019-12-27 18:33:46', '2019-12-27 18:33:46'),
(84, 1, 'Genthner+1+Drawer+Desk (1).jpg', 'public_storage', 'media/r6ykyQtkqCW3g8duc3VkTJDf8bUkgeYXgvG5cdUk.jpeg', 'jpeg', 'image/jpeg', '22151', '2019-12-27 18:37:20', '2019-12-27 18:37:20'),
(85, 1, 'Genthner+1+Drawer+Desk (2).jpg', 'public_storage', 'media/NU0EGKEn20Lsdc1pllttiJNuYVKZs01vAAK33Sqi.jpeg', 'jpeg', 'image/jpeg', '35351', '2019-12-27 18:37:20', '2019-12-27 18:37:20'),
(86, 1, 'Genthner+1+Drawer+Desk (4).jpg', 'public_storage', 'media/bi6VmD6wTejeScRLMMDiIv9O4ElM6ari9uSLkXHu.jpeg', 'jpeg', 'image/jpeg', '18493', '2019-12-27 18:37:20', '2019-12-27 18:37:20'),
(87, 1, 'Genthner+1+Drawer+Desk (3).jpg', 'public_storage', 'media/27wcEsDNdi4XcNmENpVomErbq2KJ8n16dXpHGsh2.jpeg', 'jpeg', 'image/jpeg', '38282', '2019-12-27 18:37:20', '2019-12-27 18:37:20'),
(88, 1, 'Genthner+1+Drawer+Desk.jpg', 'public_storage', 'media/fy5LEolwctPsLl4MIe3Eyf4eKDlX9XN2oSMZLcpH.jpeg', 'jpeg', 'image/jpeg', '63958', '2019-12-27 18:37:21', '2019-12-27 18:37:21'),
(89, 1, 'Latimer+Upholstered+Storage+Bench (1).jpg', 'public_storage', 'media/u7zrb3WjEr0FEkljKsUy74TXzv1Tjd3bWapiuit2.jpeg', 'jpeg', 'image/jpeg', '41227', '2019-12-27 18:43:38', '2019-12-27 18:43:38'),
(90, 1, 'Latimer+Upholstered+Storage+Bench (2).jpg', 'public_storage', 'media/nLeQ0KaEN7xXFjNRT7FLWzNevsgTm6g7haLaIjt7.jpeg', 'jpeg', 'image/jpeg', '120401', '2019-12-27 18:43:38', '2019-12-27 18:43:38'),
(91, 1, 'Latimer+Upholstered+Storage+Bench (3).jpg', 'public_storage', 'media/6lkwr392TcfqQrjH700N3a6ALj8IkPvfapqTXrOn.jpeg', 'jpeg', 'image/jpeg', '56045', '2019-12-27 18:43:38', '2019-12-27 18:43:38'),
(92, 1, 'Latimer+Upholstered+Storage+Bench.jpg', 'public_storage', 'media/ik2pBTvvi9wdSJhPKvBO2FaMuFvvbVDhYyYG4Lj5.jpeg', 'jpeg', 'image/jpeg', '93322', '2019-12-27 18:43:39', '2019-12-27 18:43:39'),
(93, 1, 'Rebello+Sun+Lounger+Set (1).jpg', 'public_storage', 'media/k3I7GzvPhq8JlSQCrCREtvUmQW7m3ikEsM1O2MHR.jpeg', 'jpeg', 'image/jpeg', '167795', '2019-12-27 18:46:32', '2019-12-27 18:46:32'),
(94, 1, 'Rebello+Sun+Lounger+Set (2).jpg', 'public_storage', 'media/B49VLJaYDSeg7AvK66BpvgEsWGw3eZmSbdhcfzlh.jpeg', 'jpeg', 'image/jpeg', '104551', '2019-12-27 18:46:33', '2019-12-27 18:46:33'),
(95, 1, 'STL_Coastal+Outdoor+Design_42945703.jpg', 'public_storage', 'media/kJSNy6gKgko1XT9XuiL0gUIOr8CtfjpaGFtrtuS2.jpeg', 'jpeg', 'image/jpeg', '151968', '2019-12-27 18:46:33', '2019-12-27 18:46:33'),
(96, 1, 'Rebello+Sun+Lounger+Set.jpg', 'public_storage', 'media/YvEpD4OFMnSO4erPsTUKwVQjalDt4Yrv93knRAI5.jpeg', 'jpeg', 'image/jpeg', '110363', '2019-12-27 18:46:33', '2019-12-27 18:46:33'),
(97, 1, 'Vernonburg+Intersecting+Wall+Shelf (1).jpg', 'public_storage', 'media/z2NmBMkLbApvwPFW0oTISLewO9yARlrx5EeWFuS8.jpeg', 'jpeg', 'image/jpeg', '24024', '2019-12-27 18:51:05', '2019-12-27 18:51:05'),
(98, 1, 'Vernonburg+Intersecting+Wall+Shelf (2).jpg', 'public_storage', 'media/VjxFWcyiNR1A6j8aXyJHBWQOg9Pvmex7eCg3AxbC.jpeg', 'jpeg', 'image/jpeg', '46274', '2019-12-27 18:51:05', '2019-12-27 18:51:05'),
(99, 1, 'Vernonburg+Intersecting+Wall+Shelf (3).jpg', 'public_storage', 'media/aMoFMku2mWFXTeb0eVtGcSEsKY8QJEmSuqyc1vZL.jpeg', 'jpeg', 'image/jpeg', '25875', '2019-12-27 18:51:05', '2019-12-27 18:51:05'),
(100, 1, 'Vernonburg+Intersecting+Wall+Shelf.jpg', 'public_storage', 'media/ma7ruGl8dbJOtA47MpSYLtI5sJEpjcEY0T7kgTi5.jpeg', 'jpeg', 'image/jpeg', '50908', '2019-12-27 18:51:05', '2019-12-27 18:51:05'),
(101, 1, 'Newtown+36%22+Single+Bathroom+Vanity+Set (2).jpg', 'public_storage', 'media/Iw9PBIujdPHaSIXG7XTdAhyle9vNHanHikUGeSVB.jpeg', 'jpeg', 'image/jpeg', '25854', '2019-12-27 18:54:12', '2019-12-27 18:54:12'),
(102, 1, 'Newtown+36%22+Single+Bathroom+Vanity+Set (1).jpg', 'public_storage', 'media/iM4J2zNSdEVYKYZgidavoQs0LwHeoonJzulhpd6P.jpeg', 'jpeg', 'image/jpeg', '69723', '2019-12-27 18:54:12', '2019-12-27 18:54:12'),
(103, 1, 'Newtown+36%22+Single+Bathroom+Vanity+Set (3).jpg', 'public_storage', 'media/3Y0u9hO4SwUYNv5BJWFq3PUlWYwNPk5xt4p2oH75.jpeg', 'jpeg', 'image/jpeg', '39444', '2019-12-27 18:54:13', '2019-12-27 18:54:13'),
(104, 1, 'Newtown+36%22+Single+Bathroom+Vanity+Set (4).jpg', 'public_storage', 'media/EdkQBsGhb2aKnetZAl0qJwtpjsb4lOy92Qbf4HVi.jpeg', 'jpeg', 'image/jpeg', '68029', '2019-12-27 18:54:13', '2019-12-27 18:54:13'),
(105, 1, 'Newtown+36%22+Single+Bathroom+Vanity+Set.jpg', 'public_storage', 'media/1KnlWpc32ewDu6Hitm5n0aDcjLK4p0U8uspfCGDG.jpeg', 'jpeg', 'image/jpeg', '48725', '2019-12-27 18:54:13', '2019-12-27 18:54:13'),
(106, 1, 'Decker+Papasan+Chair.jpg', 'public_storage', 'media/19zKUM9OO3iw0qBjWrKQDCPzaWB4aHvQXpJSaWI0.jpeg', 'jpeg', 'image/jpeg', '79809', '2019-12-30 12:19:20', '2019-12-30 12:19:20'),
(107, 1, 'download.jpg', 'public_storage', 'media/NCraZVzzgbcCFQTmKs0palW6WCzoA15Nco4xEVrD.jpeg', 'jpeg', 'image/jpeg', '3512', '2019-12-30 14:48:28', '2019-12-30 14:48:28'),
(108, 1, 'images (1).png', 'public_storage', 'media/M1GgM56iZ2VBj5rEwWv1nubJvsey0pBjGu4gkawI.png', 'png', 'image/png', '4134', '2019-12-30 14:48:29', '2019-12-30 14:48:29'),
(109, 1, 'images (2).png', 'public_storage', 'media/O0wMk2qRS2cn2sgvZZgSuYCkyO9DO0px3BLW2KPF.png', 'png', 'image/png', '3125', '2019-12-30 14:48:29', '2019-12-30 14:48:29'),
(110, 1, 'images (3).png', 'public_storage', 'media/N3flkuum6zhnDGhAn1DoU2LtT6tzAF7acvtuxgRK.png', 'png', 'image/png', '7671', '2019-12-30 14:48:29', '2019-12-30 14:48:29'),
(111, 1, 'images.jpg', 'public_storage', 'media/wsCHiFx6JmgOb7Kjo5FdxBmkQmTiFb4fnbBEou31.jpeg', 'jpeg', 'image/jpeg', '4334', '2019-12-30 14:48:29', '2019-12-30 14:48:29'),
(112, 1, 'images.png', 'public_storage', 'media/bBcfrZSqUoCA6cMS49iT8h81qL6oeWwBqQsPdDzj.png', 'png', 'image/png', '5670', '2019-12-30 14:48:29', '2019-12-30 14:48:29'),
(113, 1, 'images (5).png', 'public_storage', 'media/PuI0fnOrZMOx1nKZqocEoRumlHlMhIsV2QmwOCsB.png', 'png', 'image/png', '3039', '2019-12-30 14:51:03', '2019-12-30 14:51:03'),
(114, 1, 'images (4).png', 'public_storage', 'media/aS4f3bvA1ygkg26QXK5ZZJWb9mBFKcBDVDdEXGG5.png', 'png', 'image/png', '3846', '2019-12-30 14:51:03', '2019-12-30 14:51:03'),
(115, 1, 'images (1).jpg', 'public_storage', 'media/lPZgEMD1c2UgW94umXibHboEYovpTRnQxmGQoWbc.jpeg', 'jpeg', 'image/jpeg', '16571', '2019-12-30 15:08:08', '2019-12-30 15:08:08'),
(116, 1, 'download (1).jpg', 'public_storage', 'media/pog2L7pk36JtjDKfEWTx4WEemyphF2ac8Y4043UR.jpeg', 'jpeg', 'image/jpeg', '9746', '2019-12-30 15:14:18', '2019-12-30 15:14:18'),
(117, 1, 'Genthner+1+Drawer+Desk.jpg', 'public_storage', 'media/IFyq3tDlJZPwZZpwjvHUG94PUCtdyi6dTqQCdBTo.jpeg', 'jpeg', 'image/jpeg', '63958', '2019-12-30 15:20:37', '2019-12-30 15:20:37'),
(118, 1, 'Rebello+Sun+Lounger+Set.jpg', 'public_storage', 'media/efui1wt8eJEpXtGcULYiCLNAB3qErdfxcI0z1iZW.jpeg', 'jpeg', 'image/jpeg', '110363', '2019-12-30 15:20:52', '2019-12-30 15:20:52'),
(119, 1, 'Latimer+Upholstered+Storage+Bench.jpg', 'public_storage', 'media/lEyfhyk7nkYX2evLhGs2jILy2MTpBkz04llAqSlM.jpeg', 'jpeg', 'image/jpeg', '93322', '2019-12-30 15:21:09', '2019-12-30 15:21:09'),
(120, 1, 'Vernonburg+Intersecting+Wall+Shelf.jpg', 'public_storage', 'media/0rsE13VYtwnUB4LYuvK1HDDZWmf67A5qaVeOOICY.jpeg', 'jpeg', 'image/jpeg', '50908', '2019-12-30 15:21:25', '2019-12-30 15:21:25'),
(121, 1, 'FURNISHING BASE 2.png', 'public_storage', 'media/ROypP1LIhHhfrGSPEEQ9uGtUDX61rOGLHxXXmBAt.png', 'png', 'image/png', '19486714', '2020-01-06 13:22:58', '2020-01-06 13:22:58'),
(122, 1, 'furnishing.png', 'public_storage', 'media/6eV8QPvvLymyTdVMCktnnojSJ6Zc2tCW3cYqiyJc.png', 'png', 'image/png', '528844', '2020-01-09 10:38:43', '2020-01-09 10:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `flash_sales`
--

CREATE TABLE `flash_sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start_datetime` datetime NOT NULL,
  `end_datetime` datetime NOT NULL,
  `flash_sale_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flash_sales`
--

INSERT INTO `flash_sales` (`id`, `slug`, `title`, `description`, `start_datetime`, `end_datetime`, `flash_sale_type`, `is_active`, `created_at`, `updated_at`) VALUES
(4, 'rush-to-buy', 'Rush To Buy', 'Avail Best Offers & Discounts from Furnishing Base', '2019-12-30 00:00:00', '2019-12-31 23:59:00', 'deal_of_the_day', 1, '2019-12-30 15:01:17', '2019-12-30 15:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `flash_sale_products`
--

CREATE TABLE `flash_sale_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `flash_sale_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(18,4) UNSIGNED DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flash_sale_products`
--

INSERT INTO `flash_sale_products` (`id`, `flash_sale_id`, `product_id`, `price`, `position`, `created_at`, `updated_at`) VALUES
(8, 4, 21, '100.0000', 0, NULL, NULL),
(9, 4, 23, '100.0000', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flash_sale_translations`
--

CREATE TABLE `flash_sale_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `flash_sale_id` int(11) NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(7, 1, '2019-12-27 16:58:27', '2019-12-27 16:58:27'),
(8, 1, '2019-12-27 17:31:34', '2019-12-27 17:31:34');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `page_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `is_root` tinyint(1) NOT NULL DEFAULT '0',
  `is_fluid` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `parent_id`, `category_id`, `page_id`, `type`, `url`, `target`, `position`, `is_root`, `is_fluid`, `is_active`, `created_at`, `updated_at`) VALUES
(14, 7, NULL, NULL, NULL, 'URL', NULL, '_self', 0, 1, 0, 1, '2019-12-27 16:58:27', '2019-12-27 16:58:27'),
(15, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bedroom&page=1', '_self', 1, 0, 1, 1, '2019-12-27 16:58:51', '2020-01-31 11:49:54'),
(16, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=dining-room&page=1', '_self', 3, 0, 1, 1, '2019-12-27 16:59:06', '2020-01-31 11:50:13'),
(17, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=living-room&page=1', '_self', 2, 0, 1, 1, '2019-12-27 16:59:19', '2020-01-31 11:50:13'),
(18, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=accent&page=1', '_self', 0, 0, 1, 1, '2019-12-27 16:59:37', '2020-01-31 11:49:54'),
(19, 8, NULL, NULL, NULL, 'URL', NULL, '_self', 0, 1, 0, 1, '2019-12-27 17:31:34', '2019-12-27 17:31:34'),
(20, 8, 19, NULL, 3, 'page', NULL, '_self', 0, 0, 1, 1, '2019-12-27 17:33:21', '2019-12-27 17:36:05'),
(21, 8, 19, NULL, NULL, 'url', '/products', '_self', 1, 0, 1, 1, '2019-12-27 17:35:55', '2019-12-27 18:09:23'),
(22, 8, 19, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/', '_self', NULL, 0, 1, 1, '2019-12-27 17:36:24', '2019-12-31 11:51:40'),
(23, 7, 15, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=headboards&page=1', '_self', 1, 0, 1, 1, '2019-12-27 17:56:34', '2020-01-30 15:42:10'),
(24, 7, 15, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=nightstands&page=1', '_self', 0, 0, 1, 1, '2019-12-27 17:56:51', '2020-01-30 15:41:43'),
(25, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chairs&page=1', '_self', 1, 0, 1, 1, '2019-12-27 17:57:17', '2020-01-31 11:41:34'),
(26, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=benches&page=1', '_self', 0, 0, 1, 1, '2019-12-27 17:57:29', '2020-01-31 11:41:13'),
(27, 7, 17, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=coffee-tables&page=1', '_self', 3, 0, 1, 1, '2019-12-27 17:58:25', '2020-01-31 11:47:32'),
(28, 7, 17, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=end-tables&page=1', '_self', 2, 0, 1, 1, '2019-12-27 17:58:44', '2020-01-31 11:47:20'),
(29, 7, 17, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=console-tables&page=1', '_self', 0, 0, 1, 1, '2019-12-27 17:58:59', '2020-01-31 11:46:55'),
(30, 7, 17, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=tv-console-s&page=1', '_self', 1, 0, 1, 1, '2019-12-27 17:59:14', '2020-01-31 11:47:07'),
(31, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bar-stools&page=1', '_self', 6, 0, 1, 1, '2019-12-27 17:59:33', '2020-01-31 11:58:31'),
(32, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=desks&page=1', '_self', 5, 0, 1, 1, '2019-12-27 17:59:49', '2020-01-31 11:58:09'),
(33, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=ottomans&page=1', '_self', 4, 0, 1, 1, '2019-12-27 18:00:03', '2020-01-31 11:57:58'),
(34, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=outdoor&page=1', '_self', 3, 0, 1, 1, '2019-12-27 18:00:20', '2020-01-31 11:57:38'),
(35, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=shelves&page=1', '_self', 2, 0, 1, 1, '2019-12-27 18:00:36', '2020-01-31 11:57:26'),
(36, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=vanities&page=1', '_self', 1, 0, 1, 1, '2019-12-27 18:00:51', '2020-01-31 11:56:59'),
(37, 8, 19, NULL, NULL, 'url', 'contact', '_self', NULL, 0, 0, 1, '2019-12-27 18:08:25', '2019-12-27 18:08:25'),
(38, 8, 19, NULL, 4, 'page', NULL, '_self', NULL, 0, 1, 1, '2019-12-30 12:27:21', '2019-12-30 12:27:21'),
(41, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=youth&page=1', '_self', 6, 0, 1, 1, '2020-01-30 12:28:42', '2020-01-31 11:50:51'),
(42, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=headboard&page=1', '_self', 0, 0, 0, 1, '2020-01-30 12:29:58', '2020-01-31 12:13:51'),
(43, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=game&page=1', '_self', 5, 0, 1, 1, '2020-01-30 12:39:35', '2020-01-31 11:50:45'),
(44, 7, 14, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=outdoor-OUKu1rbD&page=1', '_self', 4, 0, 1, 1, '2020-01-30 12:41:33', '2020-01-31 11:50:34'),
(45, 7, 15, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=coffee-table&page=1', '_self', 2, 0, 0, 1, '2020-01-30 12:50:10', '2020-01-30 13:11:50'),
(46, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chest-accessory&page=1', '_self', 1, 0, 0, 1, '2020-01-30 13:12:08', '2020-01-31 12:14:15'),
(47, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=night-stand&page=1', '_self', 2, 0, 1, 1, '2020-01-30 13:13:09', '2020-01-31 12:14:35'),
(48, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bed&page=1', '_self', 3, 0, 1, 1, '2020-01-30 13:13:36', '2020-01-31 12:14:51'),
(49, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=youth-bedroom&page=1', '_self', 4, 0, 1, 1, '2020-01-30 13:15:30', '2020-01-31 12:15:13'),
(50, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=desk&page=1', '_self', 5, 0, 1, 1, '2020-01-30 13:15:58', '2020-01-31 12:15:43'),
(51, 7, 41, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chest&page=1', '_self', 6, 0, 0, 1, '2020-01-30 13:16:42', '2020-01-31 12:15:58'),
(52, 7, 43, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=table-3Xasnkgd&page=1', '_self', 1, 0, 1, 1, '2020-01-30 13:30:25', '2020-01-31 12:12:06'),
(54, 7, 43, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chair-1dj0byCd&page=1', '_self', 0, 0, 1, 1, '2020-01-30 13:31:32', '2020-01-31 12:11:36'),
(55, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chair-h8AlqCPG&page=1', '_self', 0, 0, 1, 1, '2020-01-30 13:32:21', '2020-01-31 12:03:07'),
(56, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=fireplace&page=1', '_self', 1, 0, 1, 1, '2020-01-30 13:32:56', '2020-01-31 12:04:58'),
(57, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=outdoor-seating-sets&page=1', '_self', 2, 0, 1, 1, '2020-01-30 13:37:21', '2020-01-31 12:05:13'),
(58, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=table-U0xgUm9s&page=1', '_self', 3, 0, 1, 1, '2020-01-30 13:37:47', '2020-01-31 12:05:30'),
(60, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=sofa&page=1', '_self', 4, 0, 1, 1, '2020-01-30 13:38:46', '2020-01-31 12:05:52'),
(61, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=coffee-table-60g9zI4G&page=1', '_self', 5, 0, 1, 1, '2020-01-30 13:39:18', '2020-01-31 12:09:09'),
(62, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=outdoor-dining&page=1', '_self', 6, 0, 1, 1, '2020-01-30 13:39:56', '2020-01-31 12:09:24'),
(63, 7, 44, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=dining-set-raxh8W7Q&page=1', '_self', 7, 0, 1, 1, '2020-01-30 14:56:19', '2020-01-31 12:09:48'),
(64, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=dining-chair&page=1', '_self', 6, 0, 1, 1, '2020-01-30 15:43:54', '2020-01-31 11:45:00'),
(65, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=dining-set&page=1', '_self', 2, 0, 1, 1, '2020-01-30 15:45:22', '2020-01-31 11:45:00'),
(66, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=dining-table&page=1', '_self', 3, 0, 1, 1, '2020-01-30 15:45:59', '2020-01-31 11:45:00'),
(67, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bar&page=1', '_self', 4, 0, 1, 1, '2020-01-30 15:46:42', '2020-01-31 11:45:00'),
(68, 7, 16, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=regular-ht&page=1', '_self', 5, 0, 1, 1, '2020-01-30 15:51:53', '2020-01-31 11:45:00'),
(69, 7, 18, NULL, NULL, 'url', '#', '_self', 0, 0, 1, 1, '2020-01-30 15:53:53', '2020-01-31 11:56:59'),
(70, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=table-stand&page=1', '_self', 9, 0, 1, 1, '2020-01-30 15:55:10', '2020-01-31 11:59:42'),
(71, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=lighting-accessory&page=1', '_self', 10, 0, 1, 1, '2020-01-30 15:55:49', '2020-01-31 11:59:58'),
(72, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=shelf-storage&page=1', '_self', 11, 0, 1, 1, '2020-01-30 15:58:54', '2020-01-31 12:00:14'),
(73, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=table&page=1', '_self', 7, 0, 1, 1, '2020-01-30 15:59:38', '2020-01-31 11:58:47'),
(74, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=chair&page=1', '_self', 8, 0, 1, 1, '2020-01-30 16:01:40', '2020-01-31 11:59:29'),
(75, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=accent-chair&page=1', '_self', 12, 0, 1, 1, '2020-01-30 16:02:10', '2020-01-31 12:00:34'),
(76, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=hallway&page=1', '_self', 13, 0, 1, 1, '2020-01-30 16:06:20', '2020-01-31 12:00:50'),
(77, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bench-ottoman&page=1', '_self', 14, 0, 1, 1, '2020-01-30 16:08:10', '2020-01-31 12:01:03'),
(78, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=love-seat&page=1', '_self', 15, 0, 1, 1, '2020-01-30 16:08:48', '2020-01-31 12:01:41'),
(79, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bar-MXFdkhvD&page=1', '_self', 16, 0, 1, 1, '2020-01-30 16:09:24', '2020-01-31 12:01:55'),
(80, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=office&page=1', '_self', 17, 0, 1, 1, '2020-01-30 16:10:36', '2020-01-31 12:02:16'),
(81, 7, 18, NULL, NULL, 'url', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=office-desk&page=1', '_self', 18, 0, 1, 1, '2020-01-30 16:11:33', '2020-01-31 12:02:28');

-- --------------------------------------------------------

--
-- Table structure for table `menu_item_translations`
--

CREATE TABLE `menu_item_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_item_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_item_translations`
--

INSERT INTO `menu_item_translations` (`id`, `menu_item_id`, `locale`, `name`) VALUES
(14, 14, 'en', 'root'),
(15, 15, 'en', 'BEDROOM'),
(16, 16, 'en', 'DINING ROOM'),
(17, 17, 'en', 'LIVING ROOM'),
(18, 18, 'en', 'ACCENT'),
(19, 19, 'en', 'root'),
(20, 20, 'en', 'About Us'),
(21, 21, 'en', 'Shop'),
(22, 22, 'en', 'Home'),
(23, 23, 'en', 'Headboards'),
(24, 24, 'en', 'Nightstands'),
(25, 25, 'en', 'Chairs'),
(26, 26, 'en', 'Benches'),
(27, 27, 'en', 'Coffee Tables'),
(28, 28, 'en', 'End Tables'),
(29, 29, 'en', 'Console Tables'),
(30, 30, 'en', 'TV Consoles'),
(31, 31, 'en', 'Bar Stools'),
(32, 32, 'en', 'Desks'),
(33, 33, 'en', 'Ottomans'),
(34, 34, 'en', 'Outdoor'),
(35, 35, 'en', 'Shelves'),
(36, 36, 'en', 'Vanities'),
(37, 37, 'en', 'Contact US'),
(38, 38, 'en', 'Privacy Policy'),
(41, 41, 'en', 'YOUTH'),
(42, 42, 'en', 'Headboard'),
(43, 43, 'en', 'GAME'),
(44, 44, 'en', 'OUTDOOR'),
(45, 45, 'en', 'Coffee Table'),
(46, 46, 'en', 'Chest & Accessory'),
(47, 47, 'en', 'Night Stand'),
(48, 48, 'en', 'Bed'),
(49, 49, 'en', 'Youth Bedroom'),
(50, 50, 'en', 'Desk'),
(51, 51, 'en', 'Chest'),
(52, 52, 'en', 'Table'),
(54, 54, 'en', 'Chair'),
(55, 55, 'en', 'Chair'),
(56, 56, 'en', 'Fireplace'),
(57, 57, 'en', 'Outdoor Seating Sets'),
(58, 58, 'en', 'Table'),
(60, 60, 'en', 'Sofa'),
(61, 61, 'en', 'Coffee Table'),
(62, 62, 'en', 'Outdoor Dining'),
(63, 63, 'en', 'Dining Set'),
(64, 64, 'en', 'Dining Chair'),
(65, 65, 'en', 'Dining Set'),
(66, 66, 'en', 'Dining Table'),
(67, 67, 'en', 'Bar'),
(68, 68, 'en', 'Regular Ht'),
(69, 69, 'en', 'Bench'),
(70, 70, 'en', 'Table/Stand'),
(71, 71, 'en', 'Lighting & Accessory'),
(72, 72, 'en', 'Shelf/Storage'),
(73, 73, 'en', 'Table'),
(74, 74, 'en', 'Chair'),
(75, 75, 'en', 'Accent Chair'),
(76, 76, 'en', 'Hallway'),
(77, 77, 'en', 'Bench & Ottoman'),
(78, 78, 'en', 'Love Seat'),
(79, 79, 'en', 'Bar'),
(80, 80, 'en', 'Office'),
(81, 81, 'en', 'Office & Desk');

-- --------------------------------------------------------

--
-- Table structure for table `menu_translations`
--

CREATE TABLE `menu_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_translations`
--

INSERT INTO `menu_translations` (`id`, `menu_id`, `locale`, `name`) VALUES
(7, 7, 'en', 'Category Menu'),
(8, 8, 'en', 'Primary Category');

-- --------------------------------------------------------

--
-- Table structure for table `meta_data`
--

CREATE TABLE `meta_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `entity_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_data`
--

INSERT INTO `meta_data` (`id`, `entity_type`, `entity_id`, `created_at`, `updated_at`) VALUES
(1, 'Modules\\Product\\Entities\\Product', 1, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(2, 'Modules\\Product\\Entities\\Product', 2, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(3, 'Modules\\Product\\Entities\\Product', 3, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(4, 'Modules\\Product\\Entities\\Product', 4, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(5, 'Modules\\Product\\Entities\\Product', 5, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(6, 'Modules\\Product\\Entities\\Product', 6, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(7, 'Modules\\Product\\Entities\\Product', 7, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(8, 'Modules\\Product\\Entities\\Product', 8, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(9, 'Modules\\Product\\Entities\\Product', 9, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(10, 'Modules\\Product\\Entities\\Product', 10, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(11, 'Modules\\Product\\Entities\\Product', 11, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(12, 'Modules\\Product\\Entities\\Product', 12, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(13, 'Modules\\Product\\Entities\\Product', 13, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(14, 'Modules\\Product\\Entities\\Product', 14, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(15, 'Modules\\Product\\Entities\\Product', 15, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(16, 'Modules\\Product\\Entities\\Product', 16, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(17, 'Modules\\Product\\Entities\\Product', 17, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(18, 'Modules\\Product\\Entities\\Product', 18, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(19, 'Modules\\Product\\Entities\\Product', 19, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(20, 'Modules\\Product\\Entities\\Product', 20, '2019-09-23 10:58:43', '2019-09-23 10:58:43'),
(21, 'Modules\\Product\\Entities\\Product', 21, '2019-12-27 13:13:16', '2019-12-27 13:13:16'),
(22, 'Modules\\Product\\Entities\\Product', 22, '2019-12-27 13:23:32', '2019-12-27 13:23:32'),
(23, 'Modules\\Product\\Entities\\Product', 23, '2019-12-27 13:36:14', '2019-12-27 13:36:14'),
(24, 'Modules\\Product\\Entities\\Product', 24, '2019-12-27 13:39:55', '2019-12-27 13:39:55'),
(25, 'Modules\\Product\\Entities\\Product', 25, '2019-12-27 14:46:19', '2019-12-27 14:46:19'),
(26, 'Modules\\Product\\Entities\\Product', 26, '2019-12-27 14:49:17', '2019-12-27 14:49:17'),
(27, 'Modules\\Product\\Entities\\Product', 27, '2019-12-27 14:53:13', '2019-12-27 14:53:13'),
(28, 'Modules\\Product\\Entities\\Product', 28, '2019-12-27 15:11:23', '2019-12-27 15:11:23'),
(29, 'Modules\\Page\\Entities\\Page', 1, '2019-12-27 16:07:40', '2019-12-27 16:07:40'),
(30, 'Modules\\Page\\Entities\\Page', 2, '2019-12-27 16:08:13', '2019-12-27 16:08:13'),
(31, 'Modules\\Page\\Entities\\Page', 3, '2019-12-27 17:33:03', '2019-12-27 17:33:03'),
(32, 'Modules\\Product\\Entities\\Product', 29, '2019-12-27 18:16:36', '2019-12-27 18:16:36'),
(33, 'Modules\\Product\\Entities\\Product', 30, '2019-12-27 18:22:41', '2019-12-27 18:22:41'),
(34, 'Modules\\Product\\Entities\\Product', 31, '2019-12-27 18:28:32', '2019-12-27 18:28:32'),
(35, 'Modules\\Product\\Entities\\Product', 32, '2019-12-27 18:34:01', '2019-12-27 18:34:01'),
(36, 'Modules\\Product\\Entities\\Product', 33, '2019-12-27 18:37:42', '2019-12-27 18:37:42'),
(37, 'Modules\\Product\\Entities\\Product', 34, '2019-12-27 18:43:53', '2019-12-27 18:43:53'),
(38, 'Modules\\Product\\Entities\\Product', 35, '2019-12-27 18:46:48', '2019-12-27 18:46:48'),
(39, 'Modules\\Product\\Entities\\Product', 36, '2019-12-27 18:51:20', '2019-12-27 18:51:20'),
(40, 'Modules\\Product\\Entities\\Product', 37, '2019-12-27 18:54:33', '2019-12-27 18:54:33'),
(41, 'Modules\\Page\\Entities\\Page', 4, '2019-12-30 12:24:48', '2019-12-30 12:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `meta_data_translations`
--

CREATE TABLE `meta_data_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `meta_data_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `meta_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_data_translations`
--

INSERT INTO `meta_data_translations` (`id`, `meta_data_id`, `locale`, `meta_title`, `meta_keywords`, `meta_description`) VALUES
(1, 1, 'en', NULL, '[]', NULL),
(2, 2, 'en', NULL, '[]', NULL),
(3, 3, 'en', NULL, '[]', NULL),
(4, 4, 'en', NULL, '[]', NULL),
(5, 5, 'en', NULL, '[]', NULL),
(6, 6, 'en', NULL, '[]', NULL),
(7, 7, 'en', NULL, '[]', NULL),
(8, 8, 'en', NULL, '[]', NULL),
(9, 9, 'en', NULL, '[]', NULL),
(10, 10, 'en', NULL, '[]', NULL),
(11, 11, 'en', NULL, '[]', NULL),
(12, 12, 'en', NULL, '[]', NULL),
(13, 13, 'en', NULL, '[]', NULL),
(14, 14, 'en', NULL, '[]', NULL),
(15, 15, 'en', NULL, '[]', NULL),
(16, 16, 'en', NULL, '[]', NULL),
(17, 17, 'en', NULL, '[]', NULL),
(18, 18, 'en', NULL, '[]', NULL),
(19, 19, 'en', NULL, '[]', NULL),
(20, 20, 'en', NULL, '[]', NULL),
(21, 21, 'en', 'adara-headboard-zuma-gray', '[]', NULL),
(22, 22, 'en', 'ADARA LINEN BED, TALC', '[]', NULL),
(23, 23, 'en', NULL, '[]', NULL),
(24, 24, 'en', NULL, '[]', NULL),
(25, 25, 'en', NULL, '[]', NULL),
(26, 26, 'en', NULL, '[]', NULL),
(27, 27, 'en', NULL, '[]', NULL),
(28, 28, 'en', NULL, '[]', NULL),
(29, 29, 'en', NULL, '[]', NULL),
(30, 30, 'en', NULL, '[]', NULL),
(31, 31, 'en', NULL, '[]', NULL),
(32, 32, 'en', NULL, '[]', NULL),
(33, 33, 'en', NULL, '[]', NULL),
(34, 34, 'en', NULL, '[]', NULL),
(35, 35, 'en', NULL, '[]', NULL),
(36, 36, 'en', NULL, '[]', NULL),
(37, 37, 'en', NULL, '[]', NULL),
(38, 38, 'en', NULL, '[]', NULL),
(39, 39, 'en', NULL, '[]', NULL),
(40, 40, 'en', NULL, '[]', NULL),
(41, 41, 'en', NULL, '[]', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1),
(2, '2014_10_14_200250_create_settings_table', 1),
(3, '2014_10_26_162751_create_files_table', 1),
(4, '2014_10_30_191858_create_pages_table', 1),
(5, '2014_11_31_125848_create_page_translations_table', 1),
(6, '2015_02_27_105241_create_entity_files_table', 1),
(7, '2015_11_20_184604486385_create_translations_table', 1),
(8, '2015_11_20_184604743083_create_translation_translations_table', 1),
(9, '2017_05_29_155126144426_create_products_table', 1),
(10, '2017_05_30_155126416338_create_product_translations_table', 1),
(11, '2017_08_02_153217_create_options_table', 1),
(12, '2017_08_02_153348_create_option_translations_table', 1),
(13, '2017_08_02_153406_create_option_values_table', 1),
(14, '2017_08_02_153736_create_option_value_translations_table', 1),
(15, '2017_08_03_156576_create_product_options_table', 1),
(16, '2017_08_17_170128_create_related_products_table', 1),
(17, '2017_08_17_175236_create_up_sell_products_table', 1),
(18, '2017_08_17_175828_create_cross_sell_products_table', 1),
(19, '2017_11_09_141332910964_create_categories_table', 1),
(20, '2017_11_09_141332931539_create_category_translations_table', 1),
(21, '2017_11_26_083614526622_create_meta_data_table', 1),
(22, '2017_11_26_083614526828_create_meta_data_translations_table', 1),
(23, '2018_01_24_125642_create_product_categories_table', 1),
(24, '2018_02_04_150917488267_create_coupons_table', 1),
(25, '2018_02_04_150917488698_create_coupon_translations_table', 1),
(26, '2018_03_11_181317_create_coupon_products_table', 1),
(27, '2018_03_15_091937_create_coupon_categories_table', 1),
(28, '2018_04_18_154028776225_create_reviews_table', 1),
(29, '2018_05_17_115822452977_create_currency_rates_table', 1),
(30, '2018_07_03_124153537506_create_sliders_table', 1),
(31, '2018_07_03_124153537695_create_slider_translations_table', 1),
(32, '2018_07_03_133107770172_create_slider_slides_table', 1),
(33, '2018_07_03_133107770486_create_slider_slide_translations_table', 1),
(34, '2018_07_28_190524758357_create_attribute_sets_table', 1),
(35, '2018_07_28_190524758497_create_attribute_set_translations_table', 1),
(36, '2018_07_28_190524758646_create_attributes_table', 1),
(37, '2018_07_28_190524758877_create_attribute_translations_table', 1),
(38, '2018_07_28_190524759461_create_product_attributes_table', 1),
(39, '2018_08_01_001919718631_create_tax_classes_table', 1),
(40, '2018_08_01_001919718935_create_tax_class_translations_table', 1),
(41, '2018_08_01_001919723551_create_tax_rates_table', 1),
(42, '2018_08_01_001919723781_create_tax_rate_translations_table', 1),
(43, '2018_08_03_195922206748_create_attribute_values_table', 1),
(44, '2018_08_03_195922207019_create_attribute_value_translations_table', 1),
(45, '2018_08_04_190524764275_create_product_attribute_values_table', 1),
(46, '2018_08_07_135631306565_create_orders_table', 1),
(47, '2018_08_07_135631309451_create_order_products_table', 1),
(48, '2018_08_07_135631309512_create_order_product_options_table', 1),
(49, '2018_08_07_135631309624_create_order_product_option_values_table', 1),
(50, '2018_09_11_213926106353_create_transactions_table', 1),
(51, '2018_09_19_081602135631_create_order_taxes_table', 1),
(52, '2018_09_19_103745_create_setting_translations_table', 1),
(53, '2018_10_01_224852175056_create_wish_lists_table', 1),
(54, '2018_10_04_185608_create_search_terms_table', 1),
(55, '2018_11_03_160015_create_menus_table', 1),
(56, '2018_11_03_160138_create_menu_translations_table', 1),
(57, '2018_11_03_160753_create_menu_items_table', 1),
(58, '2018_11_03_160804_create_menu_item_translation_table', 1),
(59, '2019_02_05_162605_add_position_to_slider_slides_table', 1),
(60, '2019_02_09_164343_remove_file_id_from_slider_slides_table', 1),
(61, '2019_02_09_164434_add_file_id_to_slider_slide_translations_table', 1),
(62, '2019_02_14_103408_create_attribute_categories_table', 1),
(63, '2019_09_12_142200_migration_user_add_mobile_columns', 2),
(64, '2019_09_12_142201_migration_user_set_email_nullable', 2),
(65, '2019_09_12_142202_migration_user_set_name_fields_nullable', 2),
(66, '2019_09_17_001_create_brands_table', 2),
(67, '2019_09_18_001_migration_add_column_brand_id_to_products_table', 2),
(68, '2019_09_18_151026844535_create_featured_categories_table', 2),
(69, '2019_09_18_151026844851_create_featured_category_translations_table', 2),
(70, '2019_09_19_133345634797_create_flash_sales_table', 3),
(71, '2019_09_19_133345634798_create_flash_sale_products_table', 3),
(72, '2019_09_19_133345635184_create_flash_sale_translations_table', 3),
(73, '2019_09_12_142203_migration_user_add_auth_token', 4),
(74, '2019_09_27_113935472453_create_shopping_carts_table', 5),
(75, '2019_09_27_113935472454_create_shopping_cart_items_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL,
  `is_global` tinyint(1) NOT NULL DEFAULT '1',
  `position` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `type`, `is_required`, `is_global`, `position`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'radio', 0, 1, NULL, NULL, '2019-12-30 12:39:14', '2019-12-30 12:39:14'),
(2, 'radio', 0, 0, 0, NULL, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(3, 'radio', 0, 0, 1, NULL, '2019-12-30 12:57:07', '2019-12-30 14:39:54'),
(4, 'radio', 0, 0, 2, NULL, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(5, 'radio', 0, 0, 0, NULL, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(6, 'radio', 0, 0, 1, NULL, '2019-12-30 13:06:55', '2019-12-30 14:40:11'),
(7, 'radio', 0, 0, 2, NULL, '2019-12-30 13:06:55', '2019-12-30 14:40:11'),
(8, 'radio', 0, 0, 0, NULL, '2019-12-30 13:16:02', '2019-12-30 13:22:35'),
(9, 'radio', 0, 0, 1, NULL, '2019-12-30 13:16:02', '2019-12-30 13:22:35'),
(10, 'checkbox', 0, 0, 2, NULL, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(11, 'radio', 0, 0, 0, NULL, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(12, 'radio', 0, 0, 1, NULL, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(13, 'radio', 0, 0, 0, NULL, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(14, 'radio', 0, 0, 1, NULL, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(15, 'radio', 0, 0, 0, NULL, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(16, 'radio', 0, 0, 1, NULL, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(17, 'radio', 0, 0, 2, NULL, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(18, 'radio', 0, 0, 0, NULL, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(19, 'radio', 0, 0, 1, NULL, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(20, 'radio', 0, 0, 2, NULL, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(21, 'radio', 0, 0, 0, NULL, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(22, 'radio', 0, 0, 1, NULL, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(23, 'radio', 0, 0, 2, NULL, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(24, 'radio', 0, 0, 0, NULL, '2019-12-30 13:36:34', '2019-12-30 13:36:34'),
(25, 'radio', 0, 0, 1, NULL, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(26, 'radio', 0, 0, 2, NULL, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(27, 'radio', 0, 0, 0, NULL, '2019-12-30 13:37:34', '2019-12-30 13:37:34'),
(28, 'radio', 0, 0, 1, NULL, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(29, 'radio', 0, 0, 2, NULL, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(30, 'radio', 0, 0, 0, NULL, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(31, 'radio', 0, 0, 1, NULL, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(32, 'radio', 0, 0, 2, NULL, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(33, 'radio', 0, 0, 0, NULL, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(34, 'radio', 0, 0, 1, NULL, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(35, 'radio', 0, 0, 2, NULL, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(36, 'radio', 0, 0, 0, NULL, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(37, 'radio', 0, 0, 1, NULL, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(38, 'radio', 0, 0, 2, NULL, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(39, 'radio', 0, 0, 0, NULL, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(40, 'radio', 0, 0, 1, NULL, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(41, 'radio', 0, 0, 2, NULL, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(42, 'radio', 0, 0, 0, NULL, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(43, 'radio', 0, 0, 1, NULL, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(44, 'radio', 0, 0, 2, NULL, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(45, 'radio', 0, 0, 0, NULL, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(46, 'radio', 0, 0, 1, NULL, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(47, 'radio', 0, 0, 2, NULL, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(48, 'radio', 0, 0, 0, NULL, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(49, 'radio', 0, 0, 1, NULL, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(50, 'radio', 0, 0, 2, NULL, '2019-12-30 14:38:38', '2019-12-30 14:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `option_translations`
--

CREATE TABLE `option_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_translations`
--

INSERT INTO `option_translations` (`id`, `option_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'Color'),
(2, 2, 'en', 'COLOR'),
(3, 3, 'en', 'MATERIALS'),
(4, 4, 'en', 'SIZE'),
(5, 5, 'en', 'COLOR'),
(6, 6, 'en', 'MATERIALS'),
(7, 7, 'en', 'SIZE'),
(8, 8, 'en', 'COLOR'),
(9, 9, 'en', 'MATERIALS'),
(10, 10, 'en', 'SIZE'),
(11, 11, 'en', 'COLOR'),
(12, 12, 'en', 'MATERIALS'),
(13, 13, 'en', 'COLOR'),
(14, 14, 'en', 'MATERIALS'),
(15, 15, 'en', 'COLOR'),
(16, 16, 'en', 'MATERIALS'),
(17, 17, 'en', 'SIZE'),
(18, 18, 'en', 'COLOR'),
(19, 19, 'en', 'MATERIALS'),
(20, 20, 'en', 'SIZE'),
(21, 21, 'en', 'COLOR'),
(22, 22, 'en', 'MATERIALS'),
(23, 23, 'en', 'SIZE'),
(24, 24, 'en', 'COLOR'),
(25, 25, 'en', 'SIZE'),
(26, 26, 'en', 'Material'),
(27, 27, 'en', 'COLOR'),
(28, 28, 'en', 'MATERIALS'),
(29, 29, 'en', 'SIZE'),
(30, 30, 'en', 'COLOR'),
(31, 31, 'en', 'MATERIALS'),
(32, 32, 'en', 'SIZE'),
(33, 33, 'en', 'COLOR'),
(34, 34, 'en', 'MATERIALS'),
(35, 35, 'en', 'SIZE'),
(36, 36, 'en', 'COLOR'),
(37, 37, 'en', 'MATERIALS'),
(38, 38, 'en', 'SIZE'),
(39, 39, 'en', 'COLOR'),
(40, 40, 'en', 'MATERIALS'),
(41, 41, 'en', 'SIZE'),
(42, 42, 'en', 'COLOR'),
(43, 43, 'en', 'MATERIALS'),
(44, 44, 'en', 'SIZE'),
(45, 45, 'en', 'COLOR'),
(46, 46, 'en', 'MATERIALS'),
(47, 47, 'en', 'SIZE'),
(48, 48, 'en', 'COLOR'),
(49, 49, 'en', 'MATERIALS'),
(50, 50, 'en', 'SIZE');

-- --------------------------------------------------------

--
-- Table structure for table `option_values`
--

CREATE TABLE `option_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(18,4) UNSIGNED DEFAULT NULL,
  `price_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_values`
--

INSERT INTO `option_values` (`id`, `option_id`, `price`, `price_type`, `position`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'fixed', 0, '2019-12-30 12:39:14', '2019-12-30 12:39:14'),
(2, 1, NULL, 'fixed', 1, '2019-12-30 12:39:14', '2019-12-30 12:39:14'),
(3, 2, NULL, 'fixed', 0, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(4, 2, NULL, 'fixed', 1, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(5, 2, NULL, 'fixed', 2, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(6, 2, NULL, 'fixed', 3, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(7, 2, NULL, 'fixed', 4, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(8, 3, '1100.0000', 'fixed', 0, '2019-12-30 12:57:07', '2019-12-30 13:01:26'),
(9, 3, '1300.0000', 'fixed', 1, '2019-12-30 12:57:07', '2019-12-30 13:01:26'),
(10, 3, '1200.0000', 'fixed', 2, '2019-12-30 12:57:07', '2019-12-30 13:01:26'),
(11, 4, '1300.0000', 'fixed', 0, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(12, 4, '1400.0000', 'fixed', 1, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(13, 4, '1500.0000', 'fixed', 2, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(14, 4, '1100.0000', 'fixed', 3, '2019-12-30 12:57:07', '2019-12-30 12:57:07'),
(15, 5, NULL, 'fixed', 0, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(16, 5, NULL, 'fixed', 1, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(17, 5, NULL, 'fixed', 2, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(18, 5, '500.0000', 'fixed', 3, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(19, 5, NULL, 'fixed', 4, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(20, 5, '1000.0000', 'fixed', 5, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(21, 6, '100.0000', 'fixed', 0, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(22, 6, '100.0000', 'fixed', 1, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(23, 6, '900.0000', 'fixed', 2, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(24, 7, '500.0000', 'fixed', 0, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(25, 7, '600.0000', 'fixed', 1, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(26, 7, '800.0000', 'fixed', 2, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(27, 7, '1000.0000', 'fixed', 3, '2019-12-30 13:06:55', '2019-12-30 13:06:55'),
(28, 8, NULL, 'fixed', 0, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(29, 8, NULL, 'fixed', 1, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(30, 8, NULL, 'fixed', 2, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(31, 8, '150.0000', 'fixed', 3, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(32, 8, '100.0000', 'fixed', 4, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(33, 9, '500.0000', 'fixed', 0, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(34, 9, '600.0000', 'fixed', 1, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(35, 9, '400.0000', 'fixed', 2, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(36, 10, '1000.0000', 'fixed', 0, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(37, 10, NULL, 'fixed', 1, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(38, 10, '500.0000', 'fixed', 2, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(39, 10, NULL, 'fixed', 3, '2019-12-30 13:16:02', '2019-12-30 13:16:02'),
(40, 11, NULL, 'fixed', 0, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(41, 11, NULL, 'fixed', 1, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(42, 11, NULL, 'fixed', 2, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(43, 11, NULL, 'fixed', 3, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(44, 11, NULL, 'fixed', 4, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(45, 12, '500.0000', 'fixed', 0, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(46, 12, '700.0000', 'fixed', 1, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(47, 12, '400.0000', 'fixed', 2, '2019-12-30 13:19:34', '2019-12-30 13:19:34'),
(48, 13, NULL, 'fixed', 0, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(49, 13, NULL, 'fixed', 1, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(50, 13, NULL, 'fixed', 2, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(51, 13, NULL, 'fixed', 3, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(52, 13, NULL, 'fixed', 4, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(53, 14, NULL, 'fixed', 0, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(54, 14, NULL, 'fixed', 1, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(55, 14, NULL, 'fixed', 2, '2019-12-30 13:21:12', '2019-12-30 13:21:12'),
(56, 15, NULL, 'fixed', 0, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(57, 15, NULL, 'fixed', 1, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(58, 15, NULL, 'fixed', 2, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(59, 15, '500.0000', 'fixed', 3, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(60, 15, NULL, 'fixed', 4, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(61, 16, NULL, 'fixed', 0, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(62, 16, '500.0000', 'fixed', 1, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(63, 16, '400.0000', 'fixed', 2, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(64, 17, '1000.0000', 'fixed', 0, '2019-12-30 13:27:04', '2019-12-30 13:30:40'),
(65, 17, '600.0000', 'fixed', 1, '2019-12-30 13:27:04', '2019-12-30 13:27:04'),
(66, 18, NULL, 'fixed', 0, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(67, 18, NULL, 'fixed', 1, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(68, 18, NULL, 'fixed', 2, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(69, 18, NULL, 'fixed', 3, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(70, 18, NULL, 'fixed', 4, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(71, 19, NULL, 'fixed', 0, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(72, 19, '400.0000', 'fixed', 1, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(73, 19, NULL, 'fixed', 2, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(74, 19, '600.0000', 'fixed', 3, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(75, 20, '800.0000', 'fixed', 0, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(76, 20, '400.0000', 'fixed', 1, '2019-12-30 13:29:34', '2019-12-30 13:29:34'),
(77, 21, NULL, 'fixed', 0, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(78, 21, NULL, 'fixed', 1, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(79, 21, NULL, 'fixed', 2, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(80, 21, NULL, 'fixed', 3, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(81, 21, NULL, 'fixed', 4, '2019-12-30 13:34:39', '2019-12-30 13:34:39'),
(82, 22, NULL, 'fixed', 0, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(83, 22, NULL, 'fixed', 1, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(84, 22, NULL, 'fixed', 2, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(85, 22, '500.0000', 'fixed', 3, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(86, 23, '1000.0000', 'fixed', 0, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(87, 23, '500.0000', 'fixed', 1, '2019-12-30 13:34:40', '2019-12-30 13:34:40'),
(88, 24, NULL, 'fixed', 0, '2019-12-30 13:36:34', '2019-12-30 13:36:34'),
(89, 24, NULL, 'fixed', 1, '2019-12-30 13:36:34', '2019-12-30 13:36:34'),
(90, 24, NULL, 'fixed', 2, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(91, 24, NULL, 'fixed', 3, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(92, 24, NULL, 'fixed', 4, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(93, 25, '400.0000', 'fixed', 0, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(94, 25, '200.0000', 'fixed', 1, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(95, 26, NULL, 'fixed', 0, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(96, 26, '500.0000', 'fixed', 1, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(97, 26, NULL, 'fixed', 2, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(98, 26, '1000.0000', 'fixed', 3, '2019-12-30 13:36:35', '2019-12-30 13:36:35'),
(99, 27, NULL, 'fixed', 0, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(100, 27, NULL, 'fixed', 1, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(101, 27, NULL, 'fixed', 2, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(102, 28, NULL, 'fixed', 0, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(103, 28, '400.0000', 'fixed', 1, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(104, 29, '400.0000', 'fixed', 0, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(105, 29, '300.0000', 'fixed', 1, '2019-12-30 13:37:35', '2019-12-30 13:37:35'),
(106, 30, NULL, 'fixed', 0, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(107, 30, NULL, 'fixed', 1, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(108, 30, NULL, 'fixed', 2, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(109, 30, NULL, 'fixed', 3, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(110, 31, NULL, 'fixed', 0, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(111, 31, NULL, 'fixed', 1, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(112, 31, NULL, 'fixed', 2, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(113, 32, '600.0000', 'fixed', 0, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(114, 32, '400.0000', 'fixed', 1, '2019-12-30 13:40:48', '2019-12-30 13:40:48'),
(115, 33, NULL, 'fixed', 0, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(116, 33, NULL, 'fixed', 1, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(117, 33, NULL, 'fixed', 2, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(118, 33, NULL, 'fixed', 3, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(119, 34, NULL, 'fixed', 0, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(120, 34, NULL, 'fixed', 1, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(121, 35, NULL, 'fixed', 0, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(122, 35, NULL, 'fixed', 1, '2019-12-30 13:42:26', '2019-12-30 13:42:26'),
(123, 36, NULL, 'fixed', 0, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(124, 36, NULL, 'fixed', 1, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(125, 36, NULL, 'fixed', 2, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(126, 36, NULL, 'fixed', 3, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(127, 36, NULL, 'fixed', 4, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(128, 37, NULL, 'fixed', 0, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(129, 37, NULL, 'fixed', 1, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(130, 38, NULL, 'fixed', 0, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(131, 38, NULL, 'fixed', 1, '2019-12-30 13:43:09', '2019-12-30 13:43:09'),
(132, 39, NULL, 'fixed', 0, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(133, 39, NULL, 'fixed', 1, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(134, 39, NULL, 'fixed', 2, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(135, 39, NULL, 'fixed', 3, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(136, 39, NULL, 'fixed', 4, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(137, 40, NULL, 'fixed', 0, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(138, 40, NULL, 'fixed', 1, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(139, 40, NULL, 'fixed', 2, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(140, 40, NULL, 'fixed', 3, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(141, 41, '1000.0000', 'fixed', 0, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(142, 41, '600.0000', 'fixed', 1, '2019-12-30 14:32:44', '2019-12-30 14:32:44'),
(143, 42, NULL, 'fixed', 0, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(144, 42, NULL, 'fixed', 1, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(145, 42, NULL, 'fixed', 2, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(146, 42, NULL, 'fixed', 3, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(147, 42, NULL, 'fixed', 4, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(148, 43, NULL, 'fixed', 0, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(149, 43, NULL, 'fixed', 1, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(150, 43, NULL, 'fixed', 2, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(151, 44, '1000.0000', 'fixed', 0, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(152, 44, '600.0000', 'fixed', 1, '2019-12-30 14:35:09', '2019-12-30 14:35:09'),
(153, 45, NULL, 'fixed', 0, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(154, 45, NULL, 'fixed', 1, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(155, 45, NULL, 'fixed', 2, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(156, 45, NULL, 'fixed', 3, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(157, 45, NULL, 'fixed', 4, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(158, 46, NULL, 'fixed', 0, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(159, 46, NULL, 'fixed', 1, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(160, 46, NULL, 'fixed', 2, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(161, 47, '1000.0000', 'fixed', 0, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(162, 47, '500.0000', 'fixed', 1, '2019-12-30 14:36:25', '2019-12-30 14:36:25'),
(163, 48, NULL, 'fixed', 0, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(164, 48, NULL, 'fixed', 1, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(165, 48, NULL, 'fixed', 2, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(166, 49, '1000.0000', 'fixed', 0, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(167, 49, '1200.0000', 'fixed', 1, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(168, 49, '1100.0000', 'fixed', 2, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(169, 50, '1000.0000', 'fixed', 0, '2019-12-30 14:38:38', '2019-12-30 14:38:38'),
(170, 50, '600.0000', 'fixed', 1, '2019-12-30 14:38:38', '2019-12-30 14:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `option_value_translations`
--

CREATE TABLE `option_value_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_value_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_value_translations`
--

INSERT INTO `option_value_translations` (`id`, `option_value_id`, `locale`, `label`) VALUES
(1, 1, 'en', 'Twin'),
(2, 2, 'en', 'Queen'),
(3, 3, 'en', 'Neutral'),
(4, 4, 'en', 'Gray'),
(5, 5, 'en', 'Green'),
(6, 6, 'en', 'Black'),
(7, 7, 'en', 'Brown'),
(8, 8, 'en', 'Metal'),
(9, 9, 'en', 'Wood'),
(10, 10, 'en', 'Rattan'),
(11, 11, 'en', 'Full'),
(12, 12, 'en', 'Queen'),
(13, 13, 'en', 'King'),
(14, 14, 'en', 'Twin'),
(15, 15, 'en', 'Neutral'),
(16, 16, 'en', 'Gray'),
(17, 17, 'en', 'Green'),
(18, 18, 'en', 'Black'),
(19, 19, 'en', 'Brown'),
(20, 20, 'en', 'Golden'),
(21, 21, 'en', 'Metal'),
(22, 22, 'en', 'Wood'),
(23, 23, 'en', 'Rattan'),
(24, 24, 'en', 'Full'),
(25, 25, 'en', 'Queen'),
(26, 26, 'en', 'King'),
(27, 27, 'en', 'Twin'),
(28, 28, 'en', 'Neutral'),
(29, 29, 'en', 'Gray'),
(30, 30, 'en', 'Green'),
(31, 31, 'en', 'Black'),
(32, 32, 'en', 'Brown'),
(33, 33, 'en', 'Metal'),
(34, 34, 'en', 'Wood'),
(35, 35, 'en', 'Rattan'),
(36, 36, 'en', 'Full'),
(37, 37, 'en', 'Queen'),
(38, 38, 'en', 'King'),
(39, 39, 'en', 'Twin'),
(40, 40, 'en', 'Neutral'),
(41, 41, 'en', 'Gray'),
(42, 42, 'en', 'Green'),
(43, 43, 'en', 'Black'),
(44, 44, 'en', 'Brown'),
(45, 45, 'en', 'Metal'),
(46, 46, 'en', 'Wood'),
(47, 47, 'en', 'Rattan'),
(48, 48, 'en', 'Neutral'),
(49, 49, 'en', 'Gray'),
(50, 50, 'en', 'Green'),
(51, 51, 'en', 'Black'),
(52, 52, 'en', 'Brown'),
(53, 53, 'en', 'Metal'),
(54, 54, 'en', 'Wood'),
(55, 55, 'en', 'Rattan'),
(56, 56, 'en', 'Neutral'),
(57, 57, 'en', 'Gray'),
(58, 58, 'en', 'Green'),
(59, 59, 'en', 'Black'),
(60, 60, 'en', 'Brown'),
(61, 61, 'en', 'Metal'),
(62, 62, 'en', 'Wood'),
(63, 63, 'en', 'Rattan'),
(64, 64, 'en', 'Large'),
(65, 65, 'en', 'Medium'),
(66, 66, 'en', 'Neutral'),
(67, 67, 'en', 'Gray'),
(68, 68, 'en', 'Green'),
(69, 69, 'en', 'Black'),
(70, 70, 'en', 'Brown'),
(71, 71, 'en', 'Metal'),
(72, 72, 'en', 'Wood'),
(73, 73, 'en', 'Rattan'),
(74, 74, 'en', 'Leather'),
(75, 75, 'en', 'Large'),
(76, 76, 'en', 'Medium'),
(77, 77, 'en', 'Neutral'),
(78, 78, 'en', 'Gray'),
(79, 79, 'en', 'Green'),
(80, 80, 'en', 'Black'),
(81, 81, 'en', 'Brown'),
(82, 82, 'en', 'Metal'),
(83, 83, 'en', 'Wood'),
(84, 84, 'en', 'Rattan'),
(85, 85, 'en', 'Leather'),
(86, 86, 'en', 'Large'),
(87, 87, 'en', 'Medium'),
(88, 88, 'en', 'Neutral'),
(89, 89, 'en', 'Gray'),
(90, 90, 'en', 'Green'),
(91, 91, 'en', 'Black'),
(92, 92, 'en', 'Brown'),
(93, 93, 'en', 'Large'),
(94, 94, 'en', 'Medium'),
(95, 95, 'en', 'Metal'),
(96, 96, 'en', 'Wood'),
(97, 97, 'en', 'Rattan'),
(98, 98, 'en', 'Leather'),
(99, 99, 'en', 'Neutral'),
(100, 100, 'en', 'Gray'),
(101, 101, 'en', 'Green'),
(102, 102, 'en', 'Metal'),
(103, 103, 'en', 'Wood'),
(104, 104, 'en', 'Large'),
(105, 105, 'en', 'Medium'),
(106, 106, 'en', 'Neutral'),
(107, 107, 'en', 'Gray'),
(108, 108, 'en', 'Green'),
(109, 109, 'en', 'Black'),
(110, 110, 'en', 'Metal'),
(111, 111, 'en', 'Wood'),
(112, 112, 'en', 'Rattan'),
(113, 113, 'en', 'Large'),
(114, 114, 'en', 'Medium'),
(115, 115, 'en', 'Neutral'),
(116, 116, 'en', 'Gray'),
(117, 117, 'en', 'White'),
(118, 118, 'en', 'Black'),
(119, 119, 'en', 'Metal'),
(120, 120, 'en', 'Wood'),
(121, 121, 'en', 'Large'),
(122, 122, 'en', 'Medium'),
(123, 123, 'en', 'Neutral'),
(124, 124, 'en', 'Gray'),
(125, 125, 'en', 'Green'),
(126, 126, 'en', 'Black'),
(127, 127, 'en', 'Brown'),
(128, 128, 'en', 'Metal'),
(129, 129, 'en', 'Wood'),
(130, 130, 'en', 'Large'),
(131, 131, 'en', 'Medium'),
(132, 132, 'en', 'Neutral'),
(133, 133, 'en', 'Gray'),
(134, 134, 'en', 'Green'),
(135, 135, 'en', 'Black'),
(136, 136, 'en', 'Brown'),
(137, 137, 'en', 'Metal'),
(138, 138, 'en', 'Wood'),
(139, 139, 'en', 'Rattan'),
(140, 140, 'en', 'Leather'),
(141, 141, 'en', 'Large'),
(142, 142, 'en', 'Medium'),
(143, 143, 'en', 'Neutral'),
(144, 144, 'en', 'Gray'),
(145, 145, 'en', 'White'),
(146, 146, 'en', 'Red'),
(147, 147, 'en', 'Olive'),
(148, 148, 'en', 'Metal'),
(149, 149, 'en', 'Wood'),
(150, 150, 'en', 'Rattan'),
(151, 151, 'en', 'Large'),
(152, 152, 'en', 'Medium'),
(153, 153, 'en', 'Neutral'),
(154, 154, 'en', 'Gray'),
(155, 155, 'en', 'Green'),
(156, 156, 'en', 'Black'),
(157, 157, 'en', 'Brown'),
(158, 158, 'en', 'Metal'),
(159, 159, 'en', 'Wood'),
(160, 160, 'en', 'Rattan'),
(161, 161, 'en', 'Large'),
(162, 162, 'en', 'Medium'),
(163, 163, 'en', 'Neutral'),
(164, 164, 'en', 'Black'),
(165, 165, 'en', 'Brown'),
(166, 166, 'en', 'Metal'),
(167, 167, 'en', 'Wood'),
(168, 168, 'en', 'Rattan'),
(169, 169, 'en', 'Large'),
(170, 170, 'en', 'Medium');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address_2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address_1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_address_2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(18,4) UNSIGNED NOT NULL,
  `shipping_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_cost` decimal(18,4) UNSIGNED NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `discount` decimal(18,4) UNSIGNED NOT NULL,
  `total` decimal(18,4) UNSIGNED NOT NULL,
  `payment_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency_rate` decimal(18,4) NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `customer_email`, `customer_phone`, `customer_first_name`, `customer_last_name`, `billing_first_name`, `billing_last_name`, `billing_address_1`, `billing_address_2`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `shipping_first_name`, `shipping_last_name`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_state`, `shipping_zip`, `shipping_country`, `sub_total`, `shipping_method`, `shipping_cost`, `coupon_id`, `discount`, `total`, `payment_method`, `currency`, `currency_rate`, `locale`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'rohit.alobha@gmail.com', '387287879', 'Rohit', 'Kumar', 'Rohit', 'Kumar', 'h 106 ground floor', 'Sector 63', 'Noida', 'UP', '201301', 'IN', 'Rohit', 'Kumar', 'h 106 ground floor', 'Sector 63', 'Noida', 'UP', '201301', 'IN', '1200.0000', 'free_shipping', '0.0000', NULL, '0.0000', '1200.0000', 'cod', 'INR', '1.0000', 'en', 'completed', NULL, '2019-12-30 12:04:51', '2019-12-30 12:07:51');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `unit_price` decimal(18,4) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `line_total` decimal(18,4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `unit_price`, `qty`, `line_total`) VALUES
(1, 1, 29, '1200.0000', 1, '1200.0000');

-- --------------------------------------------------------

--
-- Table structure for table `order_product_options`
--

CREATE TABLE `order_product_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_product_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_product_option_values`
--

CREATE TABLE `order_product_option_values` (
  `order_product_option_id` int(10) UNSIGNED NOT NULL,
  `option_value_id` int(10) UNSIGNED NOT NULL,
  `price` decimal(18,4) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_taxes`
--

CREATE TABLE `order_taxes` (
  `order_id` int(10) UNSIGNED NOT NULL,
  `tax_rate_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(15,4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `slug`, `is_active`, `created_at`, `updated_at`) VALUES
(3, 'about-us', 1, '2019-12-27 17:33:03', '2019-12-27 17:33:03'),
(4, 'privacy-policy', 1, '2019-12-30 12:24:48', '2019-12-30 12:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page_translations`
--

INSERT INTO `page_translations` (`id`, `page_id`, `locale`, `name`, `body`) VALUES
(3, 3, 'en', 'About Us', '<h1><strong>About US</strong></h1>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>'),
(4, 4, 'en', 'Privacy Policy', '<h1 class=\"f4 cl-white mt0 mb16\"><em>Privacy&nbsp; &amp; Policy</em></h1>\r\n<p class=\"f4 cl-white mt0 mb16\"><em>Lorem ipsum</em>, or&nbsp;<em>lipsum</em>&nbsp;as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero\'s&nbsp;<em>De Finibus Bonorum et Malorum</em>&nbsp;for use in a type specimen book. It usually begins with:</p>\r\n<blockquote class=\"page-section__blockquote\">&ldquo;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&rdquo;</blockquote>\r\n<p class=\"f4 cl-white mv16\">The purpose of&nbsp;<em>lorem ipsum</em>&nbsp;is to create a natural looking block of text (sentence, paragraph, page, etc.) that doesn\'t distract from the layout. A practice not without&nbsp;<a title=\"Controversy in the Design World\" href=\"https://loremipsum.io/#controversy\">controversy</a>, laying out pages with meaningless filler text can be very useful when the focus is meant to be on design, not content.</p>\r\n<p class=\"f4 cl-white mv16\">The passage experienced a surge in popularity during the 1960s when Letraset used it on their dry-transfer sheets, and again during the 90s as desktop publishers bundled the text with their software. Today it\'s seen all around the web; on templates, websites, and stock designs. Use our&nbsp;<a title=\"Lorem Ipsum Generator\" href=\"https://loremipsum.io/#generator\">generator</a>&nbsp;to get your own, or read on for the authoritative history of&nbsp;<em>lorem ipsum</em>.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, 'U51gV4L4m52bg0GDSSMZqJkvl0x3kugi', '2019-06-20 02:46:49', '2019-06-20 02:46:49'),
(2, 1, 'zyW7wVX4VVFa5ffUCUo5wwBAKTByzGFg', '2019-09-23 10:42:15', '2019-09-23 10:42:15'),
(3, 1, 'JU41ZA0Riukbtl4Bv8H7Q8rWmoVFgszi', '2019-09-25 16:55:39', '2019-09-25 16:55:39'),
(4, 1, 'MDo5lwea8eQYwgaqvJKa4zJuywWhyNnz', '2019-09-26 10:25:18', '2019-09-26 10:25:18'),
(15, 1, '9SYx0efK8zRUchaHN2wjRCO2pwHXXDGR', '2019-09-26 17:53:53', '2019-09-26 17:53:53'),
(21, 1, 'oaxTfJs4D4egAdmcIxOVHZ2CJOBW610r', '2019-09-27 14:28:14', '2019-09-27 14:28:14'),
(22, 1, 'RlWUbhiiF5xLe5Jqrm4mRfhRuKZ5MoDj', '2019-09-27 14:28:25', '2019-09-27 14:28:25'),
(23, 1, 'Bx2t4oyJ8lG6cvsxpaXGFgHmViLRs9Sn', '2019-09-27 14:28:36', '2019-09-27 14:28:36'),
(24, 1, 'jxHuaQmnF4J7yrdzQZTgOihTYm4trR8z', '2019-09-27 14:28:49', '2019-09-27 14:28:49'),
(25, 1, 'GM0AuYLzOh7uP7gcnTsP7pA8LkozhOij', '2019-09-27 14:29:03', '2019-09-27 14:29:03'),
(56, 1, 'KYi8wKjD2Nb0uJOfn4pOVUlEhGc7ekDc', '2019-09-27 18:05:27', '2019-09-27 18:05:27'),
(81, 1, 'INeMzjvK4jdQr5htVUD0J1MRteh3VQa5', '2019-10-04 16:03:36', '2019-10-04 16:03:36'),
(87, 1, 'hjngHd49SlbUTvbPkxE8lAQwtBYkMCou', '2019-12-27 12:44:32', '2019-12-27 12:44:32'),
(88, 1, 'Flc3CDkaHh79ola2l1RW7MhOeQAvBg2b', '2019-12-28 10:02:33', '2019-12-28 10:02:33'),
(89, 1, 'W0eaQOSp4cvFAHAI8GEEVXJwkMYDvD6a', '2019-12-30 09:53:45', '2019-12-30 09:53:45'),
(90, 1, 'h7CAEdfzTB4bFIamHXPhp5hYjlwjVfqJ', '2019-12-30 09:57:52', '2019-12-30 09:57:52'),
(91, 1, 'EJx7wmkFkyVir0kRvLjhGZOkDS50DwWN', '2019-12-30 15:05:02', '2019-12-30 15:05:02'),
(92, 1, '628mtQ12fcb32qqe0wsGdmXpqWASl5ae', '2019-12-31 10:05:58', '2019-12-31 10:05:58'),
(93, 1, 'mRPBkyhozQvGA1IYVDLoVF40XFroaqRi', '2019-12-31 10:22:06', '2019-12-31 10:22:06'),
(94, 1, 'i4cMT7z0S9tzgR2xDyjFHqWM2eSCb5cU', '2019-12-31 15:21:26', '2019-12-31 15:21:26'),
(96, 1, 'brgvnzv9tPXeSYENvsc8TMyuWcXyTm2N', '2020-01-06 11:17:10', '2020-01-06 11:17:10'),
(97, 1, '6cxjmTLpCHFU6pqAen3WAx56xk2H0NZq', '2020-01-07 17:22:34', '2020-01-07 17:22:34'),
(108, 1, 'uP1s0xzHs1eMh5pNa7x4wxSvtMu0vz0p', '2020-01-09 17:34:37', '2020-01-09 17:34:37'),
(110, 1, 'TlZLefgt6omlIYVfgDQ3Yv5nLoXQe6ul', '2020-01-30 10:45:36', '2020-01-30 10:45:36'),
(111, 1, 'Lb5HQ7Kbp8kW8Cp2jDGKlyiBR0Pwjklv', '2020-01-31 10:22:19', '2020-01-31 10:22:19'),
(112, 1, 'XqIu5sRpIh0XP3R8c625hVLlqT39hYJt', '2020-01-31 11:44:21', '2020-01-31 11:44:21');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_class_id` int(10) UNSIGNED DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(18,4) UNSIGNED NOT NULL,
  `special_price` decimal(18,4) UNSIGNED DEFAULT NULL,
  `special_price_start` date DEFAULT NULL,
  `special_price_end` date DEFAULT NULL,
  `selling_price` decimal(18,4) UNSIGNED DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manage_stock` tinyint(1) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `viewed` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL,
  `new_from` datetime DEFAULT NULL,
  `new_to` datetime DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `brand_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `tax_class_id`, `slug`, `price`, `special_price`, `special_price_start`, `special_price_end`, `selling_price`, `sku`, `manage_stock`, `qty`, `in_stock`, `viewed`, `is_active`, `new_from`, `new_to`, `deleted_at`, `created_at`, `updated_at`, `brand_id`) VALUES
(1, NULL, 'ut-quis-quidem-et-quis-atque-qui', '6072.0000', NULL, NULL, NULL, '6072.0000', 'error', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(2, NULL, 'minima-et-natus-nemo-sit.-porro-et-explicabo-eos', '7402.0000', NULL, NULL, NULL, '7402.0000', 'autem', 0, NULL, 0, 0, 0, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(3, NULL, 'sequi-quae-consequatur-et-ut-aperiam-veniam', '276.0000', NULL, NULL, NULL, '276.0000', 'esse', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(4, NULL, 'explicabo-qui-et-voluptas-a-quod-ab', '7560.0000', NULL, NULL, NULL, '7560.0000', 'et', 0, NULL, 1, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(5, NULL, 'atque-reiciendis-quae-aliquid-voluptatum-nam', '911.0000', NULL, NULL, NULL, '911.0000', 'velit', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(6, NULL, 'sunt-error-et-cum.-perferendis-maxime-non-ipsum-aut-quo-in', '4472.0000', NULL, NULL, NULL, '4472.0000', 'facilis', 0, NULL, 0, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(7, NULL, 'et-vel-adipisci-voluptates-totam-quisquam', '4310.0000', NULL, NULL, NULL, '4310.0000', 'et', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(8, NULL, 'ut-officiis-consequatur-accusamus-fugiat', '2229.0000', NULL, NULL, NULL, '2229.0000', 'aperiam', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(9, NULL, 'omnis-alias-voluptatem-quae-voluptas-distinctio-nemo-saepe', '5176.0000', NULL, NULL, NULL, '5176.0000', 'quidem', 0, NULL, 0, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(10, NULL, 'consequuntur-quos-voluptatem-cupiditate-hic-esse', '3482.0000', NULL, NULL, NULL, '3482.0000', 'ducimus', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(12, NULL, 'ratione-et-eos-necessitatibus-ea-et-omnis', '7761.0000', NULL, NULL, NULL, '7761.0000', 'ab', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(13, NULL, 'sequi-soluta-nulla-enim-ut-id-ea-nesciunt-a', '911.0000', NULL, NULL, NULL, '911.0000', 'quia', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(14, NULL, 'facilis-suscipit-perferendis-nihil-error', '8558.0000', NULL, NULL, NULL, '8558.0000', 'in', 0, NULL, 1, 0, 1, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(15, NULL, 'non-dignissimos-accusamus-voluptatem-in', '7817.0000', NULL, NULL, NULL, '7817.0000', 'placeat', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(16, NULL, 'ut-ut-et-magni-sint-architecto-sed-minima-nostrum', '2279.0000', NULL, NULL, NULL, '2279.0000', 'eos', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:28', '2019-09-23 10:58:43', '2019-12-27 16:45:28', NULL),
(17, NULL, 'cumque-sed-a-modi-laudantium-eaque', '5720.0000', NULL, NULL, NULL, '5720.0000', 'aut', 0, NULL, 1, 0, 0, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(18, NULL, 'dolores-non-et-vitae-eius-voluptatum', '32.0000', NULL, NULL, NULL, '32.0000', 'assumenda', 0, NULL, 0, 0, 0, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(19, NULL, 'iste-totam-facere-et', '5601.0000', NULL, NULL, NULL, '5601.0000', 'non', 0, NULL, 0, 0, 1, NULL, NULL, '2019-12-27 16:45:39', '2019-09-23 10:58:43', '2019-12-27 16:45:39', NULL),
(21, NULL, 'adara-headboard-zuma-gray', '735.0000', '700.0000', '2020-01-01', '2020-01-09', '735.0000', NULL, 0, NULL, 1, 13, 1, NULL, NULL, NULL, '2019-12-27 13:13:16', '2019-12-30 15:46:59', 5),
(22, NULL, 'adara-linen-bed-talc', '567.0000', '500.0000', '2019-12-30', '2020-01-03', '500.0000', NULL, 0, NULL, 1, 40, 1, NULL, NULL, NULL, '2019-12-27 13:23:32', '2020-01-09 10:53:22', 5),
(23, NULL, 'leflore-2-drawer-nightstand', '676.0000', '600.0000', '2019-12-27', '2019-12-31', '600.0000', NULL, 0, NULL, 1, 10, 1, NULL, NULL, NULL, '2019-12-27 13:36:14', '2019-12-30 18:57:49', 4),
(24, NULL, 'darius-end-table-with-storage', '1000.0000', '899.0000', '2019-12-28', '2019-12-31', '899.0000', NULL, 0, NULL, 1, 9, 1, NULL, NULL, NULL, '2019-12-27 13:39:55', '2020-01-30 15:07:10', 4),
(25, NULL, 'decker-papasan-chair', '657.0000', '600.0000', NULL, NULL, '600.0000', NULL, 0, NULL, 1, 12, 1, NULL, NULL, NULL, '2019-12-27 14:46:19', '2019-12-30 12:25:04', 5),
(26, NULL, 'montello-upholstered-bench', '787.0000', '750.0000', NULL, NULL, '750.0000', NULL, 0, NULL, 1, 6, 1, NULL, NULL, NULL, '2019-12-27 14:49:17', '2019-12-30 13:34:17', 5),
(27, NULL, 'denning-coffee-table-with-storage', '1002.0000', '1000.0000', NULL, NULL, '1000.0000', NULL, 0, NULL, 1, 6, 1, NULL, NULL, NULL, '2019-12-27 14:53:13', '2019-12-27 15:11:55', 5),
(28, NULL, 'odum-coffee-table', '1222.0000', '1200.0000', NULL, NULL, '1200.0000', NULL, 0, NULL, 1, 0, 1, NULL, NULL, NULL, '2019-12-27 15:11:23', '2019-12-27 15:11:23', 5),
(29, NULL, 'end-tables', '1200.0000', NULL, NULL, NULL, '1200.0000', NULL, 0, NULL, 1, 1, 1, NULL, NULL, NULL, '2019-12-27 18:16:36', '2019-12-30 09:56:35', 5),
(30, NULL, 'orin-sofa-entryway-hallway-hall-console-table', '1200.0000', NULL, NULL, NULL, '1200.0000', NULL, 0, NULL, 1, 4, 1, NULL, NULL, NULL, '2019-12-27 18:22:41', '2020-01-06 11:11:41', 5),
(31, NULL, 'caiden-tv-stand-for-tvs-up-to-65-inches', '13000.0000', NULL, NULL, NULL, '13000.0000', NULL, 0, NULL, 1, 2, 1, NULL, NULL, NULL, '2019-12-27 18:28:32', '2019-12-30 14:28:22', 6),
(32, NULL, 'jennifer-wood-24-bar-stool', '1300.0000', NULL, NULL, NULL, '1300.0000', NULL, 0, NULL, 1, 4, 1, NULL, NULL, NULL, '2019-12-27 18:34:01', '2019-12-30 18:57:19', 6),
(33, NULL, 'genthner-1-drawer-desk', '1500.0000', NULL, NULL, NULL, '1500.0000', NULL, 0, NULL, 1, 4, 1, NULL, NULL, NULL, '2019-12-27 18:37:42', '2019-12-30 12:06:59', 6),
(34, NULL, 'latimer-upholstered-storage-bench', '1600.0000', NULL, NULL, NULL, '1600.0000', NULL, 0, NULL, 1, 2, 1, NULL, NULL, NULL, '2019-12-27 18:43:53', '2019-12-30 13:30:49', 4),
(35, NULL, 'rebello-sun-lounger-set', '2000.0000', NULL, NULL, NULL, '2000.0000', NULL, 0, NULL, 1, 0, 1, NULL, NULL, NULL, '2019-12-27 18:46:48', '2019-12-27 18:46:48', 4),
(36, NULL, 'vernonburg-intersecting-wall-shelf', '2990.0000', NULL, NULL, NULL, '2990.0000', NULL, 0, NULL, 1, 13, 1, NULL, NULL, NULL, '2019-12-27 18:51:20', '2020-01-30 10:23:51', 4),
(37, NULL, 'newtown-36-single-bathroom-vanity-set', '2000.0000', NULL, NULL, NULL, '2000.0000', NULL, 0, NULL, 1, 14, 1, NULL, NULL, NULL, '2019-12-27 18:54:33', '2020-01-31 10:16:08', 4);

-- --------------------------------------------------------

--
-- Table structure for table `product_attributes`
--

CREATE TABLE `product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attributes`
--

INSERT INTO `product_attributes` (`id`, `product_id`, `attribute_id`) VALUES
(42, 24, 2),
(44, 25, 2),
(45, 23, 2),
(47, 27, 2),
(48, 26, 2),
(49, 28, 2),
(50, 29, 2),
(51, 30, 2),
(52, 31, 2),
(53, 32, 2),
(54, 33, 2),
(55, 34, 2),
(56, 35, 2),
(57, 36, 2),
(58, 37, 2),
(59, 21, 7),
(60, 21, 2),
(61, 22, 2),
(62, 22, 7);

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_values`
--

CREATE TABLE `product_attribute_values` (
  `product_attribute_id` int(10) UNSIGNED NOT NULL,
  `attribute_value_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_attribute_values`
--

INSERT INTO `product_attribute_values` (`product_attribute_id`, `attribute_value_id`) VALUES
(48, 1),
(49, 1),
(50, 1),
(58, 1),
(60, 1),
(45, 2),
(47, 2),
(48, 2),
(50, 2),
(51, 2),
(52, 2),
(54, 2),
(55, 2),
(56, 2),
(58, 2),
(60, 2),
(61, 2),
(42, 3),
(44, 3),
(47, 3),
(52, 3),
(53, 3),
(54, 3),
(57, 3),
(58, 3),
(60, 3),
(61, 3),
(42, 4),
(44, 4),
(45, 4),
(49, 4),
(50, 4),
(54, 4),
(55, 4),
(56, 4),
(58, 4),
(60, 4),
(61, 4),
(44, 5),
(45, 5),
(53, 5),
(54, 5),
(58, 5),
(48, 6),
(49, 6),
(51, 6),
(53, 6),
(55, 6),
(56, 6),
(57, 6),
(58, 6),
(42, 7),
(44, 7),
(47, 7),
(52, 7),
(54, 7),
(57, 7),
(58, 7),
(59, 13),
(62, 13),
(59, 14),
(62, 14);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`product_id`, `category_id`) VALUES
(21, 4),
(22, 4),
(23, 4),
(24, 4),
(21, 5),
(22, 5),
(23, 7),
(24, 7),
(25, 8),
(26, 8),
(25, 9),
(26, 10),
(27, 11),
(28, 11),
(29, 11),
(30, 11),
(31, 11),
(27, 12),
(28, 12),
(29, 14),
(30, 15),
(31, 16),
(32, 17),
(33, 17),
(34, 17),
(35, 17),
(36, 17),
(37, 17),
(32, 18),
(33, 19),
(34, 20),
(35, 21),
(36, 22),
(37, 23);

-- --------------------------------------------------------

--
-- Table structure for table `product_options`
--

CREATE TABLE `product_options` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_options`
--

INSERT INTO `product_options` (`product_id`, `option_id`) VALUES
(21, 2),
(21, 3),
(21, 4),
(22, 5),
(22, 6),
(22, 7),
(23, 8),
(23, 9),
(24, 11),
(24, 12),
(25, 13),
(25, 14),
(26, 15),
(26, 16),
(26, 17),
(27, 18),
(27, 19),
(27, 20),
(28, 21),
(28, 22),
(28, 23),
(29, 24),
(29, 25),
(29, 26),
(30, 27),
(30, 28),
(30, 29),
(31, 30),
(31, 31),
(31, 32),
(32, 33),
(32, 34),
(32, 35),
(33, 36),
(33, 37),
(33, 38),
(34, 39),
(34, 40),
(34, 41),
(35, 42),
(35, 43),
(35, 44),
(36, 45),
(36, 46),
(36, 47),
(37, 48),
(37, 49),
(37, 50);

-- --------------------------------------------------------

--
-- Table structure for table `product_translations`
--

CREATE TABLE `product_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_translations`
--

INSERT INTO `product_translations` (`id`, `product_id`, `locale`, `name`, `description`, `short_description`) VALUES
(1, 1, 'en', 'Ut quis quidem et quis atque qui.', 'Aut culpa laboriosam id quo voluptatem temporibus doloremque. Nesciunt itaque itaque architecto ut odit numquam quasi. Dolores non hic quo est. Et veniam sed aut quo. Aut nihil id repudiandae voluptatum.', NULL),
(2, 2, 'en', 'Minima et natus nemo sit. Porro et explicabo eos.', 'Soluta ea et optio voluptatem delectus. Unde et tempore magni id quam. Consequuntur possimus sunt ipsa voluptatum dolor. Voluptatem dolorem quis et facilis.', NULL),
(3, 3, 'en', 'Sequi quae consequatur et ut aperiam veniam.', 'Eum deleniti fugiat placeat sapiente. Dolorem inventore qui repudiandae hic perferendis. Voluptatem ea qui voluptas quia iste. Velit et ab vel ad rerum eum sed. Accusamus eos libero natus qui.', NULL),
(4, 4, 'en', 'Explicabo qui et voluptas a quod ab.', 'Est nulla iusto error eum. Vel modi eveniet odit dolore dolorem totam accusantium. Mollitia consequatur accusamus id voluptas molestiae vel. Et ut eveniet aut eos et animi. Est voluptates pariatur praesentium id deleniti minus laborum.', NULL),
(5, 5, 'en', 'Atque reiciendis quae aliquid voluptatum nam.', 'Molestiae eos non esse modi. Nulla explicabo est enim ut. Fugiat incidunt est consequuntur et ex maxime deserunt. Natus quibusdam mollitia saepe qui voluptatem et nam.', NULL),
(6, 6, 'en', 'Sunt error et cum. Perferendis maxime non ipsum aut quo in.', 'Et sint et qui est rem. Nemo dolores odio rem voluptas nemo labore atque. Et repellat voluptatem sapiente.', NULL),
(7, 7, 'en', 'Et vel adipisci voluptates totam quisquam.', 'In dolor sit ex et officiis. Quas numquam sunt doloremque sed dolorum praesentium. Aut consequatur in aperiam optio tenetur voluptatem minus. Autem est et quisquam sequi non ad quod quaerat.', NULL),
(8, 8, 'en', 'Ut officiis consequatur accusamus fugiat.', 'Ut quis illum ab voluptas. At eos architecto alias assumenda. Modi harum consectetur sint dolore incidunt velit vel. Excepturi aut atque aut quibusdam et fuga.', NULL),
(9, 9, 'en', 'Omnis alias voluptatem quae voluptas distinctio nemo saepe.', 'Ullam mollitia voluptatibus pariatur. Similique quo consequuntur quia dolor perspiciatis nihil. Porro culpa animi dolorum debitis suscipit et. Officia maiores exercitationem libero ducimus.', NULL),
(10, 10, 'en', 'Consequuntur quos voluptatem cupiditate hic esse.', 'Iusto rerum voluptates illum voluptatem et voluptatem. Et quasi enim eum iusto. Voluptate illum nesciunt molestias nulla quibusdam quam. Hic odio velit est est veniam impedit. Rerum asperiores omnis soluta laudantium quas delectus.', NULL),
(12, 12, 'en', 'Ratione et eos necessitatibus ea et omnis.', 'Tempore eius velit quod amet qui inventore est. Et nisi esse est a modi corrupti ut.', NULL),
(13, 13, 'en', 'Sequi soluta nulla enim ut id ea nesciunt a.', 'Est sed fuga nobis voluptatem ipsa. Doloremque rerum optio amet omnis. Maxime impedit cupiditate odio ipsa voluptas officiis.', NULL),
(14, 14, 'en', 'Facilis suscipit perferendis nihil error.', 'Quo accusantium veritatis ut minus. Doloremque sunt est id. Sed corrupti nihil iste vel excepturi numquam. Enim cum voluptatem numquam maxime beatae ducimus facilis.', NULL),
(15, 15, 'en', 'Non dignissimos accusamus voluptatem in.', 'Qui ratione molestiae recusandae sit a aut velit. Dignissimos recusandae animi consequatur similique error dolorum.', NULL),
(16, 16, 'en', 'Ut ut et magni sint architecto sed minima nostrum.', 'Cupiditate tenetur tempora excepturi sed laborum qui et. Suscipit enim voluptatem voluptas qui sunt. At ut corrupti est autem et et. Nihil voluptatum aliquid sunt qui laudantium modi.', NULL),
(17, 17, 'en', 'Cumque sed a modi laudantium eaque.', 'Quia tempore vel sapiente eaque. Repellendus eius quia consequatur deserunt quia veritatis et dolores. Et vero repellendus ea quo et corporis sit.', NULL),
(18, 18, 'en', 'Dolores non et vitae eius voluptatum.', 'Voluptate et illo dolores in et et. Nostrum nihil veniam suscipit minima consequatur consequatur. Est blanditiis cum odit nisi neque.', NULL),
(19, 19, 'en', 'Iste totam facere et.', 'Necessitatibus facere eaque amet totam et eum dignissimos. Aut ut et nisi culpa atque. Non eum sapiente magnam aspernatur. Et voluptatem necessitatibus debitis perspiciatis fuga et. Sint modi atque esse commodi.', NULL),
(21, 21, 'en', 'ADARA HEADBOARD, ZUMA GRAY', '<p>The easiest way to give your bedroom an instant makeover? Add a new headboard! This fully upholstered option brings soft, contemporary vibes with dramatic height and a subtle winged design</p>\r\n<ul>\r\n<li>Lead Time: 3-4 weeks</li>\r\n<li>Made to order items are not returnable</li>\r\n<li>Twin: 44&rdquo;W x 11&rdquo;D x 55&rdquo;H</li>\r\n<li>Full: 59&rdquo;W x 11&rdquo;D x 55&rdquo;H</li>\r\n<li>Queen: 65&rdquo;W x 11&rdquo;D x 55&rdquo;H</li>\r\n<li>King: 81&rdquo;W x 11&rdquo;D x 55&rdquo;H</li>\r\n<li>California King: 77&rdquo;W x 11&rdquo;D x 55&rdquo;H</li>\r\n<li>Made In: USA</li>\r\n<li>Materials: Solid pine wood, polyurethane, and polyester fill</li>\r\n<li>Fabric Content: 80% Polyester 20% Linen</li>\r\n<li>Assembly Required: Yes</li>\r\n<li>Care: Spot clean</li>\r\n</ul>', NULL),
(22, 22, 'en', 'ADARA LINEN BED, TALC', '<p>We love the versatility of this bed. Whether your style is feminine, modern, minimal, or organic - you can dress this bed up or down to fit your look.</p>\r\n<ul>\r\n<li>Lead Time: 3-4 weeks</li>\r\n<li>Made to order items are not returnable</li>\r\n<li>Twin: 80\"D x 44\"W x 55\"H</li>\r\n<li>Full: 80\"D x 59\"W x 55\"H</li>\r\n<li>Queen: 85\"D x 65\"W x 55\"H</li>\r\n<li>King: 85\"D x 81\"W x 55\"H</li>\r\n<li>California King: 89\"D x 77\"W x 55\"H</li>\r\n<li>Designed to be used with a standard mattress and box spring</li>\r\n<li>Twin Mattress/Boxspring: 75\"W x 39\"D</li>\r\n<li>Full Mattress/Boxspring: 75\"W x 54\"D</li>\r\n<li>Queen Mattress/Boxspring: 80\"W x 60\"D King Mattress/ Boxspring: 80\"W x 76\"D California King Mattress/Boxspring: 84\"W x 72\"D Distance between metal bed frame and bottom of headboard: 20\" Bedframe: 6\"</li>\r\n<li>Bottom of headboard: 26\"</li>\r\n<li>Bottom of upholstered side rail: 4\"</li>\r\n<li>Top of Upholstered side rail: 13.5\"</li>\r\n<li>Made In: USA</li>\r\n<li>Materials: Solid pine wood, polyurethane, and polyester fill</li>\r\n<li>Fabric Content: 95% Polyester 5% Linen</li>\r\n<li>Assembly Required: Yes</li>\r\n<li>Care: Spot clean only</li>\r\n</ul>', NULL),
(23, 23, 'en', 'Leflore 2 Drawer Nightstand', '<p>Simple lines and functional storage are the names of the game with this nightstand. With clean lines and a solid wood frame, this nightstand can blend into most styles without batting an eye. The real strength of this piece, however, is its three drawers, perfect for storing books and evening essentials. In either the master suite or a guest bedroom, a nightstand is a foundation piece that you must be sure you love.</p>\r\n<ul>\r\n<li>Lead Time&nbsp; &nbsp;: Please allow up to 5 weeks for delivery once your order has shipped</li>\r\n<li>Dimensions&nbsp; : 25\'W x 20\'D x 25\'H</li>\r\n<li>Materials&nbsp; &nbsp; &nbsp;: Leather</li>\r\n<li>Care&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Wipe with soft, dry cloth</li>\r\n</ul>', NULL),
(24, 24, 'en', 'Darius End Table with Storage', '<p>Offer clean-lined style and effortless utility to any seating group or entertainment ensemble with this understated end table, the perfect blend of low-key looks and simple versatility. Featuring two tiers of shelving, this end table makes a stylish stage for any display. Set it next to a sofa or loveseat in the living room, and the lower shelf to stack art books and hardcover novels, then top it off with a glowing table lamp to round out the look.</p>\r\n<ul>\r\n<li>19.75\"W x 23.5\"D x 17\"H</li>\r\n<li>Table Thickness: 5.12\"</li>\r\n<li>Materials: Top Grain, Iron</li>\r\n<li>Fabric Content: 100% Top Grain Leather</li>\r\n<li>Care: Avoid Exposure To Heat; Dust With Dry, Soft Cloth; Wipe Spills With A Soft Cloth</li>\r\n</ul>\r\n<p>&nbsp;</p>', NULL),
(25, 25, 'en', 'Decker Papasan Chair', '<p>Rounding out your decor while providing sensible seating arrangements in small spaces, side chairs offer style and versatility to any interior design. Take this one for example: The perfect pick for making a statement in any space be it a bedroom or living room, this piece showcases an open wicker rattan frame awash in a brown finish. Up top, it features a circular cushion upholstered microfiber polyester while tufted accents complete the look.</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li class=\"ProductWeightsDimensions-descriptionListItem\">Overall&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: 35\'\' H x 42\'\' W x 42\'\' D</li>\r\n<li class=\"ProductWeightsDimensions-descriptionListItem\">\r\n<div class=\"ProductWeightsDimensions-descriptionListItem\">Seat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: 18\'\' H x 5\'\' D</div>\r\n</li>\r\n<li class=\"ProductWeightsDimensions-descriptionListItem\">\r\n<div class=\"ProductWeightsDimensions-descriptionListItem\">Overall Product Weight : 29&nbsp;lb.</div>\r\n</li>\r\n</ul>\r\n<p>&nbsp;</p>', NULL),
(26, 26, 'en', 'Montello Upholstered Bench', '<p>Whether you\'re adding the final finishing touch to the master suite or accenting the entryway, this bold upholstered bench is sure to tie your look together. Featuring a foam-filled fabric seat with button tufting, this bench makes a comfy and upscale addition to your aesthetic, while ebony black finished wood legs tie the piece together in subtle style. Brushed nickel nailheads round out the piece in chic style.</p>\r\n<ul class=\"ProductOverviewInformation-list\">\r\n<li>Lead Time: Please allow up to 5 weeks for delivery once your order has shipped</li>\r\n<li>16\"W x 58\"D x 18\"H</li>\r\n<li>Seat Height: 16\"</li>\r\n<li>Seat Depth: 16\"</li>\r\n<li>Materials: Mango Wood</li>\r\n<li>Care: Avoid Ammonia and Silicone; Avoid Direct Sunlight; Avoid Exposure To Heat; Avoid Sharp Objects; Avoid Strong Cleaning Products; Dust With Dry, Soft Cloth; Use Coasters And Placemats; Wipe Spills With A Soft Cloth</li>\r\n</ul>', NULL),
(27, 27, 'en', 'Denning Coffee Table with Storage', '<p>Is the seating ensemble feeling empty? Try a coffee table! Not only do they anchor your space, but they offer room to stage a display and serve up trays of treats when you find yourself entertaining. This one, for example, is simple and stylish. Its wood frame is classic, pairing clean lines with a neutral solid finish for versatility. The shelf below includes two baskets, perfect for storing everything from blankets to DVDs.</p>\r\n<ul class=\"ProductOverviewInformation-list\">\r\n<li class=\"ProductOverviewInformation-list-item\">Traditional style to incorporate into your living room</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Includes two wicker baskets for storage</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Provides ample storage space in a stylish, elegant design</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Ships ready-to-assemble with step-by-step instructions</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Top Materia : Manufactured Wood</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Base Material : Manufactured Wood</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Number of Tables Included : 1</li>\r\n</ul>', NULL),
(28, 28, 'en', 'Odum Coffee Table', '<p>Crafted of manufactured wood with durable laminate veneers in a rustic Barnwood finish, this coffee table strikes a two-tier rectangular silhouette on four chunky square legs, while black metal corner caps and X-frame stretchers offer added character and charm. Its lower shelf is great for fanning out issues of your favorite magazines, while the top surface provides a perfect platform for coasters and remotes. This coffee table has a 100 lbs. weight capacity.</p>\r\n<ul class=\"ProductOverviewInformation-list\">\r\n<li class=\"ProductOverviewInformation-list-item\">Top Material : Manufactured Wood</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Base Material : Manufactured Wood</li>\r\n<li class=\"ProductOverviewInformation-list-item\">Number of Tables Included : 1</li>\r\n<li class=\"ProductOverviewInformation-list-item\">\r\n<div class=\"ProductWeightsDimensions-descriptionListItem\">Overall&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: 18\'\' H x 40\'\' L x 22\'\'&nbsp;&nbsp;</div>\r\n</li>\r\n<li class=\"ProductOverviewInformation-list-item\">\r\n<div class=\"ProductWeightsDimensions-descriptionListItem\">Shelf&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 11.38\'\' H x 40\'\' W x 22\'\'</div>\r\n</li>\r\n<li class=\"ProductOverviewInformation-list-item\">\r\n<div class=\"ProductWeightsDimensions-descriptionListItem\">Overall Product Weight&nbsp; &nbsp; &nbsp; : 48&nbsp;lb.</div>\r\n</li>\r\n</ul>', NULL),
(29, 29, 'en', 'Ginny Pet Crate', '<p>A Broadway melody in metal, this petite caddy is seemingly straight from stylish 1930 Fred Astaire and Ginger Rogers film. The Basil Street-exclusive is just big enough to hold a martini or two, its trendsetting metal construction and smartly retro Art Deco styling is hand finished in a contemporary silver hue with vintage roots. Cheers!</p>\r\n<p>&nbsp;</p>\r\n<ul>\r\n<li class=\"Specifications-descriptionList-cell\">Product Type : Crate</li>\r\n<li class=\"Specifications-descriptionList-cell\">\r\n<div class=\"Specifications-descriptionList-cell\">Material&nbsp; &nbsp; &nbsp; &nbsp; : Wood</div>\r\n</li>\r\n</ul>\r\n<p>&nbsp;</p>', NULL),
(30, 30, 'en', 'Orin Sofa Entryway Hallway Hall Console Table', '<p>Add a stylish touch that will complete your living space with this stylish accent console table. Comes with curved legs as well as modern design with clean and fine lines, this modern unit is perfect for dressing up any wall in your home or office space and ample surface area displaying your favorite decorative items.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Top Material&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Manufactured Wood</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Top Material Details : MDF</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Base Material&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Manufactured Wood</div>\r\n</li>\r\n</ul>', NULL),
(31, 31, 'en', 'Caiden TV Stand for TVs up to 65 inches', '<p>Enjoy watching movies in the comfort of your own home with this TV stand. Making an ideal entertainment center by having open and closed storage in an asymmetrical design. Book-matched styled cabinet doors give a visually interesting element to this entertainment center. With a flattering 2-tone surface made from durable laminate and metal angled legs. This media console will give your living room a mid-century modern flair you&rsquo;ll love.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Design&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Open shelving; Cabinet/Enclosed storage</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Material&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: Manufactured Wood</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell Specifications-descriptionList-cell--related\">Material Details&nbsp; : High-grade MDF and Durable Laminate</div>\r\n</li>\r\n</ul>', NULL),
(32, 32, 'en', 'Jennifer Wood 24\" Bar Stool', '<p>Outfit your at-home bar or kitchen island in understated style with this versatile 24\" bar stool. Founded atop a solid poplar wood frame in a weathered black bean finish this stool is easy to clean with a cloth after accidental spills. Leg glides below to protect your floors from scratches, rounding out the tasteful design with a practical touch.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Seat&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: Style Square</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Number of Stools Included : 2</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Base Color&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Blackbean</div>\r\n</li>\r\n</ul>', NULL),
(33, 33, 'en', 'Genthner 1 Drawer Desk', '<p>Whether you\'re setting up a casual work station or looking to display framed photos and stacked hardcover books, this writing desk is sure to update your space in clean-lined style. Crafted from engineered wood and laminate, this writing desk is a budget-friendly and understated addition to your space. Its simplistic silhouette makes it easy to blend into any aesthetic, from contemporary to transitional. Assembly is required for this 30\'\' H x 39\'\' W x 19.7\'\' D desk.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Desk Type&nbsp; &nbsp;: Writing desk</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Shape&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: Rectangular</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Top Material : Manufactured Wood</div>\r\n</li>\r\n</ul>', NULL),
(34, 34, 'en', 'Latimer Upholstered Storage Bench', '<p>Truly multi-functional MVPs, benches pull up to dining tables, divide space in open floor plans, and act as space-conscious coffee tables &ndash; and that&rsquo;s just the start. Take this one for example: Crafted from a solid wood frame, it features four turned legs and features polyester upholstery with stylish nailhead trim. Plus, this bench has a secret: It loves a clutter-free home! More than just a spare seat or stylish accent, this bench brings a convenient storage solution to the space that needs it most.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Main Material&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : Upholstered</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell Specifications-descriptionList-cell--related\">Main Material Details : 83% Polyester 17% Linen</div>\r\n</li>\r\n</ul>', NULL),
(35, 35, 'en', 'Rebello Sun Lounger Set', '<p>Complete your backyard or patio space with a cozy spot to sit back and unwind in the great outdoors. Featuring delightfully woven wicker over a durable iron frame, our chaise lounge set brings the ultimate summer experience for you and your family to enjoy. Each chaise lounge offers absolute comfort with its curved design that blissfully conforms to your body. Comfort is further accentuated by an adjustable option that is designed to fit your needs, effortlessly bringing the perks of relaxing straight to you.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Product Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: Chaise Lounge Set</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Outer Frame Material : Wicker/Rattan; Metal</div>\r\n</li>\r\n</ul>', NULL),
(36, 36, 'en', 'Vernonburg Intersecting Wall Shelf', '<p>Show off sculptures and snapshots in streamlined style with this wall-mounting intersecting cubes shelf! Crafted of manufactured wood in a smooth, painted finish, this unit consists of four intersecting cube shelves in various sizes ranging from 5\" H x 5\" W x 4\" D to 10.75\" H x 10.75\" W x 4\" D. Between each cube&rsquo;s interior ledge, top surface, and the cubbies made from intersecting squares, there are 14 platforms in all for displaying framed photos, small books, and collected curios.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Product Type : Accent Shelf</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Room Use&nbsp; &nbsp; &nbsp;: Bedroom; Hallway; Living room; Bathroom; Dining room</div>\r\n</li>\r\n</ul>', NULL),
(37, 37, 'en', 'Newtown 36\" Single Bathroom Vanity Set', '<p>A central fixture in any master or guest bathroom, your vanity is the spot for everything from brushing your teeth to stashing your bathroom essentials. To round out a remodel with a piece that lets you do it all, like this 36\'\' single vanity. Crafted from a clean-lined solid wood base, this piece is capped with a white Carrara marble surface and includes a ceramic under-mount sink. Four drawers and a two-door cabinet round this piece out with handy storage space for everything from spare shampoos to cleaning supplies.</p>\r\n<ul>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Top Material : Marble</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Top Finish&nbsp; &nbsp; : Carrara White/Gray</div>\r\n</li>\r\n<li>\r\n<div class=\"Specifications-descriptionList-cell\">Base Material : Solid + Manufactured Wood</div>\r\n</li>\r\n</ul>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

CREATE TABLE `related_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `related_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `related_products`
--

INSERT INTO `related_products` (`product_id`, `related_product_id`) VALUES
(37, 22);

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `reviewer_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `reviewer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `reviewer_id`, `product_id`, `rating`, `reviewer_name`, `comment`, `is_approved`, `created_at`, `updated_at`) VALUES
(1, 1, 33, 5, 'rohit kumar', 'Good', 1, '2019-12-30 12:06:59', '2019-12-30 12:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `permissions`, `created_at`, `updated_at`) VALUES
(1, '{\"admin.users.index\":true,\"admin.users.create\":true,\"admin.users.edit\":true,\"admin.users.destroy\":true,\"admin.roles.index\":true,\"admin.roles.create\":true,\"admin.roles.edit\":true,\"admin.roles.destroy\":true,\"admin.products.index\":true,\"admin.products.create\":true,\"admin.products.edit\":true,\"admin.products.destroy\":true,\"admin.attributes.index\":true,\"admin.attributes.create\":true,\"admin.attributes.edit\":true,\"admin.attributes.destroy\":true,\"admin.attribute_sets.index\":true,\"admin.attribute_sets.create\":true,\"admin.attribute_sets.edit\":true,\"admin.attribute_sets.destroy\":true,\"admin.options.index\":true,\"admin.options.create\":true,\"admin.options.edit\":true,\"admin.options.destroy\":true,\"admin.filters.index\":true,\"admin.filters.create\":true,\"admin.filters.edit\":true,\"admin.filters.destroy\":true,\"admin.reviews.index\":true,\"admin.reviews.create\":true,\"admin.reviews.edit\":true,\"admin.reviews.destroy\":true,\"admin.categories.index\":true,\"admin.categories.create\":true,\"admin.categories.edit\":true,\"admin.categories.destroy\":true,\"admin.orders.index\":true,\"admin.orders.show\":true,\"admin.orders.edit\":true,\"admin.transactions.index\":true,\"admin.coupons.index\":true,\"admin.coupons.create\":true,\"admin.coupons.edit\":true,\"admin.coupons.destroy\":true,\"admin.menus.index\":true,\"admin.menus.create\":true,\"admin.menus.edit\":true,\"admin.menus.destroy\":true,\"admin.menu_items.index\":true,\"admin.menu_items.create\":true,\"admin.menu_items.edit\":true,\"admin.menu_items.destroy\":true,\"admin.media.index\":true,\"admin.media.create\":true,\"admin.media.destroy\":true,\"admin.pages.index\":true,\"admin.pages.create\":true,\"admin.pages.edit\":true,\"admin.pages.destroy\":true,\"admin.currency_rates.index\":true,\"admin.currency_rates.edit\":true,\"admin.taxes.index\":true,\"admin.taxes.create\":true,\"admin.taxes.edit\":true,\"admin.taxes.destroy\":true,\"admin.translations.index\":true,\"admin.translations.edit\":true,\"admin.sliders.index\":true,\"admin.sliders.create\":true,\"admin.sliders.edit\":true,\"admin.sliders.destroy\":true,\"admin.reports.index\":true,\"admin.settings.edit\":true,\"admin.storefront.edit\":true,\"admin.brands.index\":true,\"admin.brands.create\":true,\"admin.brands.edit\":true,\"admin.brands.destroy\":true,\"admin.featured_categories.index\":true,\"admin.featured_categories.create\":true,\"admin.featured_categories.edit\":true,\"admin.featured_categories.destroy\":true,\"admin.flash_sales.index\":true,\"admin.flash_sales.create\":true,\"admin.flash_sales.edit\":true,\"admin.flash_sales.destroy\":true}', '2019-06-20 02:46:31', '2019-09-25 17:00:33'),
(2, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `role_translations`
--

CREATE TABLE `role_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_translations`
--

INSERT INTO `role_translations` (`id`, `role_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'Admin'),
(2, 2, 'en', 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `search_terms`
--

CREATE TABLE `search_terms` (
  `id` int(10) UNSIGNED NOT NULL,
  `term` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `results` int(10) UNSIGNED NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `search_terms`
--

INSERT INTO `search_terms` (`id`, `term`, `results`, `hits`, `created_at`, `updated_at`) VALUES
(1, 'adara linen bed, talc', 0, 18, '2019-12-27 14:53:50', '2019-12-27 14:56:15'),
(2, 'adara headboard, zuma gray', 2, 1, '2019-12-27 14:56:20', '2019-12-27 14:56:20'),
(3, 'latimer upholstered storage bench', 1, 1, '2019-12-30 11:34:17', '2019-12-30 11:34:17'),
(4, 'Flash sale', 0, 1, '2019-12-30 15:19:00', '2019-12-30 15:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_translatable` tinyint(1) NOT NULL DEFAULT '0',
  `plain_value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `is_translatable`, `plain_value`, `created_at`, `updated_at`) VALUES
(1, 'store_name', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(2, 'store_email', 0, 's:25:\"furnishingbase@gmaill.com\";', '2019-06-20 02:46:32', '2019-12-27 17:11:45'),
(3, 'search_engine', 0, 's:5:\"mysql\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(4, 'algolia_app_id', 0, 'N;', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(5, 'algolia_secret', 0, 'N;', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(6, 'active_theme', 0, 's:10:\"Storefront\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(7, 'supported_countries', 0, 'a:1:{i:0;s:2:\"US\";}', '2019-06-20 02:46:32', '2020-01-09 13:16:38'),
(8, 'default_country', 0, 's:2:\"US\";', '2019-06-20 02:46:32', '2020-01-09 13:16:39'),
(9, 'supported_locales', 0, 'a:1:{i:0;s:2:\"en\";}', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(10, 'default_locale', 0, 's:2:\"en\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(11, 'default_timezone', 0, 's:12:\"Asia/Kolkata\";', '2019-06-20 02:46:32', '2019-06-20 02:47:59'),
(12, 'customer_role', 0, 's:1:\"2\";', '2019-06-20 02:46:32', '2019-06-20 02:47:59'),
(13, 'reviews_enabled', 0, 's:1:\"1\";', '2019-06-20 02:46:32', '2019-06-20 02:47:59'),
(14, 'auto_approve_reviews', 0, 's:1:\"1\";', '2019-06-20 02:46:32', '2019-06-20 02:47:59'),
(15, 'supported_currencies', 0, 'a:1:{i:0;s:3:\"USD\";}', '2019-06-20 02:46:32', '2020-01-09 13:17:31'),
(16, 'default_currency', 0, 's:3:\"USD\";', '2019-06-20 02:46:32', '2020-01-09 13:17:31'),
(17, 'send_order_invoice_email', 0, 'b:0;', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(18, 'local_pickup_cost', 0, 's:1:\"0\";', '2019-06-20 02:46:32', '2019-06-20 02:48:00'),
(19, 'flat_rate_cost', 0, 's:1:\"0\";', '2019-06-20 02:46:32', '2019-06-20 02:48:00'),
(20, 'free_shipping_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(21, 'local_pickup_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(22, 'flat_rate_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(23, 'paypal_express_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(24, 'paypal_express_description', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(25, 'stripe_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(26, 'stripe_description', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(27, 'cod_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(28, 'cod_description', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(29, 'bank_transfer_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(30, 'bank_transfer_description', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(31, 'check_payment_label', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(32, 'check_payment_description', 1, NULL, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(33, 'storefront_copyright_text', 1, 's:92:\"Copyright © <a href=\"{{ store_url }}\">{{ store_name }}</a> {{ year }}. All rights reserved.\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(34, 'storefront_feature_1_icon', 0, 's:11:\"fa fa-truck\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(35, 'storefront_feature_1_title', 1, 's:13:\"Free Delivery\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(36, 'storefront_feature_1_subtitle', 1, 's:15:\"Orders over $60\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(37, 'storefront_feature_2_icon', 0, 's:18:\"fa fa-commenting-o\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(38, 'storefront_feature_2_title', 1, 's:12:\"99% Customer\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(39, 'storefront_feature_2_subtitle', 1, 's:9:\"Feedbacks\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(40, 'storefront_feature_3_icon', 0, 's:17:\"fa fa-credit-card\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(41, 'storefront_feature_3_title', 1, 's:7:\"Payment\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(42, 'storefront_feature_3_subtitle', 1, 's:14:\"Secured system\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(43, 'storefront_feature_4_icon', 0, 's:16:\"fa fa-headphones\";', '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(44, 'storefront_feature_4_title', 1, 's:12:\"24/7 Support\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(45, 'storefront_feature_4_subtitle', 1, 's:14:\"Helpline - 121\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(46, 'storefront_recently_viewed_section_enabled', 0, 's:1:\"1\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(47, 'storefront_recently_viewed_section_title', 1, 's:15:\"Recently Viewed\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(48, 'storefront_recently_viewed_section_total_products', 0, 's:1:\"5\";', '2019-06-20 02:46:32', '2019-09-25 16:59:06'),
(49, 'welcome_email', 0, 's:1:\"0\";', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(50, 'admin_order_email', 0, 's:1:\"0\";', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(51, 'order_status_email', 0, 's:1:\"0\";', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(52, 'invoice_email', 0, 's:1:\"0\";', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(53, 'maintenance_mode', 0, 's:1:\"0\";', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(54, 'allowed_ips', 0, 'N;', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(55, 'store_tagline', 1, NULL, '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(56, 'bank_transfer_instructions', 1, NULL, '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(57, 'check_payment_instructions', 1, NULL, '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(58, 'store_phone', 0, 'N;', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(59, 'store_address_1', 0, 'N;', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(60, 'store_address_2', 0, 'N;', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(61, 'store_city', 0, 'N;', '2019-06-20 02:47:59', '2019-06-20 02:47:59'),
(62, 'store_country', 0, 's:2:\"IN\";', '2019-06-20 02:47:59', '2019-06-20 08:18:23'),
(63, 'store_state', 0, 's:2:\"UP\";', '2019-06-20 02:47:59', '2019-06-20 08:18:23'),
(64, 'store_zip', 0, 's:6:\"201301\";', '2019-06-20 02:47:59', '2019-06-20 08:18:23'),
(65, 'currency_rate_exchange_service', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(66, 'fixer_access_key', 0, 's:10:\"Alobha@123\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(67, 'forge_api_key', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(68, 'currency_data_feed_api_key', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(69, 'auto_refresh_currency_rates', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(70, 'auto_refresh_currency_rate_frequency', 0, 's:5:\"daily\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(71, 'mail_from_address', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(72, 'mail_from_name', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(73, 'mail_host', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(74, 'mail_port', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(75, 'mail_username', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(76, 'mail_password', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(77, 'mail_encryption', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(78, 'custom_header_assets', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(79, 'custom_footer_assets', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(80, 'facebook_login_enabled', 0, 's:1:\"1\";', '2019-06-20 02:48:00', '2019-12-27 12:21:31'),
(81, 'facebook_login_app_id', 0, 's:12:\"832768263782\";', '2019-06-20 02:48:00', '2019-12-27 12:21:31'),
(82, 'facebook_login_app_secret', 0, 's:11:\"uwghdjhdgjh\";', '2019-06-20 02:48:00', '2019-12-27 12:21:31'),
(83, 'google_login_enabled', 0, 's:1:\"1\";', '2019-06-20 02:48:00', '2019-12-27 12:21:20'),
(84, 'google_login_client_id', 0, 's:23:\"sjhkjchksjdhksjckjsn jn\";', '2019-06-20 02:48:00', '2019-12-27 12:21:20'),
(85, 'google_login_client_secret', 0, 's:14:\"jhasbbjxhbsjbh\";', '2019-06-20 02:48:00', '2019-12-27 12:21:20'),
(86, 'free_shipping_enabled', 0, 's:1:\"1\";', '2019-06-20 02:48:00', '2019-12-30 12:03:14'),
(87, 'free_shipping_min_amount', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(88, 'local_pickup_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(89, 'flat_rate_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(90, 'paypal_express_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(91, 'paypal_express_test_mode', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(92, 'paypal_express_username', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(93, 'paypal_express_password', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(94, 'paypal_express_signature', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(95, 'stripe_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(96, 'stripe_publishable_key', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(97, 'stripe_secret_key', 0, 'N;', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(98, 'cod_enabled', 0, 's:1:\"1\";', '2019-06-20 02:48:00', '2019-12-30 12:03:33'),
(99, 'bank_transfer_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(100, 'check_payment_enabled', 0, 's:1:\"0\";', '2019-06-20 02:48:00', '2019-06-20 02:48:00'),
(101, 'storefront_theme', 0, 's:9:\"theme-red\";', '2019-09-25 16:59:05', '2020-01-09 10:38:10'),
(102, 'storefront_mail_theme', 0, 's:17:\"theme-marrs-green\";', '2019-09-25 16:59:06', '2019-12-30 09:58:13'),
(103, 'storefront_layout', 0, 's:19:\"slider_with_banners\";', '2019-09-25 16:59:06', '2019-12-27 16:23:56'),
(104, 'storefront_slider', 0, 's:1:\"2\";', '2019-09-25 16:59:06', '2019-12-30 15:47:12'),
(105, 'storefront_terms_page', 0, 's:1:\"3\";', '2019-09-25 16:59:06', '2019-12-28 10:04:40'),
(106, 'storefront_privacy_page', 0, 's:1:\"3\";', '2019-09-25 16:59:06', '2019-12-30 12:27:00'),
(107, 'storefront_footer_address', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(108, 'storefront_category_menu_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(109, 'storefront_footer_menu_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(110, 'storefront_banner_section_1_banner_1_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(111, 'storefront_banner_section_1_banner_1_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(112, 'storefront_banner_section_1_banner_1_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(113, 'storefront_banner_section_1_banner_1_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(114, 'storefront_banner_section_1_banner_2_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(115, 'storefront_banner_section_1_banner_2_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(116, 'storefront_banner_section_1_banner_2_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(117, 'storefront_banner_section_1_banner_2_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(118, 'storefront_banner_section_1_banner_3_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(119, 'storefront_banner_section_1_banner_3_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(120, 'storefront_banner_section_1_banner_3_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(121, 'storefront_banner_section_1_banner_3_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(122, 'storefront_product_carousel_section_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(123, 'storefront_recent_products_section_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(124, 'storefront_banner_section_2_banner_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(125, 'storefront_banner_section_2_banner_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(126, 'storefront_banner_section_2_banner_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(127, 'storefront_banner_section_2_banner_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(128, 'storefront_three_column_vertical_product_carousel_section_column_1_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(129, 'storefront_three_column_vertical_product_carousel_section_column_2_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(130, 'storefront_three_column_vertical_product_carousel_section_column_3_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(131, 'storefront_landscape_products_section_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(132, 'storefront_banner_section_3_banner_1_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(133, 'storefront_banner_section_3_banner_1_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(134, 'storefront_banner_section_3_banner_1_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(135, 'storefront_banner_section_3_banner_1_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(136, 'storefront_banner_section_3_banner_2_file_id', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(137, 'storefront_banner_section_3_banner_2_caption_1', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(138, 'storefront_banner_section_3_banner_2_caption_2', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(139, 'storefront_banner_section_3_banner_2_call_to_action_text', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(140, 'storefront_product_tabs_section_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(141, 'storefront_product_tabs_section_tab_1_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(142, 'storefront_product_tabs_section_tab_2_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(143, 'storefront_product_tabs_section_tab_3_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(144, 'storefront_product_tabs_section_tab_4_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(145, 'storefront_two_column_product_carousel_section_column_1_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(146, 'storefront_two_column_product_carousel_section_column_2_title', 1, NULL, '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(147, 'storefront_primary_menu', 0, 's:1:\"8\";', '2019-09-25 16:59:06', '2019-12-27 17:36:38'),
(148, 'storefront_category_menu', 0, 's:1:\"7\";', '2019-09-25 16:59:06', '2019-12-27 18:29:07'),
(149, 'storefront_footer_menu', 0, 's:1:\"7\";', '2019-09-25 16:59:06', '2019-12-27 17:08:27'),
(150, 'storefront_fb_link', 0, 's:12:\"facebook.com\";', '2019-09-25 16:59:06', '2019-12-30 12:27:00'),
(151, 'storefront_twitter_link', 0, 's:20:\"https://twitter.com/\";', '2019-09-25 16:59:06', '2019-12-30 12:27:00'),
(152, 'storefront_instagram_link', 0, 's:26:\"https://www.instagram.com/\";', '2019-09-25 16:59:06', '2019-12-30 12:27:00'),
(153, 'storefront_linkedin_link', 0, 's:25:\"https://www.linkedin.com/\";', '2019-09-25 16:59:06', '2019-12-30 12:27:00'),
(154, 'storefront_pinterest_link', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(155, 'storefront_google_plus_link', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(156, 'storefront_youtube_link', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(157, 'storefront_banner_section_1_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 12:58:12'),
(158, 'storefront_banner_section_1_banner_1_call_to_action_url', 0, 'N;', '2019-09-25 16:59:06', '2019-12-27 15:45:38'),
(159, 'storefront_banner_section_1_banner_1_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:27:24'),
(160, 'storefront_banner_section_1_banner_2_call_to_action_url', 0, 'N;', '2019-09-25 16:59:06', '2019-12-27 15:45:38'),
(161, 'storefront_banner_section_1_banner_2_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:27:24'),
(162, 'storefront_banner_section_1_banner_3_call_to_action_url', 0, 'N;', '2019-09-25 16:59:06', '2019-12-27 15:45:38'),
(163, 'storefront_banner_section_1_banner_3_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:27:24'),
(164, 'storefront_features_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 13:28:19'),
(165, 'storefront_product_carousel_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 12:57:45'),
(166, 'storefront_recent_products_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 16:42:57'),
(167, 'storefront_recent_products_section_total_products', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(168, 'storefront_banner_section_2_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:36:18'),
(169, 'storefront_banner_section_2_banner_call_to_action_url', 0, 'N;', '2019-09-25 16:59:06', '2019-12-30 11:55:51'),
(170, 'storefront_banner_section_2_banner_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:35:53'),
(171, 'storefront_three_column_vertical_product_carousel_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 14:59:37'),
(172, 'storefront_landscape_products_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:59:31'),
(173, 'storefront_banner_section_3_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:52:48'),
(174, 'storefront_banner_section_3_banner_1_call_to_action_url', 0, 's:1:\"#\";', '2019-09-25 16:59:06', '2019-12-27 17:06:31'),
(175, 'storefront_banner_section_3_banner_1_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:52:48'),
(176, 'storefront_banner_section_3_banner_2_call_to_action_url', 0, 's:1:\"#\";', '2019-09-25 16:59:06', '2019-12-27 17:06:31'),
(177, 'storefront_banner_section_3_banner_2_open_in_new_window', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:52:48'),
(178, 'storefront_product_tabs_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 15:27:24'),
(179, 'storefront_two_column_product_carousel_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 16:27:26'),
(180, 'storefront_brand_section_enabled', 0, 's:1:\"1\";', '2019-09-25 16:59:06', '2019-12-27 16:47:35'),
(181, 'storefront_product_carousel_section_products', 0, 'a:7:{i:0;s:2:\"22\";i:1;s:2:\"23\";i:2;s:2:\"24\";i:3;s:2:\"27\";i:4;s:2:\"28\";i:5;s:2:\"34\";i:6;s:2:\"35\";}', '2019-09-25 16:59:06', '2019-12-30 12:25:00'),
(182, 'storefront_three_column_vertical_product_carousel_section_column_2_products', 0, 'a:4:{i:0;s:2:\"21\";i:1;s:2:\"22\";i:2;s:2:\"23\";i:3;s:2:\"35\";}', '2019-09-25 16:59:06', '2019-12-30 12:17:34'),
(183, 'storefront_landscape_products_section_products', 0, 'a:7:{i:0;s:2:\"22\";i:1;s:2:\"23\";i:2;s:2:\"24\";i:3;s:2:\"26\";i:4;s:2:\"29\";i:5;s:2:\"34\";i:6;s:2:\"35\";}', '2019-09-25 16:59:06', '2019-12-30 12:21:57'),
(184, 'storefront_product_tabs_section_tab_1_products', 0, 'a:5:{i:0;s:2:\"22\";i:1;s:2:\"24\";i:2;s:2:\"25\";i:3;s:2:\"34\";i:4;s:2:\"35\";}', '2019-09-25 16:59:06', '2019-12-30 12:24:44'),
(185, 'storefront_product_tabs_section_tab_2_products', 0, 'a:7:{i:0;s:2:\"22\";i:1;s:2:\"24\";i:2;s:2:\"25\";i:3;s:2:\"27\";i:4;s:2:\"28\";i:5;s:2:\"29\";i:6;s:2:\"33\";}', '2019-09-25 16:59:06', '2019-12-30 12:24:44'),
(186, 'storefront_product_tabs_section_tab_3_products', 0, 'a:6:{i:0;s:2:\"22\";i:1;s:2:\"23\";i:2;s:2:\"26\";i:3;s:2:\"27\";i:4;s:2:\"28\";i:5;s:2:\"33\";}', '2019-09-25 16:59:06', '2019-12-30 12:23:06'),
(187, 'storefront_product_tabs_section_tab_4_products', 0, 'a:5:{i:0;s:2:\"21\";i:1;s:2:\"22\";i:2;s:2:\"28\";i:3;s:2:\"30\";i:4;s:2:\"33\";}', '2019-09-25 16:59:06', '2019-12-30 12:24:44'),
(188, 'storefront_two_column_product_carousel_section_column_1_products', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(189, 'storefront_two_column_product_carousel_section_column_2_products', 0, 'N;', '2019-09-25 16:59:06', '2019-09-25 16:59:06'),
(190, 'storefront_slider_banner_1_file_id', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(191, 'storefront_slider_banner_1_caption_1', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(192, 'storefront_slider_banner_1_caption_2', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(193, 'storefront_slider_banner_1_call_to_action_text', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(194, 'storefront_slider_banner_2_file_id', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(195, 'storefront_slider_banner_2_caption_1', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(196, 'storefront_slider_banner_2_caption_2', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(197, 'storefront_slider_banner_2_call_to_action_text', 1, NULL, '2019-12-27 12:45:29', '2019-12-27 12:45:29'),
(198, 'storefront_slider_banner_1_call_to_action_url', 0, 'N;', '2019-12-27 12:45:30', '2019-12-27 12:45:30'),
(199, 'storefront_slider_banner_1_open_in_new_window', 0, 's:1:\"1\";', '2019-12-27 12:45:30', '2019-12-27 16:40:34'),
(200, 'storefront_slider_banner_2_call_to_action_url', 0, 'N;', '2019-12-27 12:45:30', '2019-12-27 12:45:30'),
(201, 'storefront_slider_banner_2_open_in_new_window', 0, 's:1:\"1\";', '2019-12-27 12:45:30', '2019-12-27 16:40:34'),
(202, 'storefront_header_logo', 1, NULL, '2019-12-27 12:52:53', '2019-12-27 12:52:53'),
(203, 'storefront_favicon', 0, 's:2:\"12\";', '2019-12-27 12:52:53', '2019-12-27 12:52:53'),
(204, 'storefront_three_column_vertical_product_carousel_section_column_1_products', 0, 'a:5:{i:0;s:2:\"22\";i:1;s:2:\"23\";i:2;s:2:\"27\";i:3;s:2:\"34\";i:4;s:2:\"35\";}', '2019-12-27 14:59:37', '2019-12-30 12:17:34'),
(205, 'storefront_brands', 0, 'a:7:{i:0;s:3:\"112\";i:1;s:3:\"110\";i:2;s:3:\"111\";i:3;s:3:\"108\";i:4;s:3:\"107\";i:5;s:3:\"113\";i:6;s:3:\"114\";}', '2019-12-27 16:47:28', '2019-12-30 14:51:16'),
(206, 'storefront_three_column_vertical_product_carousel_section_column_3_products', 0, 'a:4:{i:0;s:2:\"21\";i:1;s:2:\"26\";i:2;s:2:\"34\";i:3;s:2:\"35\";}', '2019-12-30 12:16:14', '2019-12-30 12:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `setting_translations`
--

CREATE TABLE `setting_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_translations`
--

INSERT INTO `setting_translations` (`id`, `setting_id`, `locale`, `value`) VALUES
(1, 1, 'en', 's:15:\"Furnishing Base\";'),
(2, 20, 'en', 's:13:\"Free Shipping\";'),
(3, 21, 'en', 's:12:\"Local Pickup\";'),
(4, 22, 'en', 's:9:\"Flat Rate\";'),
(5, 23, 'en', 's:14:\"PayPal Express\";'),
(6, 24, 'en', 's:28:\"Pay via your PayPal account.\";'),
(7, 25, 'en', 's:6:\"Stripe\";'),
(8, 26, 'en', 's:29:\"Pay via credit or debit card.\";'),
(9, 27, 'en', 's:16:\"Cash On Delivery\";'),
(10, 28, 'en', 's:28:\"Pay with cash upon delivery.\";'),
(11, 29, 'en', 's:13:\"Bank Transfer\";'),
(12, 30, 'en', 's:100:\"Make your payment directly into our bank account. Please use your Order ID as the payment reference.\";'),
(13, 31, 'en', 's:19:\"Check / Money Order\";'),
(14, 32, 'en', 's:33:\"Please send a check to our store.\";'),
(15, 55, 'en', 'N;'),
(16, 56, 'en', 'N;'),
(17, 57, 'en', 'N;'),
(18, 107, 'en', 's:48:\"H-106, Ground Floor, Noida, Uttar Pradesh 201301\";'),
(19, 33, 'en', 's:92:\"Copyright © <a href=\"{{ store_url }}\">{{ store_name }}</a> {{ year }}. All rights reserved.\";'),
(20, 108, 'en', 's:14:\"All Categories\";'),
(21, 109, 'en', 's:16:\"Popular Category\";'),
(22, 110, 'en', 's:2:\"64\";'),
(23, 111, 'en', 'N;'),
(24, 112, 'en', 'N;'),
(25, 113, 'en', 'N;'),
(26, 114, 'en', 's:2:\"63\";'),
(27, 115, 'en', 'N;'),
(28, 116, 'en', 'N;'),
(29, 117, 'en', 'N;'),
(30, 118, 'en', 's:2:\"51\";'),
(31, 119, 'en', 'N;'),
(32, 120, 'en', 'N;'),
(33, 121, 'en', 'N;'),
(34, 35, 'en', 's:13:\"Free Delivery\";'),
(35, 36, 'en', 's:15:\"Orders over $60\";'),
(36, 38, 'en', 's:12:\"99% Customer\";'),
(37, 39, 'en', 's:9:\"Feedbacks\";'),
(38, 41, 'en', 's:7:\"Payment\";'),
(39, 42, 'en', 's:14:\"Secured system\";'),
(40, 44, 'en', 's:12:\"24/7 Support\";'),
(41, 45, 'en', 's:14:\"Helpline - 121\";'),
(42, 122, 'en', 's:19:\"Execlusive Products\";'),
(43, 123, 'en', 's:15:\"Recent Products\";'),
(44, 124, 'en', 's:2:\"63\";'),
(45, 125, 'en', 'N;'),
(46, 126, 'en', 'N;'),
(47, 127, 'en', 'N;'),
(48, 128, 'en', 's:7:\"On Sale\";'),
(49, 129, 'en', 's:6:\"ACCENT\";'),
(50, 130, 'en', 's:4:\"Best\";'),
(51, 131, 'en', 's:16:\"Popular Products\";'),
(52, 132, 'en', 's:2:\"61\";'),
(53, 133, 'en', 'N;'),
(54, 134, 'en', 'N;'),
(55, 135, 'en', 'N;'),
(56, 136, 'en', 's:2:\"62\";'),
(57, 137, 'en', 'N;'),
(58, 138, 'en', 'N;'),
(59, 139, 'en', 'N;'),
(60, 140, 'en', 's:11:\"Product Tab\";'),
(61, 141, 'en', 's:5:\"Fresh\";'),
(62, 142, 'en', 's:8:\"Must Buy\";'),
(63, 143, 'en', 's:10:\"Latest Buy\";'),
(64, 144, 'en', 's:6:\"My Fav\";'),
(65, 145, 'en', 's:16:\"Living Room Sets\";'),
(66, 146, 'en', 'N;'),
(67, 47, 'en', 's:15:\"Recently Viewed\";'),
(68, 190, 'en', 's:2:\"68\";'),
(69, 191, 'en', 'N;'),
(70, 192, 'en', 'N;'),
(71, 193, 'en', 'N;'),
(72, 194, 'en', 's:2:\"70\";'),
(73, 195, 'en', 'N;'),
(74, 196, 'en', 'N;'),
(75, 197, 'en', 'N;'),
(76, 202, 'en', 's:3:\"122\";');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_carts`
--

CREATE TABLE `shopping_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shopping_carts`
--

INSERT INTO `shopping_carts` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-09-27 14:28:25', '2019-09-27 14:28:25');

-- --------------------------------------------------------

--
-- Table structure for table `shopping_cart_items`
--

CREATE TABLE `shopping_cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `shopping_cart_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `autoplay` tinyint(1) DEFAULT NULL,
  `autoplay_speed` int(11) DEFAULT NULL,
  `arrows` tinyint(1) DEFAULT NULL,
  `dots` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `autoplay`, `autoplay_speed`, `arrows`, `dots`, `created_at`, `updated_at`) VALUES
(2, 1, 3000, 1, NULL, '2019-12-27 15:18:52', '2019-12-27 16:49:37'),
(3, 1, 3000, 1, NULL, '2019-12-27 16:04:34', '2019-12-27 16:04:34'),
(4, 1, 3000, 1, NULL, '2019-12-30 15:08:53', '2019-12-30 15:08:53');

-- --------------------------------------------------------

--
-- Table structure for table `slider_slides`
--

CREATE TABLE `slider_slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_id` int(10) UNSIGNED NOT NULL,
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `call_to_action_url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `open_in_new_window` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_slides`
--

INSERT INTO `slider_slides` (`id`, `slider_id`, `options`, `call_to_action_url`, `open_in_new_window`, `position`, `created_at`, `updated_at`) VALUES
(5, 2, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bedroom&page=1', 0, 0, '2019-12-27 15:18:52', '2019-12-30 11:59:26'),
(6, 2, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bedroom&page=1', 0, 1, '2019-12-27 15:21:33', '2019-12-30 11:59:26'),
(7, 3, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 1, 0, '2019-12-27 16:04:34', '2019-12-27 16:04:34'),
(8, 2, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', 'http://35.154.49.203/laraecom/public/products?sort=latest&category=bedroom&page=1', 0, 2, '2019-12-27 16:22:52', '2019-12-30 11:59:26'),
(10, 3, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 0, 1, '2019-12-30 15:21:28', '2019-12-30 15:21:28'),
(11, 3, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 0, 2, '2019-12-30 15:21:28', '2019-12-30 15:21:28'),
(12, 3, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 0, 3, '2019-12-30 15:21:29', '2019-12-30 15:21:29'),
(13, 3, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 0, 4, '2019-12-30 15:21:29', '2019-12-30 15:21:29'),
(14, 4, '{\"caption_1\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_2\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"caption_3\":{\"delay\":null,\"effect\":\"fadeInUp\"},\"call_to_action\":{\"delay\":null,\"effect\":\"fadeInUp\"}}', NULL, 0, 0, '2019-12-30 15:46:48', '2019-12-30 15:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `slider_slide_translations`
--

CREATE TABLE `slider_slide_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_slide_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `caption_1` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption_2` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption_3` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `call_to_action_text` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_slide_translations`
--

INSERT INTO `slider_slide_translations` (`id`, `slider_slide_id`, `locale`, `file_id`, `caption_1`, `caption_2`, `caption_3`, `call_to_action_text`) VALUES
(5, 5, 'en', 54, NULL, NULL, NULL, 'SHOP'),
(6, 6, 'en', 52, NULL, NULL, NULL, 'SHOP'),
(7, 7, 'en', 58, NULL, NULL, NULL, NULL),
(8, 8, 'en', 51, NULL, NULL, NULL, 'SHOP'),
(10, 10, 'en', 117, NULL, NULL, NULL, NULL),
(11, 11, 'en', 117, NULL, NULL, NULL, NULL),
(12, 12, 'en', 119, NULL, NULL, NULL, NULL),
(13, 13, 'en', 120, NULL, NULL, NULL, NULL),
(14, 14, 'en', 117, 'Deal Of The Day', NULL, NULL, 'SHOP');

-- --------------------------------------------------------

--
-- Table structure for table `slider_translations`
--

CREATE TABLE `slider_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_translations`
--

INSERT INTO `slider_translations` (`id`, `slider_id`, `locale`, `name`) VALUES
(2, 2, 'en', 'Coffee Table'),
(3, 3, 'en', 'Furniture Furnish Your Life.'),
(4, 4, 'en', 'Deal of The Day');

-- --------------------------------------------------------

--
-- Table structure for table `tax_classes`
--

CREATE TABLE `tax_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `based_on` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_class_translations`
--

CREATE TABLE `tax_class_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_class_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_rates`
--

CREATE TABLE `tax_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_class_id` int(10) UNSIGNED NOT NULL,
  `country` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` decimal(8,4) UNSIGNED NOT NULL,
  `position` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate_translations`
--

CREATE TABLE `tax_rate_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_rate_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translation_translations`
--

CREATE TABLE `translation_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `translation_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `up_sell_products`
--

CREATE TABLE `up_sell_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `up_sell_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number_otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number_otp_valid_till` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `api_token`, `permissions`, `last_login`, `created_at`, `updated_at`, `mobile_number`, `mobile_number_otp`, `mobile_number_otp_valid_till`) VALUES
(1, 'rohit', 'kumar', 'rohit.alobha@gmail.com', '$2y$10$9pPJEnz8IQYaYrVDbwgGXu.qF.RixQrL6gwFUDujZ0KFXNRqe9o3q', '70Xiq1XOITvIbwGVDa2Nzz6iagSAC64CqKsj5ZXJ4URPx7ROPmJDl6YyIyK1', NULL, '2020-01-31 11:44:21', '2019-06-20 02:46:32', '2020-01-31 11:44:21', NULL, NULL, NULL),
(7, 'Furnishing', 'Base', 'admin@furnishingbase.com', '$2y$10$hFAL5VTRh4tTDB2AYYV2buB9Wg0rna3s8FGus6JMlA/6xxdZHcwd6', NULL, '{\"admin.attributes.index\":true,\"admin.attributes.create\":true,\"admin.attributes.edit\":true,\"admin.attributes.destroy\":true,\"admin.attribute_sets.index\":true,\"admin.attribute_sets.create\":true,\"admin.attribute_sets.edit\":true,\"admin.attribute_sets.destroy\":true,\"admin.brands.index\":true,\"admin.brands.create\":true,\"admin.brands.edit\":true,\"admin.brands.destroy\":true,\"admin.categories.index\":true,\"admin.categories.create\":true,\"admin.categories.edit\":true,\"admin.categories.destroy\":true,\"admin.coupons.index\":true,\"admin.coupons.create\":true,\"admin.coupons.edit\":true,\"admin.coupons.destroy\":true,\"admin.currency_rates.index\":true,\"admin.currency_rates.edit\":true,\"admin.featured_categories.index\":true,\"admin.featured_categories.create\":true,\"admin.featured_categories.edit\":true,\"admin.featured_categories.destroy\":true,\"admin.flash_sales.index\":true,\"admin.flash_sales.create\":true,\"admin.flash_sales.edit\":true,\"admin.flash_sales.destroy\":true,\"admin.media.index\":true,\"admin.media.create\":true,\"admin.media.edit\":true,\"admin.media.destroy\":true,\"admin.menus.index\":true,\"admin.menus.create\":true,\"admin.menus.edit\":true,\"admin.menus.destroy\":true,\"admin.menu_items.index\":true,\"admin.menu_items.create\":true,\"admin.menu_items.edit\":true,\"admin.menu_items.destroy\":true,\"admin.options.index\":true,\"admin.options.create\":true,\"admin.options.edit\":true,\"admin.options.destroy\":true,\"admin.orders.index\":true,\"admin.orders.show\":true,\"admin.orders.edit\":true,\"admin.pages.index\":true,\"admin.pages.create\":true,\"admin.pages.edit\":true,\"admin.pages.destroy\":true,\"admin.products.index\":true,\"admin.products.create\":true,\"admin.products.edit\":true,\"admin.products.destroy\":true,\"admin.reports.index\":true,\"admin.reviews.index\":true,\"admin.reviews.edit\":true,\"admin.reviews.destroy\":true,\"admin.settings.edit\":true,\"admin.shopping_carts.index\":true,\"admin.shopping_carts.create\":true,\"admin.shopping_carts.edit\":true,\"admin.shopping_carts.destroy\":true,\"admin.sliders.index\":true,\"admin.sliders.create\":true,\"admin.sliders.edit\":true,\"admin.sliders.destroy\":true,\"admin.taxes.index\":true,\"admin.taxes.create\":true,\"admin.taxes.edit\":true,\"admin.taxes.destroy\":true,\"admin.transactions.index\":true,\"admin.translations.index\":true,\"admin.translations.edit\":true,\"admin.users.index\":true,\"admin.users.create\":true,\"admin.users.edit\":true,\"admin.users.destroy\":true,\"admin.roles.index\":true,\"admin.roles.create\":true,\"admin.roles.edit\":true,\"admin.roles.destroy\":true,\"admin.storefront.edit\":true}', NULL, '2019-12-30 12:10:43', '2020-01-09 13:14:17', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-06-20 02:46:32', '2019-06-20 02:46:32'),
(7, 1, '2019-12-30 12:10:43', '2019-12-30 12:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `wish_lists`
--

CREATE TABLE `wish_lists` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wish_lists`
--

INSERT INTO `wish_lists` (`user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 24, '2020-01-30 13:00:11', '2020-01-30 13:00:11'),
(1, 26, '2019-12-30 13:27:42', '2019-12-30 13:27:42'),
(1, 33, '2019-12-30 12:05:30', '2019-12-30 12:05:30'),
(1, 35, '2019-12-30 12:05:26', '2019-12-30 12:05:26'),
(1, 37, '2020-01-06 13:11:19', '2020-01-06 13:11:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activations_user_id_index` (`user_id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attributes_attribute_set_id_index` (`attribute_set_id`);

--
-- Indexes for table `attribute_categories`
--
ALTER TABLE `attribute_categories`
  ADD PRIMARY KEY (`attribute_id`,`category_id`),
  ADD KEY `attribute_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `attribute_sets`
--
ALTER TABLE `attribute_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute_set_translations`
--
ALTER TABLE `attribute_set_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_set_translations_attribute_set_id_locale_unique` (`attribute_set_id`,`locale`);

--
-- Indexes for table `attribute_translations`
--
ALTER TABLE `attribute_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_translations_attribute_id_locale_unique` (`attribute_id`,`locale`);

--
-- Indexes for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_values_attribute_id_index` (`attribute_id`);

--
-- Indexes for table `attribute_value_translations`
--
ALTER TABLE `attribute_value_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_value_translations_attribute_value_id_locale_unique` (`attribute_value_id`,`locale`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_slug_unique` (`slug`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `coupons_code_index` (`code`);

--
-- Indexes for table `coupon_categories`
--
ALTER TABLE `coupon_categories`
  ADD PRIMARY KEY (`coupon_id`,`category_id`,`exclude`),
  ADD KEY `coupon_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `coupon_products`
--
ALTER TABLE `coupon_products`
  ADD PRIMARY KEY (`coupon_id`,`product_id`),
  ADD KEY `coupon_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `coupon_translations`
--
ALTER TABLE `coupon_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupon_translations_coupon_id_locale_unique` (`coupon_id`,`locale`);

--
-- Indexes for table `cross_sell_products`
--
ALTER TABLE `cross_sell_products`
  ADD PRIMARY KEY (`product_id`,`cross_sell_product_id`),
  ADD KEY `cross_sell_products_cross_sell_product_id_foreign` (`cross_sell_product_id`);

--
-- Indexes for table `currency_rates`
--
ALTER TABLE `currency_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currency_rates_currency_unique` (`currency`);

--
-- Indexes for table `entity_files`
--
ALTER TABLE `entity_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entity_files_entity_type_entity_id_index` (`entity_type`,`entity_id`),
  ADD KEY `entity_files_file_id_index` (`file_id`),
  ADD KEY `entity_files_zone_index` (`zone`);

--
-- Indexes for table `featured_categories`
--
ALTER TABLE `featured_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `featured_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `featured_category_translations`
--
ALTER TABLE `featured_category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `featured_category_translations_locale_unique` (`featured_category_id`,`locale`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `files_user_id_index` (`user_id`),
  ADD KEY `files_filename_index` (`filename`);

--
-- Indexes for table `flash_sales`
--
ALTER TABLE `flash_sales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `flash_sales_slug_unique` (`slug`);

--
-- Indexes for table `flash_sale_products`
--
ALTER TABLE `flash_sale_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flash_sale_products_flash_sale_id_foreign` (`flash_sale_id`),
  ADD KEY `flash_sale_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `flash_sale_translations`
--
ALTER TABLE `flash_sale_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `flash_sale_translations_flash_sale_id_locale_unique` (`flash_sale_id`,`locale`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_parent_id_foreign` (`parent_id`),
  ADD KEY `menu_items_category_id_foreign` (`category_id`),
  ADD KEY `menu_items_page_id_foreign` (`page_id`),
  ADD KEY `menu_items_menu_id_index` (`menu_id`);

--
-- Indexes for table `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_item_translations_menu_item_id_locale_unique` (`menu_item_id`,`locale`);

--
-- Indexes for table `menu_translations`
--
ALTER TABLE `menu_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menu_translations_menu_id_locale_unique` (`menu_id`,`locale`);

--
-- Indexes for table `meta_data`
--
ALTER TABLE `meta_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_data_entity_type_entity_id_index` (`entity_type`,`entity_id`);

--
-- Indexes for table `meta_data_translations`
--
ALTER TABLE `meta_data_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `meta_data_translations_meta_data_id_locale_unique` (`meta_data_id`,`locale`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `option_translations`
--
ALTER TABLE `option_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `option_translations_option_id_locale_unique` (`option_id`,`locale`);

--
-- Indexes for table `option_values`
--
ALTER TABLE `option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_values_option_id_index` (`option_id`);

--
-- Indexes for table `option_value_translations`
--
ALTER TABLE `option_value_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `option_value_translations_option_value_id_locale_unique` (`option_value_id`,`locale`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_customer_id_index` (`customer_id`),
  ADD KEY `orders_coupon_id_index` (`coupon_id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_products_order_id_foreign` (`order_id`),
  ADD KEY `order_products_product_id_foreign` (`product_id`);

--
-- Indexes for table `order_product_options`
--
ALTER TABLE `order_product_options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `order_product_options_order_product_id_option_id_unique` (`order_product_id`,`option_id`),
  ADD KEY `order_product_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `order_product_option_values`
--
ALTER TABLE `order_product_option_values`
  ADD PRIMARY KEY (`order_product_option_id`,`option_value_id`),
  ADD KEY `order_product_option_values_option_value_id_foreign` (`option_value_id`);

--
-- Indexes for table `order_taxes`
--
ALTER TABLE `order_taxes`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `order_taxes_tax_rate_id_foreign` (`tax_rate_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `page_translations_page_id_locale_unique` (`page_id`,`locale`);

--
-- Indexes for table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`),
  ADD KEY `persistences_user_id_foreign` (`user_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_attributes_product_id_index` (`product_id`),
  ADD KEY `product_attributes_attribute_id_index` (`attribute_id`);

--
-- Indexes for table `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD PRIMARY KEY (`product_attribute_id`,`attribute_value_id`),
  ADD KEY `product_attribute_values_attribute_value_id_foreign` (`attribute_value_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `product_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`product_id`,`option_id`),
  ADD KEY `product_options_option_id_foreign` (`option_id`);

--
-- Indexes for table `product_translations`
--
ALTER TABLE `product_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_translations_product_id_locale_unique` (`product_id`,`locale`);
ALTER TABLE `product_translations` ADD FULLTEXT KEY `name` (`name`);

--
-- Indexes for table `related_products`
--
ALTER TABLE `related_products`
  ADD PRIMARY KEY (`product_id`,`related_product_id`),
  ADD KEY `related_products_related_product_id_foreign` (`related_product_id`);

--
-- Indexes for table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reminders_user_id_foreign` (`user_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_reviewer_id_index` (`reviewer_id`),
  ADD KEY `reviews_product_id_index` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_translations`
--
ALTER TABLE `role_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_translations_role_id_locale_unique` (`role_id`,`locale`);

--
-- Indexes for table `search_terms`
--
ALTER TABLE `search_terms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `search_terms_term_unique` (`term`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `setting_translations`
--
ALTER TABLE `setting_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `setting_translations_setting_id_locale_unique` (`setting_id`,`locale`);

--
-- Indexes for table `shopping_carts`
--
ALTER TABLE `shopping_carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shopping_carts_user_id_foreign` (`user_id`);

--
-- Indexes for table `shopping_cart_items`
--
ALTER TABLE `shopping_cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shopping_cart_items_shopping_cart_id_foreign` (`shopping_cart_id`),
  ADD KEY `shopping_cart_items_product_id_foreign` (`product_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_slides`
--
ALTER TABLE `slider_slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slider_slides_slider_id_foreign` (`slider_id`);

--
-- Indexes for table `slider_slide_translations`
--
ALTER TABLE `slider_slide_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slider_slide_translations_slider_slide_id_locale_unique` (`slider_slide_id`,`locale`);

--
-- Indexes for table `slider_translations`
--
ALTER TABLE `slider_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slider_translations_slider_id_locale_unique` (`slider_id`,`locale`);

--
-- Indexes for table `tax_classes`
--
ALTER TABLE `tax_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_class_translations`
--
ALTER TABLE `tax_class_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_class_translations_tax_class_id_locale_unique` (`tax_class_id`,`locale`);

--
-- Indexes for table `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tax_rates_tax_class_id_index` (`tax_class_id`);

--
-- Indexes for table `tax_rate_translations`
--
ALTER TABLE `tax_rate_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_rate_translations_tax_rate_id_locale_unique` (`tax_rate_id`,`locale`);

--
-- Indexes for table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_foreign` (`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `transactions_order_id_unique` (`order_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `translations_key_index` (`key`);

--
-- Indexes for table `translation_translations`
--
ALTER TABLE `translation_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translation_translations_translation_id_locale_unique` (`translation_id`,`locale`);

--
-- Indexes for table `up_sell_products`
--
ALTER TABLE `up_sell_products`
  ADD PRIMARY KEY (`product_id`,`up_sell_product_id`),
  ADD KEY `up_sell_products_up_sell_product_id_foreign` (`up_sell_product_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_number_unique` (`mobile_number`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_role_id_foreign` (`role_id`);

--
-- Indexes for table `wish_lists`
--
ALTER TABLE `wish_lists`
  ADD PRIMARY KEY (`user_id`,`product_id`),
  ADD KEY `wish_lists_product_id_foreign` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attribute_sets`
--
ALTER TABLE `attribute_sets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attribute_set_translations`
--
ALTER TABLE `attribute_set_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attribute_translations`
--
ALTER TABLE `attribute_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attribute_values`
--
ALTER TABLE `attribute_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `attribute_value_translations`
--
ALTER TABLE `attribute_value_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `category_translations`
--
ALTER TABLE `category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_translations`
--
ALTER TABLE `coupon_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_rates`
--
ALTER TABLE `currency_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `entity_files`
--
ALTER TABLE `entity_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;

--
-- AUTO_INCREMENT for table `featured_categories`
--
ALTER TABLE `featured_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `featured_category_translations`
--
ALTER TABLE `featured_category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `flash_sales`
--
ALTER TABLE `flash_sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `flash_sale_products`
--
ALTER TABLE `flash_sale_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `flash_sale_translations`
--
ALTER TABLE `flash_sale_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `menu_translations`
--
ALTER TABLE `menu_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `meta_data`
--
ALTER TABLE `meta_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `meta_data_translations`
--
ALTER TABLE `meta_data_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `option_translations`
--
ALTER TABLE `option_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `option_values`
--
ALTER TABLE `option_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `option_value_translations`
--
ALTER TABLE `option_value_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_product_options`
--
ALTER TABLE `order_product_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `product_attributes`
--
ALTER TABLE `product_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `product_translations`
--
ALTER TABLE `product_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `role_translations`
--
ALTER TABLE `role_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `search_terms`
--
ALTER TABLE `search_terms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `setting_translations`
--
ALTER TABLE `setting_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `shopping_carts`
--
ALTER TABLE `shopping_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shopping_cart_items`
--
ALTER TABLE `shopping_cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `slider_slides`
--
ALTER TABLE `slider_slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `slider_slide_translations`
--
ALTER TABLE `slider_slide_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `slider_translations`
--
ALTER TABLE `slider_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tax_classes`
--
ALTER TABLE `tax_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_class_translations`
--
ALTER TABLE `tax_class_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_rates`
--
ALTER TABLE `tax_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax_rate_translations`
--
ALTER TABLE `tax_rate_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translation_translations`
--
ALTER TABLE `translation_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activations`
--
ALTER TABLE `activations`
  ADD CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attributes`
--
ALTER TABLE `attributes`
  ADD CONSTRAINT `attributes_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_categories`
--
ALTER TABLE `attribute_categories`
  ADD CONSTRAINT `attribute_categories_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_set_translations`
--
ALTER TABLE `attribute_set_translations`
  ADD CONSTRAINT `attribute_set_translations_attribute_set_id_foreign` FOREIGN KEY (`attribute_set_id`) REFERENCES `attribute_sets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_translations`
--
ALTER TABLE `attribute_translations`
  ADD CONSTRAINT `attribute_translations_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_values`
--
ALTER TABLE `attribute_values`
  ADD CONSTRAINT `attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `attribute_value_translations`
--
ALTER TABLE `attribute_value_translations`
  ADD CONSTRAINT `attribute_value_translations_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupon_categories`
--
ALTER TABLE `coupon_categories`
  ADD CONSTRAINT `coupon_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `coupon_categories_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupon_products`
--
ALTER TABLE `coupon_products`
  ADD CONSTRAINT `coupon_products_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `coupon_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `coupon_translations`
--
ALTER TABLE `coupon_translations`
  ADD CONSTRAINT `coupon_translations_coupon_id_foreign` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cross_sell_products`
--
ALTER TABLE `cross_sell_products`
  ADD CONSTRAINT `cross_sell_products_cross_sell_product_id_foreign` FOREIGN KEY (`cross_sell_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cross_sell_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entity_files`
--
ALTER TABLE `entity_files`
  ADD CONSTRAINT `entity_files_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `featured_categories`
--
ALTER TABLE `featured_categories`
  ADD CONSTRAINT `featured_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `flash_sale_products`
--
ALTER TABLE `flash_sale_products`
  ADD CONSTRAINT `flash_sale_products_flash_sale_id_foreign` FOREIGN KEY (`flash_sale_id`) REFERENCES `flash_sales` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `flash_sale_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_items_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `menu_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menu_items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_item_translations`
--
ALTER TABLE `menu_item_translations`
  ADD CONSTRAINT `menu_item_translations_menu_item_id_foreign` FOREIGN KEY (`menu_item_id`) REFERENCES `menu_items` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `menu_translations`
--
ALTER TABLE `menu_translations`
  ADD CONSTRAINT `menu_translations_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `meta_data_translations`
--
ALTER TABLE `meta_data_translations`
  ADD CONSTRAINT `meta_data_translations_meta_data_id_foreign` FOREIGN KEY (`meta_data_id`) REFERENCES `meta_data` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `option_translations`
--
ALTER TABLE `option_translations`
  ADD CONSTRAINT `option_translations_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `option_values`
--
ALTER TABLE `option_values`
  ADD CONSTRAINT `option_values_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `option_value_translations`
--
ALTER TABLE `option_value_translations`
  ADD CONSTRAINT `option_value_translations_option_value_id_foreign` FOREIGN KEY (`option_value_id`) REFERENCES `option_values` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_products`
--
ALTER TABLE `order_products`
  ADD CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_product_options`
--
ALTER TABLE `order_product_options`
  ADD CONSTRAINT `order_product_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_product_options_order_product_id_foreign` FOREIGN KEY (`order_product_id`) REFERENCES `order_products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_product_option_values`
--
ALTER TABLE `order_product_option_values`
  ADD CONSTRAINT `order_product_option_values_option_value_id_foreign` FOREIGN KEY (`option_value_id`) REFERENCES `option_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_product_option_values_order_product_option_id_foreign` FOREIGN KEY (`order_product_option_id`) REFERENCES `order_product_options` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_taxes`
--
ALTER TABLE `order_taxes`
  ADD CONSTRAINT `order_taxes_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_taxes_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `persistences`
--
ALTER TABLE `persistences`
  ADD CONSTRAINT `persistences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_attributes`
--
ALTER TABLE `product_attributes`
  ADD CONSTRAINT `product_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD CONSTRAINT `product_attribute_values_attribute_value_id_foreign` FOREIGN KEY (`attribute_value_id`) REFERENCES `attribute_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attribute_values_product_attribute_id_foreign` FOREIGN KEY (`product_attribute_id`) REFERENCES `product_attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_options`
--
ALTER TABLE `product_options`
  ADD CONSTRAINT `product_options_option_id_foreign` FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_translations`
--
ALTER TABLE `product_translations`
  ADD CONSTRAINT `product_translations_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `related_products`
--
ALTER TABLE `related_products`
  ADD CONSTRAINT `related_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `related_products_related_product_id_foreign` FOREIGN KEY (`related_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reminders`
--
ALTER TABLE `reminders`
  ADD CONSTRAINT `reminders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_translations`
--
ALTER TABLE `role_translations`
  ADD CONSTRAINT `role_translations_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `setting_translations`
--
ALTER TABLE `setting_translations`
  ADD CONSTRAINT `setting_translations_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shopping_carts`
--
ALTER TABLE `shopping_carts`
  ADD CONSTRAINT `shopping_carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shopping_cart_items`
--
ALTER TABLE `shopping_cart_items`
  ADD CONSTRAINT `shopping_cart_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `shopping_cart_items_shopping_cart_id_foreign` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_carts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slider_slides`
--
ALTER TABLE `slider_slides`
  ADD CONSTRAINT `slider_slides_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slider_slide_translations`
--
ALTER TABLE `slider_slide_translations`
  ADD CONSTRAINT `slider_slide_translations_slider_slide_id_foreign` FOREIGN KEY (`slider_slide_id`) REFERENCES `slider_slides` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `slider_translations`
--
ALTER TABLE `slider_translations`
  ADD CONSTRAINT `slider_translations_slider_id_foreign` FOREIGN KEY (`slider_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tax_class_translations`
--
ALTER TABLE `tax_class_translations`
  ADD CONSTRAINT `tax_class_translations_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD CONSTRAINT `tax_rates_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tax_rate_translations`
--
ALTER TABLE `tax_rate_translations`
  ADD CONSTRAINT `tax_rate_translations_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `throttle`
--
ALTER TABLE `throttle`
  ADD CONSTRAINT `throttle_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `translation_translations`
--
ALTER TABLE `translation_translations`
  ADD CONSTRAINT `translation_translations_translation_id_foreign` FOREIGN KEY (`translation_id`) REFERENCES `translations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `up_sell_products`
--
ALTER TABLE `up_sell_products`
  ADD CONSTRAINT `up_sell_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `up_sell_products_up_sell_product_id_foreign` FOREIGN KEY (`up_sell_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wish_lists`
--
ALTER TABLE `wish_lists`
  ADD CONSTRAINT `wish_lists_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wish_lists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
