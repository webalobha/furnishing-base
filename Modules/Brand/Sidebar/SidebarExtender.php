<?php

namespace Modules\Brand\Sidebar;

use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Maatwebsite\Sidebar\Group;
use Modules\Admin\Sidebar\BaseSidebarExtender;

class SidebarExtender extends BaseSidebarExtender
{
    public function extend(Menu $menu)
    {
        $menu->group(trans('admin::sidebar.content'), function (Group $group) {
            $group->item('Brands', function (Item $item) {
                $item->icon('fa fa-tags');
                $item->weight(20);
                $item->route('admin.brands.index');
                $item->authorize(
                    $this->auth->hasAccess('admin.brands.index')
                );
            });
        });
    }
}
