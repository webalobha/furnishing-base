<?php

namespace Modules\Brand\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Media\Eloquent\HasMedia;
use Modules\Media\Entities\File;
use Modules\Brand\Admin\BrandTable;

class Brand extends Model
{
    use HasMedia;

    protected $fillable = ['slug', 'display_name', 'title', 'description'];

    //products relation
    public function products()
    {
        return $this->hasMany('Modules\Product\Entities\Product');
    }

    public function table($request)
    {
        $query = $this->newQuery()
            ->withBannerImage();
        return new BrandTable($query);
    }

    public function scopeWithBannerImage($query)
    {
        $query->with(['files' => function ($query) {
            $query->wherePivot('zone', 'banner_image');
        }]);
    }

    public function scopeWithProducts($query)
    {
        $query->with(['products' => function ($query) {
            $query->withBaseImage()->orderByDesc('created_at');
        }]);
    }

    public function getBannerImageAttribute()
    {
        return $this->files->where('pivot.zone', 'banner_image')->first() ?: new File;
    }

    public function getBannerImageUrlAttribute()
    {
        return $this->banner_image->path;
    }

    public static function list_api()
    {
        $brands = self::withBannerImage()->get()->sortBy("display_name", SORT_NATURAL|SORT_FLAG_CASE)->values();
        return $brands;
    }

    public static function get_api($slug)
    {
        $brand = self::withBannerImage()->withProducts()->where(["slug"=>$slug])->first();
        return $brand;
    }
}
