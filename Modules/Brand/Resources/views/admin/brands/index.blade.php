@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', 'Brands')

    <li class="active">Brands</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'brands')
    @slot('name', 'Brand')

    @slot('thead')
        <tr>
            @include('admin::partials.table.select_all')

            <th>Slug</th>
            <th>Display Name</th>
            <th>Banner Image</th>
            <th>Title</th>
            <th>Description</th>
            <th data-sort>Created At</th>
        </tr>
    @endslot
@endcomponent

@push('scripts')
    <script>
        new DataTable('#brands-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'slug', name: 'slug', orderable: false, defaultContent: '' },
                { data: 'display_name' },
                { data: 'thumbnail', orderable: false, searchable: false, width: '10%' },
                { data: 'title', name: 'title' },
                { data: 'description', name: 'description', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
@endpush
