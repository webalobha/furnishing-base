@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => 'Brand']))
    @slot('subtitle', $brand->slug)

    <li><a href="{{ route('admin.brands.index') }}">Brands</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => 'Brand']) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.brands.update', $brand) }}" class="form-horizontal" id="brand-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}

        {!! $tabs->render(compact('brand')) !!}
    </form>
@endsection

@include('brand::admin.brands.partials.scripts')
