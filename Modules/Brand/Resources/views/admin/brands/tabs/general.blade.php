<div class="row">
    <div class="col-md-8">
        @include('media::admin.image_picker.single', [
        'title' => 'Brand Banner Image',
        'inputName' => 'files[banner_image]',
        'file' => $brand->banner_image,
        ])
        {{ Form::text('slug', 'Slug', $errors, $brand, ['required' => true]) }}
        {{ Form::text('display_name', 'Display Name', $errors, $brand, ['required' => true]) }}
        {{ Form::text('title', 'Title', $errors, $brand, ['required' => true]) }}
        {{ Form::text('description', 'Description', $errors, $brand, ['required' => true]) }}
    </div>
</div>
