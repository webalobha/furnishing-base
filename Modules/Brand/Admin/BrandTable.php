<?php

namespace Modules\Brand\Admin;

use Modules\Admin\Ui\AdminTable;

class BrandTable extends AdminTable
{
    public function make()
    {
        return $this->newTable()
            ->editColumn('thumbnail', function ($brand) {
                $path = optional($brand->banner_image)->path;
                return view('brand::admin.brands.partials.table.thumbnail', compact('path'));
            });
    }
}
