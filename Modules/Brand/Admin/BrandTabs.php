<?php

namespace Modules\Brand\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;

class BrandTabs extends Tabs
{
    public function make()
    {
        $this->group('brand_information', 'Brand Information')
            ->active()
            ->add($this->general());
    }

    public function general()
    {
        return tap(new Tab('general', 'General'), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);

            $tab->fields([
                'slug',
                'display_name',
                'title',
                'description'
            ]);

            $tab->view('brand::admin.brands.tabs.general');
        });
    }
}
