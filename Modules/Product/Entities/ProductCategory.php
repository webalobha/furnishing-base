<?php

namespace Modules\Product\Entities;

use Modules\Support\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class ProductCategory extends Model
{
  
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
 // protected $table ='product_categories';
  protected $fillable =
   						[
   							'product_id',
                'category_id',
                       	];

      

}