<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Admin\Traits\HasCrudActions;
use Modules\Product\Http\Requests\SaveProductRequest;

#other classes
use DB;
// use File;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

#models
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Entities\AttributeSet;
use Modules\Attribute\Entities\AttributeValue;
use Modules\Attribute\Entities\ProductAttribute;
use Modules\Attribute\Entities\AttributeTranslation;
use Modules\Attribute\Entities\ProductAttributeValue;
use Modules\Attribute\Entities\AttributeSetTranslation;
use Modules\Attribute\Entities\AttributeValueTranslation;

use Modules\Product\Entities\Product;
use Modules\Media\Entities\File;
use Modules\Media\Entities\EntityFile;
use Modules\Category\Entities\Category;
use Modules\Product\Entities\ProductCategory;
use Modules\Category\Entities\CategoryTranslation;

class ProductController extends Controller
{
    use HasCrudActions;

    public function __construct(
                                Product                       $product,
                                File                          $file,
                                Category                      $category,
                                Attribute                     $attribute,
                                EntityFile                    $entityFiles,
                                AttributeSet                  $attributeSet,
                                AttributeValue                $attributeValue,
                                ProductAttribute              $productAttribute,
                                ProductCategory               $product_categories,
                                CategoryTranslation           $categoryTranslation,
                                AttributeTranslation          $attributeTranslation,
                                ProductAttributeValue         $productAttributeValue,
                                AttributeSetTranslation       $attributeSetTranslation,
                                AttributeValueTranslation     $attributeValueTranslation
                               )
    {
        $this->file                      =  $file;
        $this->product                   =  $product;
        $this->category                  =  $category;
        $this->attribute                 =  $attribute;
        $this->entityFiles               =  $entityFiles;
        $this->attributeSet              =  $attributeSet;
        $this->attributeValue            =  $attributeValue;
        $this->productAttribute          =  $productAttribute;
        $this->product_categories        =  $product_categories;
        $this->categoryTranslation       =  $categoryTranslation;
        $this->attributeTranslation      =  $attributeTranslation;
        $this->productAttributeValue     =  $productAttributeValue;
        $this->attributeSetTranslation   =  $attributeSetTranslation;
        $this->attributeValueTranslation =  $attributeValueTranslation;
    }

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'product::products.product';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'product::admin.products';

    /**
     * Form requests for the resource.
     *
     * @var array|string
     */
    protected $validation = SaveProductRequest::class;

    public function bulkupload(Request $request)
    {
        return view('product::admin.products.bulkupload');
    }


    #store bulkupload file
    public function storeBulkupload(Request $request)
    {
        if(!isset($request->csv_file))
        {
            return back()->with('error', 'Please select File');
        } else
        {
            #assign file name 
            $fileName = $request->csv_file;
            $fileNames = [];
            
            # Load The file in Excel and Extract the File
            Excel::load($fileName, function ($reader)
            {
                $duplicate_count = $inserted_count = 0;
                $rowNumber =  $emptyRowCount = 0;
                $required_headers = ['item', 'category', 'price'];

                //Start Database transaction
                foreach ($reader->toArray() as $key => $store)
                {
                  try
                  {

                    if($emptyRowCount == 5)
                    break;
               
                    //check empty rows
                    if($this->empty_row_check($required_headers, $store))
                    {
                        $emptyRowCount++;
                        continue;
                    }

                    # get description of product.
                 
                    $resultDescription = $this->setDescription($store);
                   
                    #defining the variable
                    $shortDescription           =   '';
                    $style1                     =   '';
                    $style2                     =   '';
                    $color                      =   '';
                    $productType                =   '';
                    $item                       =   '';
                    
    
                    if(!empty($store['short_description'])) {
                        $shortDescription = $store['short_description'];
                    }
                    
                    $data_product = [
                                        'slug'              =>   $store['name'] ?? '',
                                        'price'             =>   $store['price'] ?? '',
                                        'description'       =>   $resultDescription['description'] ?? '',
                                        'short_description' =>   $shortDescription ?? '',
                                        'is_active'         =>   '1',
                                        'manage_stock'      =>   '0',
                                        'in_stock'          =>   '1',
                                        'name'              =>   $store['name'] ?? '',
                                        'item_number'       =>   $store['item'] ?? '',
                                    ];
                    
                    # store newly created product data
                    $productid = $this->product->create($data_product);
                    
                    # create category and subcategory with his product category relation and category translation.
                    $this->storeCategory($store['category'], $store['subcategory'], $productid->id);
                    
                    # upload the files (Refrence and image file).
                    $result = $this->storeAndUploadFiles($productid, $store);
                   

                    #uplode color, Product_Type,style1 and  style2
                    $this->storeFeature( $store, $productid->id );
                  // DB::commit();
                  } catch(\Exception $ex)
                  {
                    continue;
                  }
                }
            });

            return back()->with('success', 'Store Data has been stored Successfully.');
        }
    }

    # Check Empty Row
    private function empty_row_check($headers, $row) : bool
    {
        $col_count = 0;
        foreach ($headers as $header)
        {
            
            if(is_null($row[$header]))
            {
                $col_count++;
            }
        }
        return ($col_count == count($headers)) ? TRUE : FALSE;
    }

    #Extension validation function
    private function extensionCheck($file_name)
    {
        # Get the File Name
        $fileName = $file_name;

        # Explode the file to get The extension
        $explodedFile = explode('.', $fileName);
    
        # Count the Explode Array File 
        $countExplode = count($explodedFile);

        # Get the Extension Of File
        $extension = $explodedFile[$countExplode - 1];
        
        return $extension;
    }

    /**
     * set description data.s
     * 
     * @param $customer Array.
     */
    public function setDescription($data)
    { 
        $longDescription = $shippingLengthInch = $shippingWidthInch = $shippingHeight = $shippingVolume = $shippingWeight = $productDimension = $material = $material1 = $material2 = $feature1 = $feature2 = $feature3 = $feature4 = $feature5 = $feature6 = $feature7 = $feature8 = $feature9 = $feature10 = $summary = '';

        if(!empty($data['long_description'])) {
            $longDescription = '<p>'.$data['long_description'].'</p>';
        }
        
        $ul     = "<ul>";
        $endUl  =  "</ul><p>&nbsp</p>";

        if(!empty($data['shipping_length_inch'])) {
            $shippingLengthInch = '<li> Shipping Length (Inch) :'.
                                    $data['shipping_length_inch'].'</li>';
        }

        if(!empty($data['shipping_width_inch'])) {
            $shippingWidthInch = '<li> Shipping Width (Inch) :'.
                                    $data['shipping_width_inch'].'</li>';
        }

        if(!empty($data['shipping_height_inch'])) {
           $shippingHeight    = '<li>Shipping Height (Inch)  : '.
                                $data['shipping_height_inch'].'</li>'; 
        }

        if(!empty($data['shipping_volume_cuft'])) {
            $shippingVolume    = '<li>Shipping Volume (CuFt) : '.
                                $data['shipping_volume_cuft'].'</li>'; 
        }

        if(!empty($data['shipping_weight_lb'])) {
           $shippingWeight    = '<li>Shipping Weight (LB) : '.
                                $data['shipping_weight_lb'].'</li>'; 
        }

        if(!empty($data['product_dimension_inch'])) {
           $productDimension  = '<li>Product Dimension (Inch) : '.
                                $data['product_dimension_inch'].'</li>'; 
        }

        if(!empty($data['material'])){
           $material   = '<li>Material : '.$data['material'].'</li>'; 
        }

        if(!empty($data['material_1'])) {
          $material1     = '<li> Material %1 :' . $data['material_1'] . '</li>';
        }

        if(!empty($data['material_2'])) {
          $material2   = '<li>Material %2 :'. $data['material_2'] . '</li>'; 
        }

        if(!empty($data['summary'])) {
          $summary  = '<li>Summary :'. $data['summary'] . '</li>';
        }

        if(!empty($data['feature_1'])) {
          $feature1    = $data['feature_1' ] . ','; 
        } 

        if(!empty($data['feature_2'])) {
          $feature2    = $data['feature_2'].','; 
        }else {
           $feature2 = '';
        }
        if(!empty($data['feature_3'])) {
          $feature3    = $data['feature_3'].','; 
        }

        if(!empty($data['feature_4'])) {
          $feature4   = $data['feature_4'].','; 
        }

        if(!empty($data['feature_5'])) {
          $feature5   = $data['feature_5'].','; 
        }

        if(!empty($data['feature_6'])) {
          $feature6   = $data['feature_6'].','; 
        }

        if(!empty($data['feature_7'])) {
          $feature7   = $data['feature_7'].','; 
        }

        if(!empty($data['feature_8'])) {
          $feature8   = $data['feature_8'].','; 
        }

       if(!empty($data['feature_9'])) {
          $feature9   = $data['feature_9'].','; 
        }

        if(!empty($data['feature_10'])) {
          $feature10  = $data['feature_10'].','; 
        }
        

        $features     = '<li>Features : '. $feature1 . $feature2 . $feature3 
                                         . $feature4 . $feature5 . $feature6 
                                         . $feature7 . $feature8 . $feature9 
                                         . $feature10 .
                        '</li>'; 

        $description =  $longDescription  . $ul . $shippingLengthInch . $shippingWidthInch .
                        $shippingHeight   . $shippingVolume . $shippingWeight . 
                        $productDimension . $material . $material1 . $material2 . 
                        $features . $summary .
                        $endUl;

        return ['description'  =>  $description];
    }

   /**
    * create new category
    * 
    * @param category name.
    */
   public function storeCategory($categoryName, $subCategoryName, $productId)
   { 

        $categorySlugName = strtolower($categoryName);
       
        $category = $this->category->where('slug', $categorySlugName)->first();

        if (!empty($category))
        {
            # create the product category.
            $this->storeProductCategory($category->id, $productId);

            if (!empty($subCategoryName)) {
              $this->storeSubCategory($subCategoryName, $category->id, $productId);
            }
        } else {

        if (!empty($categoryName)) {
           #set data for product and is category
           $productData = [
                            'slug'           =>  $categoryName,
                            'is_searchable'  =>  1,
                            'is_active'      =>  1,
                          ];
           
            #storing newly created product_category                          
            $categoryId = $this->category->create($productData);
           
            # generate category translation
            $this->storeCatgeoryTranslation($categoryId);

            # create the product category.
            $this->storeProductCategory($categoryId->id, $productId);

            if (!empty($subCategoryName)) {
              $this->storeSubCategory($subCategoryName, $categoryId->id, $productId);
            }
        }
     }
   }

   /**
    * create new sub-category
    * 
    * @param sub-category name.
    */
    public function storeSubCategory($subCategoryName, $categoryId, $productId)
    {
        $subCategorySlugName = slugify($subCategoryName);

        $subCategory = $this->category
                        ->where('slug', $subCategorySlugName)
                        ->where('parent_id', $categoryId)
                        ->first();

      if (!empty($subCategory)) {
          # create the product category.
          $this->storeProductCategory($subCategory->id, $productId);

      } else {

                if (!empty($subCategoryName)) {
                #set data for product and is category
                $productData = [
                                'parent_id'      =>  $categoryId,
                                'slug'           =>  $subCategoryName,
                                'is_searchable'  =>  0,
                                'is_active'      =>  1,
                              ];

                #storing newly created product_category                          
                $subCategoryId = $this->category->create($productData);

                # generate category translation
                $this->storeCatgeoryTranslation($subCategoryId);

                # create the product category.
                $this->storeProductCategory($subCategoryId->id, $productId);
           }
       }
    }

     /**
    * store category lang translation data.
    * 
    * @param $category Array
    */
   public function storeCatgeoryTranslation($category)
   {
      $catgeoryTranslationData  = [
                                  'category_id'  =>  $category->id,
                                  'locale'       =>  'en',
                                  'name'         =>  $category->slug,
                                  ];

      $this->categoryTranslation->create($catgeoryTranslationData);
   }

   /**
    * store product and category relation
    * 
    * @param category id, product id.
    */
   public function storeProductCategory($categoryId, $productId)
   {
      $productCategory  =  $this->product_categories
                                ->where('category_id', $categoryId)
                                ->where('product_id', $productId)
                                ->first();

      if (empty($productCategory)) {
        #set data for product and is category
        $productData =  [
                          'product_id'   => $productId,
                          'category_id'  => $categoryId,
                        ];

        #storing newly created product_category                          
        $this->product_categories->create($productData);
      }
   }

   /**
    * Store and upload the files.
    * 
    * @param $image array, $productId
    */
   public function storeAndUploadFiles($productId, $image)
   {
    
      if (!empty($image['image1'])) {

        $result  = $this->fileExistsOrNot($image['image1']);

        if ($result['message'] == 1) {
            # store file 
            $this->storeFileNameAndUrl( 'base_image', $result, $productId );
        }
      }

      if (!empty($image['image2'])) {

        $result  = $this->fileExistsOrNot($image['image2']);
        
        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image3'])) {

        $result  = $this->fileExistsOrNot($image['image3']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image4'])) {

        $result  = $this->fileExistsOrNot($image['image4']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image5'])) {

        $result  = $this->fileExistsOrNot($image['image5']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_5'])) {

        $result  = $this->fileExistsOrNot($image['image_5']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_6'])) {

        $result  = $this->fileExistsOrNot($image['image_6']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_7'])) {

        $result  = $this->fileExistsOrNot($image['image_7']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_8'])) {

        $result  = $this->fileExistsOrNot($image['image_8']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_9'])) {

        $result  = $this->fileExistsOrNot($image['image_9']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }

      if (!empty($image['image_10'])) {

        $result  = $this->fileExistsOrNot($image['image_10']);

        if ($result['message'] == 1) {
           
            # store file 
            $this->storeFileNameAndUrl( 'additional_images', $result, $productId );
        }
      }
   }
   
   /**
    * check file exist or not
    * 
    * @param File name.
    */
    public function fileExistsOrNot($fileName)
    {    

        if (file_exists(storage_path('app/public/media1/' . $fileName)) ) {

        # Get the File
        $file = storage_path('app/public/media1/'. $fileName);

        # Explode the file to get The extension
        $explodedFile = explode('.', $file);

        # get file actual name for set path
        $pathFileName = explode('media1', $file);

        $output  =  [
                      'message'   =>  1,
                      'file_name' =>  $fileName,
                      'extension' =>  end($explodedFile),
                      'size'      =>  filesize($file),
                      'path'      =>  'media1' . end($pathFileName),
                      'mime'      =>   mime_content_type($file)
                    ];

        return $output;

      } else {
        return ['message' => 2];
      } 
    }

   /**
    * store the file path
    * 
    * @param $type, $result, $productId
    */
   public function storeFileNameAndUrl( $type, $result, $productId )
   {
      $fileData   =  [
                          'user_id'   => '1',
                          'disk'      => 'public_storage',
                          'filename'  =>  $result['file_name'] ?? '',
                          'path'      =>  $result['path'] ?? '',
                          'extension' =>  $result['extension'] ?? '',
                          'mime'      =>  $result['mime'] ?? '',
                          'size'      =>  $result['size'] ?? '',
                     ];

      $file = $this->file->create($fileData);

      # storing newly created file reference to entity table
      $entityData = [
                         'file_id'       =>  $file->id,
                         'entity_type'   =>  'Modules\Product\Entities\Product',
                         'entity_id'     =>  $productId->id ?? '',
                         'zone'          =>  $type ?? '',
                     ];

      $this->entityFiles->create($entityData);
   }

   /**
    * Store feature
    * 
    * @param
    */
   public function storeFeature($colorStyle, $productId)
   {
      # set the specification variable.
      $specificationAttributeSetTranslation = 'Specifications';

      # check name alrady in db or not.
      $checkSpecification  =  $this->attributeSetTranslation
                                    ->where('name', $specificationAttributeSetTranslation)
                                    ->first();

      # check is exist or not.
      if (!empty($checkSpecification)) {
        
        $this->storeColorSizeStyleAttribute($colorStyle, $productId, $checkSpecification->attribute_set_id);

      } else {

        $attributeSet = $this->attributeSet->create();

        $attributeSetTranslationData =  [ 
                                          'attribute_set_id' =>  $attributeSet->id ?? '',
                                          'locale'           =>  'en',
                                          'name'             =>  $specificationAttributeSetTranslation,
                                        ];

        $attributeSetTranslationId  =  $this->attributeSetTranslation->create($attributeSetTranslationData);

        $this->storeColorSizeStyleAttribute($colorStyle, $productId, $attributeSetTranslationId->attribute_set_id);
      }
   }

   /**
    * store specification attributes.
    * 
    * @param
    */
   public function storeColorSizeStyleAttribute($colorStyle, $productId, $attributeSetId)
   {
      $color    = 'Color';
      $size     = 'Size'; # product type
      $style    = 'Style';
      $style1   = 'Style1';
      $item_number ='Item Number';

      $colorId  =  $this->attributeTranslation->where('name', $color)->first();

      if (!empty($colorId)) {
        $this->storeAttributeValue($colorId->attribute_id, $colorStyle, $productId, $color);
      } else {
        $this->storeNewAttribute($attributeSetId, $color, $colorStyle, $productId);
      }

      $sizeId  =  $this->attributeTranslation->where('name', $size)->first();
      if (!empty($sizeId)) {
        $this->storeAttributeValue($sizeId->attribute_id, $colorStyle, $productId, $size);
      } else {
        $this->storeNewAttribute($attributeSetId, $size, $colorStyle, $productId);
      }

      $styleId  =  $this->attributeTranslation->where('name', $style)->first();
      if (!empty($styleId)) {
        $this->storeAttributeValue($styleId->attribute_id, $colorStyle, $productId, $style);
      } else {
        $this->storeNewAttribute($attributeSetId, $style, $colorStyle, $productId);
      }

      $styleId1  =  $this->attributeTranslation->where('name', $style1)->first();
      if (!empty($styleId1)) {
        $this->storeAttributeValue($styleId1->attribute_id, $colorStyle, $productId, $style1);
      } else {
        $this->storeNewAttribute($attributeSetId, $style1, $colorStyle, $productId);
      }
     /* $item_numberId  =  $this->attributeTranslation->where('name', $item_number)->first();
      if (!empty($item_numberId)) {
        $this->storeAttributeValue($item_numberId->attribute_id, $colorStyle, $productId, $item_number);
      } else {
        $this->storeNewAttribute($attributeSetId, $item_number, $colorStyle, $productId);
      }*/

   }

   /**
    * store the color size
    * 
    * @param 
    */
   public function storeNewAttribute($attributeSetId, $name, $colorStyle, $productId)
   {
      $attributes =   [ 
                        'attribute_set_id'  =>  $attributeSetId ?? '',
                        'is_filterable'     =>  1,
                      ];

      $attributesId  = $this->attribute->create($attributes);

      $attributeTranslationData = [ 
                                  'attribute_id'     =>  $attributesId->id ?? '',
                                  'locale'           =>  'en',
                                  'name'             =>  $name,
                                  ];

      $attributeTranslationId  =  $this->attributeTranslation->create($attributeTranslationData);

      $this->storeAttributeValue($attributesId->id, $colorStyle, $productId, $name);

   }
   
   /**
    * Store the attribute value.
    * 
    * @param
    */
   public function storeAttributeValue($attributesId, $colorStyle, $productId, $type)
   {
      if ($type == 'Color') {
        if(!empty($colorStyle['color'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['color'], $productId);
        }
      }

      if ($type == 'Size') {
        if(!empty($colorStyle['product_type'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['product_type'], $productId);
        }
      }

      if ($type == 'Style') {
        if(!empty($colorStyle['style_1'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['style_1'], $productId);
        }
      }

      if ($type == 'Style1') {
        if(!empty($colorStyle['style_2'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['style_2'], $productId);
        }
      }
     /* if ($type == 'Item Number') {
        if(!empty($colorStyle['item'])) {
        $this->storeAttributeValueTemplate($attributesId, $colorStyle['item'], $productId);
        }
      }*/
   }

   /**
    * store attribute value.
    * 
    *
    */
   public function storeAttributeValueTemplate($attributesId, $value, $productId)
   {
      if(!empty($value)) {

        $translationValue = slugify($value);

        $attributeTranslationValue = $this->attributeValueTranslation
                                          ->where('value', $translationValue)
                                          ->first();

        if(empty($attributeTranslationValue)) {

          $attributeValue  =  ['attribute_id'  =>  $attributesId, 'position'  => 0];

          $attributeValueId =  $this->attributeValue->create($attributeValue);
          
          $attributeValueTranslation  =  [
                                         'attribute_value_id'  =>  $attributeValueId->id, 
                                         'value'               =>  $value ?? '', 
                                         'locale'              =>  'en'
                                         ];
           
          $attributeValueTranslationId = $this->attributeValueTranslation
                                                          ->updateOrCreate($attributeValueTranslation);

          $this->storeRelationProductWithValue($attributesId, $productId, $attributeValueId->id);                                                

        } else {

          $this->storeRelationProductWithValue($attributesId,
                                               $productId, 
                                               $attributeTranslationValue->attribute_value_id
                                              );

        }
      }
          
   }

   /**
    * store relation of product with value.
    * 
    * @param
    */
   public function storeRelationProductWithValue($attributesId, $productId, $attributeValueId)
   {
      /*
      $attributeValue  =  ['attribute_id'  =>  $attributesId, 'product_id'  => $productId];
      
      $productAttributeId  =  $this->productAttribute->create($attributeValue);*/
      $productAttributeId  =  new ProductAttribute();
      $productAttributeId->attribute_id =  $attributesId;
      $productAttributeId->product_id   =   $productId;
      $productAttributeId->save();

     /* $productAttributeValueDatas  =  [
                                          'product_attribute_id'  =>  $productAttributeId->id, 
                                          'attribute_value_id'    =>  $attributeValueId
                                      ];*/
      $productAttributeValue  = new ProductAttributeValue();    
      $productAttributeValue->product_attribute_id = $productAttributeId->id;
      $productAttributeValue->attribute_value_id   =  $attributeValueId;
      $productAttributeValue->save();
      
      /*$this->productAttributeValue->create($productAttributeValueDatas);*/
   }
}
