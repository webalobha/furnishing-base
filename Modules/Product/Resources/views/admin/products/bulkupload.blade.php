@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('Bulk Upload', ['resource' => trans('Bulk Upload')]))

    <li><a href="{{ route('admin.products.index') }}">Bulk upload</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('product::products.product')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.products.store-bulkupload') }}" class="form-horizontal" id="product-create-form" novalidate enctype='multipart/form-data'>
        {{ csrf_field() }}
        <div class="row" style="margin-right: 15px; margin-left: 15px;">
            <div class="col-12">
              <div class="form-group ImageAndVideoDivforReview">
                <label> Upload CSV</label>
                <div class="row" style="margin-right: 15px; margin-left: 15px;">
                  <div class="col-5">
                    <div class="company-logo logo d-flex">
                      <div class="cloud_img">
                         <span class="button"><i class="fa fa-cloud-upload"></i></span>
                      </div>
                      <input type="file" id="csv_file" class="fileLogo form-control" name="csv_file" onchange="return reviewImageVideoValidation();" multiple="multiple" required>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="row justify-content-center my-4"  style="margin-right: 15px; margin-left: 15px;">
           <div class="form-group">
              <div class="col-md-offset-2 col-md-10">
                  <a href="{{url('storage/sample.xlsx')}}" class="btn btn-primary">Download sample</a>
                  <button type="submit" class="btn btn-primary" data-loading="">Save</button>
              </div>
          </div>
        </div>
    </form>
    <script>
    	//CSV Validation
		function reviewImageVideoValidation(value)
		{ 
		    var fileInput =  document.getElementById(`csv_file`); 
		    var fi =  document.getElementById(`csv_file`); 
		    var filePath = fileInput.value; 
		    var extension = filePath.split('.').pop().toLowerCase(); 
		    // Allowing file type 
		    //var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.mp4|\.webm|\.avi)$/i; 
		    //if (!allowedExtensions.exec(filePath)) { 
		    if ((extension != 'csv') && (extension != 'CSV') && (extension != 'xlsx'))
		    { 
		        alert('Invalid file type, please select file type of CSV, xlsx'); 
		        fileInput.value = ''; 
		        return false; 
		    } 

		    // //Size check for image
		    // if(extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'gif')
		    // { 
		    //     if(imageSizeinKb > 1024)
		    //     { 
		    //         alert('Images Size cant ne more than 1MB'); 
		    //         fileInput.value = ''; 
		    //         return false;
		    //     }
		    // }

		}
    </script>
@endsection

@include('product::admin.products.partials.shortcuts')

