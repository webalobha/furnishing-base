<?php

return [
    'admin.featured_categories' => [
        'index' => 'featuredcategory::permissions.index',
        'create' => 'featuredcategory::permissions.create',
        'edit' => 'featuredcategory::permissions.edit',
        'destroy' => 'featuredcategory::permissions.destroy',
    ],

// append

];
