<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'all-assets' => [
        'admin.featuredcategory.css' => ['module' => 'featuredcategory:admin/css/featuredcategory.css'],
        'admin.featuredcategory.js' => ['module' => 'featuredcategory:admin/js/featuredcategory.js'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your pages
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'required-assets' => [],
];
