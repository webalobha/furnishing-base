<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturedCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured_category_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('featured_category_id');

            $table->string('locale');

            $table->unique(['featured_category_id', 'locale'], 'featured_category_translations_locale_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featured_category_translations');
    }
}
