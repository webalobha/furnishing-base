<?php

namespace Modules\FeaturedCategory\Entities;

use Modules\Support\Eloquent\TranslationModel;

class FeaturedCategoryTranslation extends TranslationModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
}
