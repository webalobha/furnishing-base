<?php

namespace Modules\FeaturedCategory\Entities;

use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;
use Modules\Media\Eloquent\HasMedia;
use Modules\Media\Entities\File;
use Modules\FeaturedCategory\Admin\FeaturedCategoryTable;

class FeaturedCategory extends Model
{
    use Translatable, HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                         'feature_type',
                         'position',
                         'category_id'
                          ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];

    public static $feature_types = [
        'mobile_home_top'=>'Mobile Home Top',
        'mobile_home_bottom'=>'Mobile Home Bottom'
    ];

    public function category()
    {
        return $this->belongsTo('Modules\Category\Entities\Category');
    }

    public function table($request)
    {
        $query = $this->newQuery();
        return new FeaturedCategoryTable($query);
    }

    public function getBannerImageAttribute()
    {
        return $this->files->where('pivot.zone', 'banner_image')->first() ?: new File;
    }

    public function getBannerImageUrlAttribute()
    {
        return $this->banner_image->path;
    }

    public static function list_api($feature_type)
    {
        $featued_categories = FeaturedCategory::with('category:id,slug')->where(["feature_type"=>$feature_type])->get()->sortBy('position');
        $objs = [];
        foreach ($featued_categories as $featued_category) {
            $obj = [
                "id" => $featued_category->category->id,
                "name" => $featued_category->category->name,
                "slug" => $featued_category->category->slug,
                "banner_image" => $featued_category->banner_image->path
            ];
            array_push($objs, $obj);
        }
        return $objs;
    }
}
