<?php

namespace Modules\FeaturedCategory\Providers;

use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;
use Modules\FeaturedCategory\Admin\FeaturedCategoryTabs;
use Modules\Admin\Ui\Facades\TabManager;

class FeaturedCategoryServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        TabManager::register('featured_categories', FeaturedCategoryTabs::class);
        $this->addAdminAssets('admin.featured_categories.(create|edit)', [
            'admin.media.css', 'admin.media.js'
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['permissions.php']);
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
