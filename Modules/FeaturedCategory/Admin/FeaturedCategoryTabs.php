<?php

namespace Modules\FeaturedCategory\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\Category\Entities\Category;

class FeaturedCategoryTabs extends Tabs
{
    public function make()
    {
        $this->group('featured_category_information', 'Featured Category Information')
            ->active()
            ->add($this->general());
    }

    public function general()
    {
        return tap(new Tab('general', 'General'), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);

            $tab->fields([
                'feature_type'
            ]);

            $tab->view('featuredcategory::admin.featured_categories.tabs.general', [
                'categories' => Category::all()->pluck('name', 'id')->toArray(),
                'feature_types' => [
                    'app_home' => "Mobile App Home"
                ]
            ]);
        });
    }
}
