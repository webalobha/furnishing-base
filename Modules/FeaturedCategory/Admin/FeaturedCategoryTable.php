<?php

namespace Modules\FeaturedCategory\Admin;

use Modules\Admin\Ui\AdminTable;

class FeaturedCategoryTable extends AdminTable
{
    public function make()
    {
        return $this->newTable();
    }
}
