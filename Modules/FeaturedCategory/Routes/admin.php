<?php

Route::get('featured-categories', [
    'as' => 'admin.featured_categories.index',
    'uses' => 'FeaturedCategoryController@index',
    'middleware' => 'can:admin.featured_categories.index',
]);

Route::get('featured-categories/create', [
    'as' => 'admin.featured_categories.create',
    'uses' => 'FeaturedCategoryController@create',
    'middleware' => 'can:admin.featured_categories.create',
]);

Route::post('featured-categories', [
    'as' => 'admin.featured_categories.store',
    'uses' => 'FeaturedCategoryController@store',
    'middleware' => 'can:admin.featured_categories.create',
]);

Route::get('featured-categories/{id}/edit', [
    'as' => 'admin.featured_categories.edit',
    'uses' => 'FeaturedCategoryController@edit',
    'middleware' => 'can:admin.featured_categories.edit',
]);

Route::put('featured-categories/{id}', [
    'as' => 'admin.featured_categories.update',
    'uses' => 'FeaturedCategoryController@update',
    'middleware' => 'can:admin.featured_categories.edit',
]);

Route::delete('featured-categories/{ids?}', [
    'as' => 'admin.featured_categories.destroy',
    'uses' => 'FeaturedCategoryController@destroy',
    'middleware' => 'can:admin.featured_categories.destroy',
]);

Route::get('featured-categories/index_ajax', [
    'as' => 'admin.featured_categories.index_ajax',
    'uses' => 'FeaturedCategoryController@index_ajax',
    'middleware' => 'can:admin.featured_categories.index',
]);

Route::post('featured-categories/save_grid', [
    'as' => 'admin.featured_categories.save_grid',
    'uses' => 'FeaturedCategoryController@save_grid',
    'middleware' => 'can:admin.featured_categories.create',
]);

// append

