<?php

namespace Modules\FeaturedCategory\Http\Requests;

use Modules\Core\Http\Requests\Request;

class SaveFeaturedCategoryRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'featuredcategory::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=> 'required'
        ];
    }
}
