<?php

namespace Modules\FeaturedCategory\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\FeaturedCategory\Entities\FeaturedCategory;
use Modules\FeaturedCategory\Http\Requests\SaveFeaturedCategoryRequest;
use Illuminate\Http\Request;

class FeaturedCategoryController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = FeaturedCategory::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'featuredcategory::featured_categories.featured_category';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'featuredcategory::admin.featured_categories';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveFeaturedCategoryRequest::class;

    public function index(Request $request)
    {
        $feature_type = $request->get('feature_type');
        $feature_types = FeaturedCategory::$feature_types;
        if (!$feature_type || !in_array($feature_type, array_keys($feature_types))) {
            return redirect(route('admin.featured_categories.index', ["feature_type"=>(session('current_feature_type') ?? array_keys($feature_types)[0])]));
        }
        session(['current_feature_type' => $feature_type]);
        return view('featuredcategory::admin.featured_categories.index')->with([
            'feature_types'=> $feature_types,
            'feature_type'=> $feature_type
        ]);
    }

    public function index_ajax(Request $request)
    {
        $feature_type = $request->get('feature_type');
        session(['current_feature_type' => $feature_type]);
        $featured_categories = FeaturedCategory::where(['feature_type'=>$feature_type])->get()->sortBy('position');
        return view('featuredcategory::admin.featured_categories.partials.cards')->with([
            'featured_categories'=> $featured_categories
        ]);
    }

    public function save_grid(Request $request)
    {
        $grid_order = $request->grid_order;
        foreach ($grid_order as $key=>$id) {
            FeaturedCategory::where(["id"=>$id])->update(["position"=>$key]);
        }
        return $grid_order;
    }
}
