@foreach($featured_categories as $featured_category)
<div class="col-sm-3" style="padding: 10px;">
    <div class="card" style="width: 15rem;height: 15rem;text-align: center;border:1px solid black;" pk="{{ $featured_category->id }}">
        <div class="single-image image-holder-wrapper clearfix">
            @if($featured_category->banner_image->path)
            <img class="card-img-top" src="{{ $featured_category->banner_image->path }}" alt="Banner Image" style="width: 100px;height: 100px;">
            @else
            <div class="image-holder placeholder">
                <i class="fa fa-picture-o" style="width: 100px;height: 100px;"></i>
            </div>
            @endif
        </div>
        <div class="card-body">
            <p class="card-text">{{ $featured_category->category->name }}</p>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.featured_categories.edit', ['id'=> $featured_category->id]) }}"><i class="fa fa-edit"></i></a>
            <a href="javascript:void(0);" class="delete_icon" onclick="featured_category_delete('{{ $featured_category->id }}')"><i class="fa fa-trash"></i></a>
        </div>
    </div>
</div>
@endforeach
