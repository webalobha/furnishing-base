@push('shortcuts')
<dl class="dl-horizontal">
    <dt><code>b</code></dt>
    <dd>{{ trans('admin::admin.shortcuts.back_to_index', ['name' => trans('featuredcategory::featured_categories.featured_category')]) }}</dd>
</dl>
@endpush
@push('scripts')
<script>
    keypressAction([
        { key: 'b', route: "{{ route('admin.featured_categories.index') }}" }
    ]);
</script>
@endpush
