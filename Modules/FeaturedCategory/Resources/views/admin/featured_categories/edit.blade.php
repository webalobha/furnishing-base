@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('featuredcategory::featured_categories.featured_category')]))
    @slot('subtitle', '')

    <li><a href="{{ route('admin.featured_categories.index') }}">{{ trans('featuredcategory::featured_categories.featured_categories') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('featuredcategory::featured_categories.featured_category')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.featured_categories.update', $featuredCategory) }}" class="form-horizontal" id="featured-category-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}
        {!! $tabs->render(compact('featuredCategory')) !!}
    </form>
@endsection

@include('featuredcategory::admin.featured_categories.partials.shortcuts')
