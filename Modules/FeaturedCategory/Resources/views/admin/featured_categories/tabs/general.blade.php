<div class="row">
    <div class="col-md-8">
        <input type="hidden" name="feature_type" value="{{ $featuredCategory->feature_type ? $featuredCategory->feature_type : session('current_feature_type') }}">
        {{ Form::select('category_id', 'Category', $errors, $categories, $featuredCategory) }}
        @include('media::admin.image_picker.single', [
        'title' => 'Banner Image',
        'inputName' => 'files[banner_image]',
        'file' => $featuredCategory->banner_image,
        ])
    </div>
</div>
