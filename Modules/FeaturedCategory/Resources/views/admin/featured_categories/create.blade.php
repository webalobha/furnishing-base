@extends('admin::layout')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('featuredcategory::featured_categories.featured_category')]))

    <li><a href="{{ route('admin.featured_categories.index') }}">{{ trans('featuredcategory::featured_categories.featured_categories') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('featuredcategory::featured_categories.featured_category')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.featured_categories.store') }}" class="form-horizontal" id="featured-category-create-form" novalidate>
        {{ csrf_field() }}
        {!! $tabs->render(compact('featuredCategory')) !!}
    </form>
@endsection

@include('featuredcategory::admin.featured_categories.partials.shortcuts')
