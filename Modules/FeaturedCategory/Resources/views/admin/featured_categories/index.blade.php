@extends('admin::layout')
@component('admin::components.page.header')
@slot('title', trans('featuredcategory::featured_categories.featured_categories'))
<li class="active">{{ trans('featuredcategory::featured_categories.featured_categories') }}</li>
@endcomponent
@component('admin::components.page.index_table')
@slot('buttons', ['create'])
@slot('resource', 'featured_categories')
@slot('name', 'Feature Category')
<div class="row">
    <div class="col-sm-2 form-group">
        <label for="select_feature_type" class="control-label text-left">Select Feature Type</label>
    </div>
    <div class="col-sm-6 form-group">
        <select name="select_feature_type" id="select_feature_type" class="form-control">
            @foreach($feature_types as $key=>$value)
            <option value="{{ $key }}" {{ ($key==$feature_type) ? 'selected' : '' }}>{{ $value }}</option>
            @endforeach
        </select>
    </div>
    <div class="btn-group pull-right">
        <button class="btn btn-primary btn-actions btn-create" onclick="save_grid()">
            Save Changes
        </button>
        <button class="btn btn-primary btn-actions btn-create" onclick="index_ajax()">
            Reset
        </button>
    </div>
</div>
<div class="row" id="drag_div">
</div>
@endcomponent
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script>
    var route_index = "{{ route('admin.featured_categories.index') }}";
    var route_index_ajax = "{{ route('admin.featured_categories.index_ajax') }}";
    var route_save_grid = "{{ route('admin.featured_categories.save_grid') }}";

    const select_feature_type = $("#select_feature_type");
    const featured_category_delete = function (id) {
        $.ajax({
            url: route_index + "/" + id,
            type: 'DELETE',
            success: function (result) {
                index_ajax();
                console.log("deleted");
            }
        });
    }
    const index_ajax = function () {
        $.ajax({
            url: route_index_ajax + "?feature_type=" + select_feature_type.val().trim(),
            type: 'GET',
            success: function (result) {
                $("#drag_div").html(result);
                var el = document.getElementById('drag_div');
                var sortable = Sortable.create(el);
            }
        });
    }
    const save_grid = function () {
        var grid_order = $('.card').map(function () { return this.getAttribute("pk"); }).toArray();
        $.ajax({
            url: route_save_grid,
            data: {
                grid_order: grid_order
            },
            type: 'POST',
            success: function (result) {
            }
        });
    }
    $(document).ready(function () {
        index_ajax();
        select_feature_type.on('change', function () {
            index_ajax();
        });
    });
</script>
@endpush
