<?php

return [
    'index' => 'Index Featured Category',
    'create' => 'Create Featured Category',
    'edit' => 'Edit Featured Category',
    'destroy' => 'Delete Featured Category',
];
