<?php

namespace Modules\Payment\Gateways;

use Omnipay\Omnipay;
use Modules\Cart\Facades\Cart;
use Omnipay\Common\CreditCard;

class AuthorizeNet
{
    public $label;
    public $description;

    public function __construct()
    {
        $this->label = setting('authorization_net_label');
        $this->description = setting('authorization_net_description');
    }

   
}
