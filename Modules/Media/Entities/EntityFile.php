<?php

namespace Modules\Media\Entities;


use Modules\Support\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class EntityFile extends Model
{
  
 // protected $table ='product_categories';
  protected $fillable =[
  	                    'file_id',
                        'entity_type',
                        'entity_id',
                        'zone',
                       ];

}
