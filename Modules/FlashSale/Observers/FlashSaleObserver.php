<?php

namespace Modules\FlashSale\Observers;

use Modules\FlashSale\Entities\FlashSale;
use Illuminate\Support\Str;

class FlashSaleObserver
{
    public function creating(FlashSale $flashSale)
    {
        if (!$flashSale->slug) {
            $flashSale->slug = Str::slug($flashSale->title);
        }
    }
    /**
     * Handle the flash sale "created" event.
     *
     * @param  \FleetCart\FlashSale  $flashSale
     * @return void
     */
    public function created(FlashSale $flashSale)
    {
        //
    }

    /**
     * Handle the flash sale "updated" event.
     *
     * @param  \FleetCart\FlashSale  $flashSale
     * @return void
     */
    public function updated(FlashSale $flashSale)
    {
        //
    }

    /**
     * Handle the flash sale "deleted" event.
     *
     * @param  \FleetCart\FlashSale  $flashSale
     * @return void
     */
    public function deleted(FlashSale $flashSale)
    {
        //
    }

    /**
     * Handle the flash sale "restored" event.
     *
     * @param  \FleetCart\FlashSale  $flashSale
     * @return void
     */
    public function restored(FlashSale $flashSale)
    {
        //
    }

    /**
     * Handle the flash sale "force deleted" event.
     *
     * @param  \FleetCart\FlashSale  $flashSale
     * @return void
     */
    public function forceDeleted(FlashSale $flashSale)
    {
        //
    }
}
