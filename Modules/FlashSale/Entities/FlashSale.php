<?php

namespace Modules\FlashSale\Entities;

use Modules\Support\Eloquent\Model;
use Modules\Support\Eloquent\Translatable;
use Modules\Media\Entities\File;
use Modules\Media\Eloquent\HasMedia;
use Modules\FlashSale\Observers\FlashSaleObserver;
use Modules\FlashSale\Admin\FlashSaleTable;

class FlashSale extends Model
{
    use Translatable, HasMedia;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translations'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'flash_sale_type',
        'slug',
        'title',
        'description',
        'start_datetime',
        'end_datetime',
        'is_active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];

    protected $dispatchesEvents = [
        'creating' => FlashSaleObserver::class
    ];

    public static $flash_sale_types = [
        'deal_of_the_day' => 'Deal of the Day',
        'start_to_end' => 'Start to End'
    ];

    public function products()
    {
        return $this->belongsToMany('Modules\Product\Entities\Product', 'flash_sale_products')->withPivot('price', 'position')->orderBy('flash_sale_products.position');
    }

    public function table($request)
    {
        $query = $this->newQuery();
        return new FlashSaleTable($query);
    }

    public function getBannerImageAttribute()
    {
        return $this->files->where('pivot.zone', 'banner_image')->first() ?: new File;
    }

    public function getBannerImageUrlAttribute()
    {
        return $this->banner_image->path;
    }

    public function scopeWithBannerImage($query)
    {
        $query->with(['files' => function ($q) {
            $q->wherePivot('zone', 'banner_image');
        }]);
    }

    public function scopeWithProducts($query)
    {
        $query->with(['products'=> function ($query) {
            $query->withBaseImage();
        }]);
    }

    public function scopeActive($query)
    {
        return $query->where(["is_active"=>true]);
    }

    public static function deal_of_the_day()
    {
        return self::active()
            ->withBannerImage()
            ->withProducts()
            ->where(["flash_sale_type"=>"deal_of_the_day"])->where("start_datetime", "=", date("Y-m-d"))->first();
    }

    public static function start_to_end()
    {
        return self::active()
            ->withBannerImage()
            ->withProducts()
            ->where(["flash_sale_type"=>"start_to_end"])->where("start_datetime", "<=", now())->where("end_datetime", ">=", now())->get();
    }
}
