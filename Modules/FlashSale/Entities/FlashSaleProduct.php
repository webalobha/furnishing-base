<?php

namespace Modules\FlashSale\Entities;

use Modules\Support\Eloquent\Model;
use Modules\Support\Money;

class FlashSaleProduct extends Model
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'flash_sale_id',
        'price',
        'is_active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];

    public function flash_sale()
    {
        return $this->belongsTo('Modules\FlashSale\Entities\FlashSale');
    }

    public function product()
    {
        return $this->belongsTo('Modules\Product\Entities\Product');
    }

    public function getPriceAttribute($price)
    {
        return Money::inDefaultCurrency($price);
    }
}
