<?php

namespace Modules\FlashSale\Providers;

use Modules\Support\Traits\AddsAsset;
use Illuminate\Support\ServiceProvider;
use Modules\Support\Traits\LoadsConfig;
use Modules\FlashSale\Admin\FlashSaleTabs;
use Modules\Admin\Ui\Facades\TabManager;
use Modules\FlashSale\Entities\FlashSale;
use Modules\FlashSale\Observers\FlashSaleObserver;

class FlashSaleServiceProvider extends ServiceProvider
{
    use AddsAsset, LoadsConfig;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        FlashSale::observe(FlashSaleObserver::class);
        TabManager::register('flash_sales', FlashSaleTabs::class);
        $this->addAdminAssets('admin.flash_sales.(create|edit)', [
            'admin.media.css', 'admin.media.js'
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->loadConfigs(['assets.php', 'permissions.php']);
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
