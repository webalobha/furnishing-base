@extends('admin::layout')
@component('admin::components.page.header')
@slot('title', trans('admin::resource.edit', ['resource' => trans('flashsale::flash_sales.flash_sale')]))
@slot('subtitle', '')
<li><a href="{{ route('admin.flash_sales.index') }}">{{ trans('flashsale::flash_sales.flash_sales') }}</a></li>
<li class="active">{{ trans('admin::resource.edit', ['resource' => trans('flashsale::flash_sales.flash_sale')]) }}</li>
@endcomponent
@section('content')
<form method="POST" action="{{ route('admin.flash_sales.update', $flashSale) }}" class="form-horizontal" id="flash-sale-edit-form" novalidate>
    {{ csrf_field() }}
    {{ method_field('put') }}
    {!! $tabs->render(compact('flashSale')) !!}
</form>
@endsection
@include('flashsale::admin.flash_sales.partials.shortcuts')
@push('scripts')
<script>
    const start_datetime = '{{ $flashSale->start_datetime }}';
    const end_datetime = '{{ $flashSale->end_datetime }}';

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
    $(document).ready(function () {
        $("#title").on('input', function () {
            $("#slug").val(slugify($("#title").val()));
        });
        $("#start_datetime").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            defaultDate: new Date(start_datetime)
        });
        $("#end_datetime").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            defaultDate: new Date(end_datetime)
        });
    });
</script>
@endpush
