@foreach($products as $product)
<div class="col-sm-3" style="padding: 10px;">
    <div class="card" style="width: 15rem;height: 15rem;text-align: center;border:1px solid black;" pk="{{ $product->id }}">
        <div class="single-image image-holder-wrapper clearfix">
            @if($product->base_image->path)
            <img class="card-img-top" src="{{ $product->base_image->path }}" alt="Banner Image" style="width: 100px;height: 100px;">
            @else
            <div class="image-holder placeholder">
                <i class="fa fa-picture-o" style="width: 100px;height: 100px;"></i>
            </div>
            @endif
        </div>
        <div class="card-body">
            <p class="card-text">{{ $product->name }}</p>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.products.edit', ['id'=> $product->id]) }}"><i class="fa fa-edit"></i></a>
            <a href="javascript:void(0);" class="delete_icon" onclick="product_delete('{{ $product->id }}')"><i class="fa fa-trash"></i></a>
        </div>
    </div>
</div>
@endforeach
