@extends('admin::layout')
@component('admin::components.page.header')
@slot('title', trans('flashsale::flash_sales.flash_sales'))
<li class="active">{{ trans('flashsale::flash_sales.flash_sales') }}</li>
@endcomponent
@component('admin::components.page.index_table')
@slot('buttons', ['create'])
@slot('resource', 'flash_sales')
@slot('name', trans('flashsale::flash_sales.flash_sale'))
@component('admin::components.table')
@slot('thead')
<tr>
    @include('admin::partials.table.select_all')
    <th>{{ trans('flashsale::flash_sales.table.flash_sale_type') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.slug') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.title') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.banner_image') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.description') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.start_datetime') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.end_datetime') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.is_active') }}</th>
    <th>{{ trans('flashsale::flash_sales.table.created_at') }}</th>
</tr>
@endslot
@endcomponent
@endcomponent
@push('scripts')
<script>
    new DataTable('#flash_sales-table .table', {
        columns: [
            { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
            { data: 'flash_sale_type' },
            { data: 'slug' },
            { data: 'title' },
            { data: 'thumbnail', orderable: false, searchable: false, width: '10%' },
            { data: 'description' },
            { data: 'start_datetime', name: 'start_datetime', searchable: false },
            { data: 'end_datetime', name: 'end_datetime', searchable: false },
            { data: 'status', name: 'is_active', searchable: false },
            { data: 'created', name: 'created_at', searchable: false },
        ],
    });
</script>
@endpush
