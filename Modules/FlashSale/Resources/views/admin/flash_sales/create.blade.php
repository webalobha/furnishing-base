@extends('admin::layout')
@component('admin::components.page.header')
@slot('title', trans('admin::resource.create', ['resource' => trans('flashsale::flash_sales.flash_sale')]))
<li><a href="{{ route('admin.flash_sales.index') }}">{{ trans('flashsale::flash_sales.flash_sales') }}</a></li>
<li class="active">{{ trans('admin::resource.create', ['resource' => trans('flashsale::flash_sales.flash_sale')]) }}</li>
@endcomponent
@section('content')
<form method="POST" action="{{ route('admin.flash_sales.store') }}" class="form-horizontal" id="flash-sale-create-form" novalidate>
    {{ csrf_field() }}
    {!! $tabs->render(compact('flashSale')) !!}
</form>
@endsection
@include('flashsale::admin.flash_sales.partials.shortcuts')
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
    $(document).ready(function () {
        $("#title").on('input', function () {
            $("#slug").val(slugify($("#title").val()));
        });
        $("#start_datetime").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            defaultDate: moment().startOf('day').toDate()
        });
        $("#end_datetime").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            defaultDate: moment().endOf('day').toDate()
        });
    });
</script>
@endpush
