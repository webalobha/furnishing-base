<div class="row">
    <div class="col-md-8">
        {{ Form::select('flash_sale_type', trans('flashsale::attributes.flash_sale_type'), $errors, $flash_sale_types, $flashSale) }}
        @include('media::admin.image_picker.single', [
        'title' => trans('flashsale::attributes.banner_image'),
        'inputName' => 'files[banner_image]',
        'file' => $flashSale->banner_image,
        ])
        {{ Form::text('slug', trans('flashsale::attributes.slug'), $errors, $flashSale, ['required' => true]) }}
        {{ Form::text('title', trans('flashsale::attributes.title'), $errors, $flashSale, ['required' => true]) }}
        {{ Form::textarea('description', trans('flashsale::attributes.description'), $errors, $flashSale) }}
        {{ Form::text('start_datetime', trans('flashsale::attributes.start_datetime'), $errors, $flashSale, ['class' => 'datetime-picker', 'required'=>true]) }}
        {{ Form::text('end_datetime', trans('flashsale::attributes.end_datetime'), $errors, $flashSale, ['class' => 'datetime-picker', 'required'=>true]) }}
        {{ Form::checkbox('is_active', trans('flashsale::attributes.is_active'), trans('flashsale::flash_sales.form.enable_the_flash_sale'), $errors, $flashSale) }}
    </div>
</div>
