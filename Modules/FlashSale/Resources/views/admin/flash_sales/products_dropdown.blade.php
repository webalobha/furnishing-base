<select style="width:100%;">
    <option value="0">Product ## Price ## Special Price</option>
    @foreach($products as $product)
    <option value="{{ $product->id }}" {{ $product->id == $product_id  ? 'selected' : ''}}>
        {{ $product->name.' ## '.($product->price ? $product->price->format() : 'N/A').' ## '.($product->special_price ? $product->special_price->format() : 'N/A') }}
    </option>
    @endforeach
</select>
