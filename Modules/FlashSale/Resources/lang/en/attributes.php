<?php

return [
    'flash_sale_type' => "Flash Sale Type",
    'banner_image' => 'Banner Image',
    'slug' => 'Slug',
    'title' => 'Title',
    'description' => 'Description',
    'start_datetime' => 'Start Datetime',
    'end_datetime' => 'End Datetime',
    'is_active' => ''
];
