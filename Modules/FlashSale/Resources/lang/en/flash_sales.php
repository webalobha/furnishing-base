<?php

return [
    'flash_sale' => 'Flash Sale',
    'flash_sales' => 'Flash Sales',
    'products' => 'Manage Products',
    'form' => [
        'enable_the_flash_sale' => 'Enable the Flash Sale'
    ],
    'table' => [
        'flash_sale_type' => 'Flash Sale Type',
        'slug' => 'Slug',
        'title' => 'Title',
        'banner_image' => 'Banner Image',
        'description' => 'Description',
        'start_datetime' => 'Start Datetime',
        'end_datetime' => 'End Datetime',
        'is_active' => 'Status',
        'created_at' => 'Created At'
    ]
];
