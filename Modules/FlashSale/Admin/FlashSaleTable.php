<?php

namespace Modules\FlashSale\Admin;

use Modules\Admin\Ui\AdminTable;
use Modules\FlashSale\Entities\FlashSale;

class FlashSaleTable extends AdminTable
{
    public function make()
    {
        return $this->newTable()
        ->editColumn('thumbnail', function ($flashSale) {
            $path = optional($flashSale->banner_image)->path;
            return view('flashsale::admin.flash_sales.partials.table.thumbnail', compact('path'));
        })
        ->editColumn('flash_sale_type', function ($flashSale) {
            $flash_sale_type_display = FlashSale::$flash_sale_types[$flashSale->flash_sale_type];
            return $flash_sale_type_display;
        });
    }
}
