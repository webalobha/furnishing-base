<?php

namespace Modules\FlashSale\Admin;

use Modules\Admin\Ui\Tab;
use Modules\Admin\Ui\Tabs;
use Modules\FlashSale\Entities\FlashSale;

class FlashSaleTabs extends Tabs
{
    public function make()
    {
        $this->group('flash_sale_information', 'Flash Sale Information')
            ->active()
            ->add($this->general());
    }

    public function general()
    {
        return tap(new Tab('general', 'General'), function (Tab $tab) {
            $tab->active();
            $tab->weight(5);

            $tab->fields([
                'flash_sale_type',
                'slug',
                'title',
                'description',
                'start_datetime',
                'end_datetime',
                'is_active'
            ]);

            $tab->view('flashsale::admin.flash_sales.tabs.general', [
                'flash_sale_types' => FlashSale::$flash_sale_types
            ]);
        });
    }
}
