<?php

namespace Modules\FlashSale\Http\Requests;

use Modules\Core\Http\Requests\Request;
use Illuminate\Validation\Rule;
use Modules\FlashSale\Entities\FlashSale;

class SaveFlashSaleRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'flashsale::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $base_rules = [
            'flash_sale_type'=> ['required', Rule::in(array_keys(FlashSale::$flash_sale_types))],
            'slug'=>['required'],
            'title'=> ['required', 'max:190'],
            'start_datetime'=> ['required'],
            'end_datetime'=> ['required']
        ];
        if (request()->route()->getName() == 'admin.flash_sales.store') {
            array_push($base_rules['slug'], 'unique:flash_sales,slug');
        }
        return $base_rules;
    }
}
