<?php

namespace Modules\FlashSale\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\FlashSale\Entities\FlashSale;
use Modules\FlashSale\Http\Requests\SaveFlashSaleRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Support\Money;
use Modules\Product\Entities\Product;
use Illuminate\Database\Eloquent\Builder;

class FlashSaleController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = FlashSale::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'flashsale::flash_sales.flash_sale';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'flashsale::admin.flash_sales';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveFlashSaleRequest::class;

    public function products_index(Request $request)
    {
        $flash_sales = FlashSale::all();
        if ($flash_sales->isEmpty()) {
            $request->session()->flash('error', 'Please create a Flash Sale first');
            return redirect(route('admin.flash_sales.index'));
        }
        return view("{$this->viewPath}.products_index")->with([
            'flash_sales' => $flash_sales
        ]);
    }

    public function products_list(Request $request)
    {
        $flash_sale_id = $request->flash_sale_id;
        $flash_sale = FlashSale::where(['id'=>$flash_sale_id])->first();
        $products = [];
        foreach ($flash_sale->products as $product) {
            $obj = [
                'id'=>$product->id,
                'name'=>$product->name,
                'price'=>$product->price,
                'special_price'=>$product->special_price,
                'flash_sale_price'=>Money::inDefaultCurrency($product->pivot->price),
            ];
            array_push($products, $obj);
        }
        return response()->json([
            "data"=>[
                "products"=>$products
            ]
        ]);
    }

    public function products_dropdown(Request $request)
    {
        $flash_sale_id = $request->flash_sale_id;
        $product_id = $request->product_id;
        $flash_sale = FlashSale::where(['id'=>$flash_sale_id])->first();
        $products = Product::select('id', 'price', 'special_price')->whereDoesntHave('flash_sales', function (Builder $query) use ($flash_sale_id) {
            $query->where('flash_sale_id', $flash_sale_id);
        })->get();
        if ($product_id) {
            $product_collection = Product::where(["id"=>$product_id])->get();
            $products = $product_collection->merge($products);
        }
        return view("{$this->viewPath}.products_dropdown")->with([
            'products'=>$products,
            'product_id'=>$product_id
        ]);
    }

    public function products_sync(Request $request)
    {
        $flash_sale_id = $request->flash_sale_id;
        $product_ids = $request->product_ids;
        $flash_sale = FlashSale::where(['id'=>$flash_sale_id])->first();
        $products = [];
        foreach ($product_ids as $product_id) {
            $products[$product_id["id"]] = $product_id["data"];
        }
        $flash_sale->products()->sync($products);
        return response()->json();
    }
}
