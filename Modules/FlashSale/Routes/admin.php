<?php

Route::get('flash-sales', [
    'as' => 'admin.flash_sales.index',
    'uses' => 'FlashSaleController@index',
    'middleware' => 'can:admin.flash_sales.index',
]);

Route::get('flash-sales/create', [
    'as' => 'admin.flash_sales.create',
    'uses' => 'FlashSaleController@create',
    'middleware' => 'can:admin.flash_sales.create',
]);

Route::post('flash-sales', [
    'as' => 'admin.flash_sales.store',
    'uses' => 'FlashSaleController@store',
    'middleware' => 'can:admin.flash_sales.create',
]);

Route::get('flash-sales/{id}/edit', [
    'as' => 'admin.flash_sales.edit',
    'uses' => 'FlashSaleController@edit',
    'middleware' => 'can:admin.flash_sales.edit',
]);

Route::put('flash-sales/{id}', [
    'as' => 'admin.flash_sales.update',
    'uses' => 'FlashSaleController@update',
    'middleware' => 'can:admin.flash_sales.edit',
]);

Route::delete('flash-sales/{ids?}', [
    'as' => 'admin.flash_sales.destroy',
    'uses' => 'FlashSaleController@destroy',
    'middleware' => 'can:admin.flash_sales.destroy',
]);

Route::get('flash-sales/products_index', [
    'as' => 'admin.flash_sales.products_index',
    'uses' => 'FlashSaleController@products_index',
    'middleware' => 'can:admin.flash_sales.index',
]);

Route::post('flash-sales/products_list', [
    'as' => 'admin.flash_sales.products_list',
    'uses' => 'FlashSaleController@products_list',
    'middleware' => 'can:admin.flash_sales.index',
]);

Route::post('flash-sales/products_sync', [
    'as' => 'admin.flash_sales.products_sync',
    'uses' => 'FlashSaleController@products_sync',
    'middleware' => 'can:admin.flash_sales.index',
]);

Route::post('flash-sales/products_dropdown', [
    'as' => 'admin.flash_sales.products_dropdown',
    'uses' => 'FlashSaleController@products_dropdown',
    'middleware' => 'can:admin.flash_sales.index',
]);


// append
