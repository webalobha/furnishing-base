<div class="row">
    <div class="col-md-8">
        {{ Form::checkbox('authorization_net_enabled', trans('setting::attributes.authorization_net_enabled'), trans('setting::settings.form.enabled_authorization_net'), $errors, $settings) }}
        {{ Form::text('translatable[authorization_net_label]', trans('setting::attributes.translatable.authorization_net_label'), $errors, $settings, ['required' => true]) }}
        {{ Form::textarea('translatable[authorization_net_description]', trans('setting::attributes.translatable.authorization_net_description'), $errors, $settings, ['rows' => 3, 'required' => true]) }}
        {{ Form::checkbox('authorization_net_test_mode', trans('setting::attributes.authorization_net_test_mode'), trans('setting::settings.form.use_autorize_net_for_test_payments'), $errors, $settings) }}
        
        <div class="{{ old('authorization_net_enabled', array_get($settings, 'authorization_net_enabled')) ? '' : 'hide' }}" id="paypal-express-fields">
            {{ Form::text('authorization_net_login_id', trans('setting::attributes.authorization_net_login_id'), $errors, $settings, ['required' => true]) }}
            {{ Form::text('authorization_net_transaction_id', trans('setting::attributes.authorization_net_transaction_id'), $errors, $settings, ['required' => true]) }}
           
        </div>
    </div>
</div>
