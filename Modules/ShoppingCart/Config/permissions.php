<?php

return [
    'admin.shopping_carts' => [
        'index' => 'shoppingcart::permissions.index',
        'create' => 'shoppingcart::permissions.create',
        'edit' => 'shoppingcart::permissions.edit',
        'destroy' => 'shoppingcart::permissions.destroy',
    ],

// append

];
