<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'all-assets' => [
        'admin.shoppingcart.css' => ['module' => 'shoppingcart:admin/css/shoppingcart.css'],
        'admin.shoppingcart.js' => ['module' => 'shoppingcart:admin/js/shoppingcart.js'],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your pages
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'required-assets' => [],
];
