<?php

Route::get('shopping-carts', [
    'as' => 'admin.shopping_carts.index',
    'uses' => 'ShoppingCartController@index',
    'middleware' => 'can:admin.shopping_carts.index',
]);

Route::get('shopping-carts/create', [
    'as' => 'admin.shopping_carts.create',
    'uses' => 'ShoppingCartController@create',
    'middleware' => 'can:admin.shopping_carts.create',
]);

Route::post('shopping-carts', [
    'as' => 'admin.shopping_carts.store',
    'uses' => 'ShoppingCartController@store',
    'middleware' => 'can:admin.shopping_carts.create',
]);

Route::get('shopping-carts/{id}/edit', [
    'as' => 'admin.shopping_carts.edit',
    'uses' => 'ShoppingCartController@edit',
    'middleware' => 'can:admin.shopping_carts.edit',
]);

Route::put('shopping-carts/{id}', [
    'as' => 'admin.shopping_carts.update',
    'uses' => 'ShoppingCartController@update',
    'middleware' => 'can:admin.shopping_carts.edit',
]);

Route::delete('shopping-carts/{ids?}', [
    'as' => 'admin.shopping_carts.destroy',
    'uses' => 'ShoppingCartController@destroy',
    'middleware' => 'can:admin.shopping_carts.destroy',
]);

// append

