<?php

namespace Modules\ShoppingCart\Entities;

use Modules\Support\Eloquent\Model;

class ShoppingCartItem extends Model
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shopping_cart_id',
        'product_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];

    public function shopping_cart()
    {
        return $this->belongsTo('Modules\ShoppingCart\Entities\ShoppingCart');
    }

    public function product()
    {
        return $this->belongsTo('Modules\Product\Entities\Product');
    }
}
