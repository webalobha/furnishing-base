<?php

namespace Modules\ShoppingCart\Entities;

use Modules\Support\Eloquent\Model;

class ShoppingCart extends Model
{
    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [];

    public function shopping_cart_items()
    {
        return $this->hasMany('Modules\ShoppingCart\Entities\ShoppingCartItem');
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User');
    }
}
