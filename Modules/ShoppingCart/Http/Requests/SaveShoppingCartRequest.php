<?php

namespace Modules\ShoppingCart\Http\Requests;

use Modules\Core\Http\Requests\Request;

class SaveShoppingCartRequest extends Request
{
    /**
     * Available attributes.
     *
     * @var string
     */
    protected $availableAttributes = 'shoppingcart::attributes';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
