<?php

namespace Modules\ShoppingCart\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Modules\Admin\Traits\HasCrudActions;
use Modules\ShoppingCart\Entities\ShoppingCart;
use Modules\ShoppingCart\Http\Requests\SaveShoppingCartRequest;

class ShoppingCartController extends Controller
{
    use HasCrudActions;

    /**
     * Model for the resource.
     *
     * @var string
     */
    protected $model = ShoppingCart::class;

    /**
     * Label of the resource.
     *
     * @var string
     */
    protected $label = 'shoppingcart::shopping_carts.shopping_cart';

    /**
     * View path of the resource.
     *
     * @var string
     */
    protected $viewPath = 'shoppingcart::admin.shopping_carts';

    /**
     * Form requests for the resource.
     *
     * @var array
     */
    protected $validation = SaveShoppingCartRequest::class;
}
