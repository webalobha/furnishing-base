<?php

return [
    'index' => 'Index Shopping Cart',
    'create' => 'Create Shopping Cart',
    'edit' => 'Edit Shopping Cart',
    'destroy' => 'Delete Shopping Cart',
];
