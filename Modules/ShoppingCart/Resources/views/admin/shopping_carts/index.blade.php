@extends('admin::master')

@component('admin::components.page.header')
    @slot('title', trans('shoppingcart::shopping_carts.shopping_carts'))

    <li class="active">{{ trans('shoppingcart::shopping_carts.shopping_carts') }}</li>
@endcomponent

@component('admin::components.page.index_table')
    @slot('buttons', ['create'])
    @slot('resource', 'shopping_carts')
    @slot('name', trans('shoppingcart::shopping_carts.shopping_cart'))

    @component('admin::components.table')
        @slot('thead')
            <tr>
                @include('admin::partials.table.select_all')
            </tr>
        @endslot
    @endcomponent
@endcomponent

@push('scripts')
    <script>
        new DataTable('#shopping_carts-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                //
            ],
        });
    </script>
@endpush
