@extends('admin::master')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.create', ['resource' => trans('shoppingcart::shopping_carts.shopping_cart')]))

    <li><a href="{{ route('admin.shopping_carts.index') }}">{{ trans('shoppingcart::shopping_carts.shopping_carts') }}</a></li>
    <li class="active">{{ trans('admin::resource.create', ['resource' => trans('shoppingcart::shopping_carts.shopping_cart')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.shopping_carts.store') }}" class="form-horizontal" id="shopping-cart-create-form" novalidate>
        {{ csrf_field() }}
    </form>
@endsection

@include('shoppingcart::admin.shopping_carts.partials.shortcuts')
