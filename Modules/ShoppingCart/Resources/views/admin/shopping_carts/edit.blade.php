@extends('admin::master')

@component('admin::components.page.header')
    @slot('title', trans('admin::resource.edit', ['resource' => trans('shoppingcart::shopping_carts.shopping_cart')]))
    @slot('subtitle', '')

    <li><a href="{{ route('admin.shopping_carts.index') }}">{{ trans('shoppingcart::shopping_carts.shopping_carts') }}</a></li>
    <li class="active">{{ trans('admin::resource.edit', ['resource' => trans('shoppingcart::shopping_carts.shopping_cart')]) }}</li>
@endcomponent

@section('content')
    <form method="POST" action="{{ route('admin.shopping_carts.update', $shoppingCart) }}" class="form-horizontal" id="shopping-cart-edit-form" novalidate>
        {{ csrf_field() }}
        {{ method_field('put') }}
    </form>
@endsection

@include('shoppingcart::admin.shopping_carts.partials.shortcuts')
