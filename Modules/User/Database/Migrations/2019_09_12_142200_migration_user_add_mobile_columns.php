<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrationUserAddMobileColumns extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('mobile_number')->nullable()->unique();
            $table->string('mobile_number_otp')->nullable();
            $table->datetime('mobile_number_otp_valid_till')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('mobile_number');
            $table->dropColumn('mobile_number_otp');
            $table->dropColumn('mobile_number_otp_valid_till');
        });
    }
}
